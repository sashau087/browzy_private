(function($) {

$.fn.prepareFileDropInput = function() {

    this.each(function(i,elem){

        var $inputFile = $(elem);
        var $dropZone = $inputFile.closest('.dashed_rounded_block');

        var mouseOverClass = "drag_over";

        $dropZone.off("dragover dragleave drop");
        $dropZone.on("dragover", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $dropZone.addClass(mouseOverClass);
            var x = e.pageX;
            var y = e.pageY;

            var ooleft = $dropZone.offset().left;
            var ooright = $dropZone.outerWidth() + ooleft;
            var ootop = $dropZone.offset().top;
            var oobottom = $dropZone.outerHeight() + ootop;

            if (!(x < ooleft || x > ooright || y < ootop || y > oobottom)) {
                $inputFile.offset({ top: y - $inputFile.outerHeight() / 2, left: x - $inputFile.outerWidth() / 2 });
            } else {
                $inputFile.offset({ top: -400, left: -400 });
            }
        });
        $dropZone.on("dragleave drop", function () {
            $dropZone.removeClass(mouseOverClass);
        });

        var $button = $dropZone.find(".browse_button");
        if (!$button.length) {
            var buttonWord = 'Browse';
            if (typeof $inputFile.attr('title') != 'undefined') {
              buttonWord = $inputFile.attr('title');
            }
            $button = $inputFile.wrap('<a class="browse_button filebutton"></a>').parent();
            $button.prepend($('<span></span>').html(buttonWord));
        }

        if ($button.length) {
            $button.css("margin-left", -$button.outerWidth() / 2 + "px");

            $button.off("mousemove");
            $button.on("mousemove", function (e) {
                var x = e.pageX;
                var y = e.pageY;

                var oleft = $button.offset().left;
                var oright = $button.outerWidth() + oleft;
                var otop = $button.offset().top;
                var obottom = $button.outerHeight() + otop;

                if (!(x < oleft || x > oright || y < otop || y > obottom)) {
                    $inputFile.offset({ top: y - $inputFile.outerHeight() / 2, left: x - $inputFile.outerWidth() / 2 });
                } else {
                    $inputFile.offset({ top: -400, left: -400 });
                }
            });

            $inputFile.on("change", function(e) {
                $button.find("span").text(e.target.files[0].name);
            });
        }
    })
};
})(jQuery);
