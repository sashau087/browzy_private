$(document).ready(function(){
    window.jPostsLoading = $('#posts_loading')
    if (window.jPostsLoading.length) {
        $(window).scroll(function(){
            if (window.jPostsLoading && isScrolledIntoView(window.jPostsLoading)) {
                window.jPostsLoading = null;
                $.get(window.loadPostsUrl, function(response) {
                     $("#posts_loading").replaceWith(response);
                });
            }
        });
    }
});

function isScrolledIntoView(jEl) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();
    var elemTop = jEl.offset().top;
    var elemBottom = elemTop + jEl.height();
    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

var isAdvancedUpload = function() {
    var div = document.createElement('div');
    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

//function upload_file(ajaxData, inputHash, inputFile, file) {
//    $.ajax({
//        url: '/post/upload',
//        type: 'POST',
//        data: ajaxData,
//        dataType: 'json',
//        cache: false,
//        contentType: false,
//        processData: false,
//        complete: function (http_request) {
//            if (http_request.readyState == 4 && http_request.status == 200) {
//                inputHash.val(http_request.responseText);
//                inputFile.prev().text(file.name);
//                set_background_file(inputFile.closest('.gray_rounded_block'), file);
//            } else {
//                alert('Something goes wrong with file upload');
//            }
//        }
//    });
//}

//function clear_prev_covers(cover_parent) {
//    $.each(cover_parent.find('input[type="file"]'), function (i, value) {
//        var span = value.previousSibling;
//        span.textContent = span.innerText = value.title;
//        value.name = '';
//    });
//}

function change_source(event) {
    var val = $(event.target).val();
    $('#my_post').prop('checked', !val);
    $('#other_source').prop('checked', !!val);
}

function clear_source() {
    $('#source_link').val('');
}

function validateLogin() {
    var valid = true;
    var $username = $('#log [name="username"]');
    var $password = $('#log [name="password"]');
    if (!$username.val()) {
        valid = false;
        showError($username, "Please enter a username");
    } else {
        hideError($username);
    }
    if (!$password.val()) {
        valid = false;
        showError($password, "Please enter a password");
    } else {
        hideError($password);
    }
    return valid;
}

function showError($input, errorMsg) {
    $input.parent().addClass("has-error");
    $input.parent().find(".error").text(errorMsg);
}

function hideError($input) {
    $input.parent().removeClass("has-error");
}

function selectButton(button) {
    $(button).parent().find("button").removeClass("btn-selected").addClass("btn-secondary");
    $(button).removeClass("btn-secondary").addClass("btn-selected");
}

function selectTab(element) {
    $(element).closest("ul").children().removeClass("active");
    $(element).closest("li").addClass("active");
}

function showMessage(msg) {
    $('#message').html(htmlDecode(msg));
    $('#messageDialog').modal('show');
}

function htmlDecode(input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function htmlToText(html) {
    return $('<div>').html(html).text();
}

function setAvatar(event) {
    if (validateImage(event)) {
//        setImage($(event.target).closest('.image').find('img'), event.target.files[0]);
        return true;
    } else {
        return false;
    }
}

function reduceActivityCounter() {
    var nm = $(".top_menu .num").html()-1;
    $(".top_menu .num").html(nm);
}

// ---------------------
// View post
// ---------------------
function setLiked() {
    $('.likes2').addClass('selected').attr('data-original-title', "You liked this post");
    $('.dislikes2').removeClass('selected').attr('data-original-title', "If you dislike click here");
}

function setDisliked() {
    $('.dislikes2').addClass('selected').attr('data-original-title', "You disliked this post");
    $('.likes2').removeClass('selected').attr('data-original-title', "If you like click here");
}

function setStared() {
    $(".star").attr('data-original-title', "Post marked as favorite");
}

function likeDislikeResponseHandler(jParentEl, response) {
    if (response) {
        var parts = response.split(',');
        jParentEl.find(".likes").text(parts[0]);
        jParentEl.find(".dislikes").text(parts[1]);
    }
}

function starResponseHandler(jEl, response) {
    if (response) {
        jEl.text(response);
    }
}