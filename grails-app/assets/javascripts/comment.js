var s=1;
$(document).ready(function(){

    if (document['location']['hash']!=="") {
        var classComment=document['location']['hash'].replace("#","")
        $('html, body').animate({scrollTop: $('.'+classComment).offset().top+200}, 2000);
        return false;
    }
    $(".leave_comment").click(function(){
        $(".textarea_container .form-control").focus()
        $('html, body').animate({
            scrollTop: $("#leave_comment").offset().top-10
        }, 200);
    })

    $(".add_comment").click(function(event) {
            event.preventDefault()
            var formdata = new FormData();

            if($('.commentBox').val()=='' && ($("[name='image']").val()=='')){
                $(".textarea_container .form-control").focus()
                $('html, body').animate({
                    scrollTop: $("#leave_comment").offset().top-10
                }, 200);
            }else {
                if($('.commentBox').val()==''){
                    $('.commentBox').val("....")
                }
                formdata.append("text", $('.commentBox').val());
                formdata.append("postId", $('#block_of_comments').attr('postId'));
                formdata.append("file", $("[name='image']").get(0).files[0]);
                var url = "/comment/saveComment";
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    complete: function (data) {
                        $('.commentBox').val('')
                        $("[name='image']").val('')
                        $('.img_comment').html('')
                        $('.commentfile span').html('Upload image')
                        var formdata2 = new FormData();
                        formdata2.append("postId", $('#block_of_comments').attr('postId'));
                        formdata2.append("commentId", data.responseJSON);
                        $.ajax({
                            type: "POST",
                            url: "/comment/generateTemplate",
                            dataType: 'JSON',
                            data: formdata2,
                            contentType: false,
                            cache: false,
                            processData: false,
                            complete: function (data) {
                                $('.subComment').after(data.responseText)
                                $('.noComments').remove()

                            }
                        })
                    }
                });
            }
            return false;
        });

        $("#more_comments").click(function(event) {
                var formdata = new FormData();
                formdata.append("postId", $('#block_of_comments').attr('postId'));
                formdata.append("lastId", $('#block_of_comments').attr('lastId'));
//            formdata.append("file3", $('#img_product3').get(0).files[0]);

                $.ajax({
                    type: "POST",
                    url: "/comment/commentMore",
                    dataType: 'JSON',
                    data: formdata,
                    contentType: false,
                    cache: false,
                    processData: false,
                    complete: function (data) {
                        for(var i=0; i<data.responseJSON.length; i++){
                            $('#block_of_comments').attr('lastId', data.responseJSON[i][0])
                            var formdata2 = new FormData();
                            formdata2.append("postId", $('#block_of_comments').attr('postId'));
                            formdata2.append("commentId", data.responseJSON[i][0]);
                            $.ajax({
                                type: "POST",
                                url: "/comment/generateTemplate",
                                dataType: 'JSON',
                                data: formdata2,
                                contentType: false,
                                cache: false,
                                processData: false,
                                complete: function (data) {
                                    console.log(data.responseText)
                                    $(".com").before(data.responseText)
                                }
                            })
                        }
                        if(data.responseJSON.length<3){
                            $("#more_comments").remove()
                        }
                    }
                })
                return false;
            });
        $(".commentFile").change(function(){

            readURL(this, "");

            // alert(this+' next')
        });
    $(".commentFileReply").each(function () {
        $(this).change(function(){

            readURL(this, $(this).attr('id'));
        });
    })
})

function add_comment_reply(id, event) {
    event.preventDefault()
    var idComment = id;
    var formdata = new FormData();

    if($('.commentBox' + idComment).val()=='' && ($("[name='image']" + idComment).val()=='')){
        $(".textarea_container .form-control").focus()
        $('html, body').animate({
            scrollTop: $("#leave_comment").offset().top-10
        }, 200);
    }else {
        if($('.commentBox' + idComment).val()==''){
            $('.commentBox' + idComment).val("....")
        }
        formdata.append("text", $('.commentBox' + idComment).val());
        formdata.append("id", idComment);
        formdata.append("postId", $('#block_of_comments').attr('postId'));
        formdata.append("file", $("[name='image" + idComment + "']").get(0).files[0]);
        var url = "/saveReplyComment";
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: formdata,
            processData: false,
            contentType: false,
            complete: function (data) {
                $('.img_comment'+idComment).html('')
                $('.commentBox' + idComment).val('')
                $("[name='image']" + idComment).val('')
                $('.commentfile' + idComment + ' span').html('Upload image')
                var formdata2 = new FormData();
                formdata2.append("postId", $('#block_of_comments').attr('postId'));
                formdata2.append("commentId", data.responseJSON);
                // if($('.replyBlock' + idComment).find('button').hasClass('additional')) {
                formdata2.append("class1", 1);
                // }
                $.ajax({
                    type: "POST",
                    url: "/comment/generateTemplate",
                    dataType: 'JSON',
                    data: formdata2,
                    contentType: false,
                    cache: false,
                    processData: false,
                    complete: function (data) {
                        console.log('.com' + idComment)
                        if ($('.replyBlock' + idComment).find('button').hasClass('additional')) {
                            $('.com' + idComment).after(data.responseText)
                        } else {

                            $('.replyBlock' + idComment).after(data.responseText)
                        }
                    }
                })
                $('.replyBlock' + idComment).css({'display': 'none'})
                $('.replyButton' + idComment).html('Reply')
                $('.replyButton' + idComment).attr('onclick', 'reply(' + id + ")")
            }
        });
    }
    return false;
}
function reply(id) {
    $('.replyBlock'+id).css({'display': 'block'})
}
function dislikeComment(comment_id) {

    var formdata = new FormData();
    formdata.append("id", comment_id);
    $.ajax({
        type: "POST",
        url: "/comment/dislikeComment",
        dataType: 'JSON',
        data: formdata,
        contentType: false,
        cache: false,
        processData: false,
        complete: function (data) {

            if(!($('.dislikesComment'+comment_id).hasClass('dislikesCommentAdded'))){
                $('.dislikesComment'+comment_id).html(parseInt($('.dislikesComment'+comment_id).html())+1)
            }
            if(($('.likesComment'+comment_id).hasClass('likesCommentAdded'))){
                $('.likesComment'+comment_id).html(parseInt($('.likesComment'+comment_id).html())-1)
            }
            $('.likesComment'+comment_id).removeClass('likesCommentAdded')
            $('.dislikesComment'+comment_id).addClass('dislikesCommentAdded')
        }
    })
}
function likeComment(comment_id) {
    var x=$(this)
    console.log(comment_id)
    var formdata = new FormData();
    formdata.append("id", comment_id);
    $.ajax({
        type: "POST",
        url: "/comment/likeComment",
        dataType: 'JSON',
        data: formdata,
        contentType: false,
        cache: false,
        processData: false,
        complete: function (data) {

            if($('.dislikesComment'+comment_id).hasClass('dislikesCommentAdded')){
                $('.dislikesComment'+comment_id).html(parseInt($('.dislikesComment'+comment_id).html())-1)
            }
            if(!($('.likesComment'+comment_id).hasClass('likesCommentAdded'))){
                $('.likesComment'+comment_id).html(parseInt($('.likesComment'+comment_id).html())+1)
            }
            $('.likesComment'+comment_id).addClass('likesCommentAdded')
            $('.dislikesComment'+comment_id).removeClass('dislikesCommentAdded')
        }
    })
}
function readURL(input, idFile) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            // $(this).parent().find('img_comment').html('dd')
            $('#photo').remove()
            $('.img_comment'+idFile).html('<img id="photo" style="margin: 12px;width: 300px;" src="'+e.target.result+'"><br><div class="delete_img" onclick="removeImage(this, '+idFile+')" style="margin-left: 12px;color: #ff618d;">Delete image</div>')
        }
        reader.readAsDataURL(input.files[0]);

    }
}
function removeImage(image, fileId) {
    if(fileId==''){
        $(image).parent().html('')
        $("[name='image']").val('')
    }else{
        $('.img_comment'+fileId).html('')
        $("[name='image']" + fileId).val('')
    }

}
function openComments(element) {
    $('.post_menu ul li').each(function () {
        $(this).removeClass('active')
    })
    $(element).parent().addClass('active')
    $('.commentsBlock').css({'display': 'block'})
    $('.postsBlock').css({'display': 'none'})
    return false;
}
function scrollToComment(id, element) {
    var formdata2 = new FormData();

    formdata2.append("id", id);
    $.ajax({
        url: "/comment/changestatus",
        type: 'POST',
        dataType: 'JSON',
        data: formdata2,
        processData: false,
        contentType: false,
        complete: function (data) {
        }
    });
    $('html, body').animate({scrollTop: $('.com'+id).offset().top}, 2000);
    $(element).parent().remove()
    var countReplies=parseInt($('.countreplies b').html())
    $('.countreplies b').html(countReplies-1)
    $('.countreplies').attr('onclick', "openReplies("+(countReplies-1)+")")
    $('.commentsBlockForReplies').css({'display': 'none'})
    return false;
}
function openReplies(id) {

    if(parseInt(id)!==0){
        $('.commentsBlockForReplies').css({'display': 'block'})
    }
}
function deleteComment(id) {
    var formdata = new FormData();

    formdata.append("id", id);
    $.ajax({
        url: "/comment/deleteComment",
        type: 'POST',
        dataType: 'JSON',
        data: formdata,
        processData: false,
        contentType: false,
        complete: function (data) {
            $('.com'+id).remove()
        }
    });
}