var userTblApp = angular.module('userTblApp', []);

userTblApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    $httpProvider.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET,PUT,POST,DELETE';
    $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = 'Content-Type';
    $httpProvider.defaults.headers.common['Access-Control-Max-Age'] = '86400';  //one day
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
}]);

userTblApp.controller('UserTblCtrl', ['$scope', '$http',
    function ($scope, $http) {
        $scope.sortUser = 'username';
        $scope.sortReverseUser = false;

        $scope.loading = true;
        $http.get('admin/userList').success(function (res) {
            $scope.users = res.users || [];
            $scope.posts = [];
            $scope.loading = false;
            $('#tableUsers').removeClass('hidden');
        });
    }
]);

userTblApp.controller('PostTblCtrl', ['$scope', '$http',
    function ($scope, $http) {
        $scope.sortPost = 'title';
        $scope.sortReversePost = false;

        $scope.findPosts = function () {
            $scope.author = this.user.username;
            $http({
                method: 'POST',
                url: 'admin/postList',
                params: {
                    author: $scope.author
                }
            }).then(function success(res) {
                $scope.posts = res.data.posts || [];
                $('#tablePosts').removeClass('hidden');
            });
        }
    }
]);