var VideoClass = function () {

    var valid

    var changeBlocks = function(elem) {
        var count = elem.dataset.count;
        var jContainer = $('.sortable');
        jContainer.empty();

        for (var i = 0; i < count; i++) {
            var newBlock = $('.post_block').first().clone();
            newBlock.addClass("visible");
            jContainer.append(newBlock);
            newBlock.find('[name=videolink]').change(updateVideoPreview);
            initTagsInput(newBlock.find('.video-tags-input'));
        }
    };

    var initTagsInput = function(jInputContainer) {
        jInputContainer.find('input').tagsinput({
          cancelConfirmKeysOnEmpty: false
        });
        jInputContainer.find('input')
            .focus(function() {
                jInputContainer.addClass('focused');
            })
            .blur(function() {
                jInputContainer.removeClass('focused');
            });
    };

    var updateVideoPreview = function() {
        var url = this.value;
        var jBlock = $(this).closest('.post_block');

        var baseUrl;
        var videoId;
        var regex = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/
        var match = url.match(regex)
        if (match) {
            videoId = match[7]
            baseUrl = 'https://www.youtube.com/embed'
        }

        if (!videoId) {
            match = url.match(/^.*(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/)

            if (match) {
                videoId = match[3]
                baseUrl = 'https://player.vimeo.com/video'
            }
        }

        console.log("baseUrl: " + baseUrl + ", videoId: " + videoId)
        if (baseUrl && videoId) {
            jBlock.find('.gray_rounded_block img').hide()
            jBlock.find('.gray_rounded_block iframe').attr('src', baseUrl + '/' + videoId).show()
        } else {
            jBlock.find('.gray_rounded_block img').show()
            jBlock.find('.gray_rounded_block iframe').hide()
        }
    }

    this.changeNav = function(elem) {
        var active = document.querySelector('#navigation>div.active')
        if(active) {
            active.classList.remove('active');
        }
        elem.classList.add('active');
        changeBlocks(elem);
    }

    this.validate = function() {
        valid = true;

        var jContainer = $('.sortable');

        jContainer.find('[name=title]').each(function() {
            var jTitle = $(this);
            var title = jTitle.val();
            if (!title) {
                showError(jTitle, 'Title is missing');
            } else if (title.length > MAX_POST_TITLE_SIZE) {
                showError(jTitle, 'Maximum title length is ' + MAX_POST_TITLE_SIZE);
            }
        });

        jContainer.find('[name=text]').each(function() {
            var jText = $(this);
            var text = jText.val();
            if (!text) {
                showError(jText, 'Description is missing');
            } else if (text.length > MAX_TEXT_SIZE) {
                showError(jText, 'Maximum description length is ' + MAX_TEXT_SIZE);
            }
        });

        jContainer.find('[name=videolink]').each(function() {
            var jText = $(this);
            var text = jText.val();
            if (!text) {
                showError(jText, 'Video link is missing');
            }
        });

        // check that there is a video preview, meaning that the video link is valid
        jContainer.find('.gray_rounded_block').each(function() {
            var jThis = $(this);
            if (jThis.find('img').is(':visible')) {
                showError(jThis.closest('.post_block').find('[name=videolink]'), 'Invalid video link');
            }
        });

        return valid;
    }

    var showError = function($input, error) {
        $input.addClass('input_error');
//        var $errorDiv = $input.next();
//        var $msgDiv = $input.siblings('.constraint');
//        if (!$errorDiv.hasClass('error')) {
//            $errorDiv = $("<div/>").addClass('error');
//            $input.after($errorDiv);
//        }
//        $errorDiv.text(error);
//        $msgDiv.hide();

        $input.on('focus change click', function() {
            $(this).removeClass('input_error').off();
//            $errorDiv.remove();
//            $msgDiv.show();
        })

        valid = false;
    }
};
var video = new VideoClass();

var nav = document.querySelector('#navigation');
if (nav) {
    var liArr = nav.childNodes;
    for (var i = 0; i < liArr.length; i++) {
        liArr[i].onclick = function() {
            video.changeNav(this)
        }
    }
}
nav.firstElementChild.click();

$("[name='_action_publish'], [name='_action_save']")
    .click(function(event) {
        try {
            if (video.validate(event)) {
                //preparePost();
                return true;
            }
        } catch (e) {
            console.error('Validation error', e.message)
            alert(e)
        }
        return false;
    });
