
$(document).ready(function(){
    $(".content_links .text").click(function() {
        addBlock("text", "textarea");
        return false;
    });
    $(".content_links .image").click(function() {
        addBlock("image", "input");
        return false;
    });
    $(".content_links .video").click(function() {
        addBlock("video", "input");
        return false;
    });
    $(".content_links .gif").click(function() {
        addBlock("gif", "input");
        return false;
    });
    $(".content_links .link").click(function() {
        addBlock("link", "input");
    });

    $('#cover_block input[type="file"]').prepareFileDropInput();

    $("[name='_action_preview'], [name='_action_saveDraft'], [name='_action_addPost']")
        .click(function(event) {
            try {
                if (validate(event)) {
                    preparePost();
                    return true;
                }
            } catch (e) {
                console.error('Validation error', e.message)
                alert(e)
            }
            return false;
        });


    $('#tags').tagsinput({
      cancelConfirmKeysOnEmpty: false
    });
    $('.tags-input input')
        .focus(function() {
            $('.tags-input').addClass('focused');
        })
        .blur(function() {
            $('.tags-input').removeClass('focused');
        });
});

function addBlock(type, inputTag) {
    var newBlock = $('.add_' + type).first().clone();
    newBlock.appendTo(".sortable").addClass("visible");

    var maxId = 0;
    $(".sortable").find(".visible").each(function () {
        if ($(this).data("blockType") === type) {
            maxId = Math.max($(this).data("idInType"), maxId);
        }
    });

    var idInType = maxId + 1;
    employBlock(newBlock, type, idInType, inputTag);
    return false;
}

// Add attributes and listeners to a block template copy
function employBlock($block, type, idInType, inputTag) {
    $block.data("blockType", type).data("idInType", idInType);
    $block.find(inputTag).attr("name", type + idInType);

    $block.find("[name='delete_block']").click(function() {
        $block.remove();
    });
    $block.find("input[type='file']").prepareFileDropInput();
}

// -----------------------------------
// Client side validation
// -----------------------------------
function validate(event) {

    var title = $('#title').val();
    if (!title) {
        showError($('#title'), 'Title is missing');
    } else if (title.length > MAX_POST_TITLE_SIZE) {
        showError($('#title'), 'Maximum title length is ' + MAX_POST_TITLE_SIZE);
    }

    var text = $('#text').val();
    if (!text) {
        showError($('#text'), 'Text is missing');
    } else if (text.length > MAX_TEXT_SIZE) {
        showError($('#text'), 'Maximum text length is ' + MAX_TEXT_SIZE);
    }

    var category = $('#category').val();
    if (!category || category === 'null') {
        showError($('#category').parent().find("button"), 'Please select a category');
    }

    var link = $('#source_link').val();
    if (link.length > MAX_LINK_SIZE) {
        showError($('#source_link'), 'Maximum link length is ' + MAX_LINK_SIZE);
    }

    var hasImage = false;
    $(".sortable").find(".visible").each(function () {
        var $this = $(this);
        var $input = $this.find("input,textarea").last();
        var type = $this.data("blockType");
        var idInType = $this.data("idInType");
        if (type == 'text' || type == 'video' || type == 'link') {
            if ($input.val().length > MAX_TEXT_SIZE) {
                showError($input, "Maximum text length is " + MAX_TEXT_SIZE);
            }
        }
        if (type == 'image' || type == 'video') {
            hasImage = true;
        }
    });

    // show errors
    var $errors = $('#post_errors');
    $errors.empty();
    if ($('.input_error').length) {
        $("<div/>").text('Some fields are invalid. Please fix.').appendTo($errors);
    }
    if (!hasImage) {
        $("<div/>").text('Please add at least one image or video.').appendTo($errors);
    }

    if (!$errors.children().length) {
        return true;
    } else {
        return false;
    }
}

function showError($input, error) {
    $input.addClass('input_error');
    var $errorDiv = $input.next();
    var $msgDiv = $input.siblings('.constraint');
    if (!$errorDiv.hasClass('error')) {
        $errorDiv = $("<div/>").addClass('error');
        $input.after($errorDiv);
    }
    $errorDiv.text(error);
    $msgDiv.hide();

    $input.on('focus change click', function() {
        $(this).removeClass('input_error').off();
        $errorDiv.remove();
        $msgDiv.show();
    })
}

function updateStructureInput() {
    var structure = new Set();
    $(".sortable").find(".visible").each(function () {
        var $this = $(this);
        var input = $this.find("input,textarea").last().get(0);
        // allow empty values, which is possible on post update
//        if (!input.value) {
//            return;
//        }
        var type = $this.data("blockType");
        var idInType = $this.data("idInType");
        structure.add({type: type, name: type + idInType, idInType: idInType});
    });
    $("[name='structure']").val(JSON.stringify(Array.from(structure)));
}

function preparePost() {
    updateStructureInput();
}

function setImage(file, target, event) {
    if (validateImage(event)) {
        set_background_file($(target).closest('.dashed_rounded_block'), file);
        return true;
    } else {
        return false;
    }
}

function set_image(event) {
    setImage(event.target.files[0], event.target, event);
}

function styleCategoryOption() {
    var $option = $(this);
    if ($option.attr('value') !== 'null') {
        $option.data('content', '<span style="color:#000;">' + $option.text() + '</span>');
    }
}