$(document).ready(function(){

	$('.about_block').contentEditable().change(function(e) {
		if ($(e.changedField).hasClass('nameedit')){
            $('.about_block').find('#name').val(getText(e.changed.name));
        }
        if ($(e.changedField).hasClass('textedit')){
            $('.about_block').find('#about').val(e.changed.about);
        }
	})
	$('[contenteditable]').on('paste', function(e) {
	    e.preventDefault();
        var text = e.originalEvent.clipboardData.getData("text/plain");
        document.execCommand("insertHTML", false, text);
	});

	$('div.author_name[contenteditable]').keydown(function(e) {
        if (e.keyCode === 13) {
            return false;
        }
      });

    $('.cancel_button').click(function(e) {
        $('input[type="password"]').val('');
    });


	$( function() {
		$( ".sortable" ).sortable();
		//$( ".sortable" ).disableSelection();
	} );

	$(document).click(function (e){
		var div = $(".submenu_trigger");
		if (!div.is(e.target)
			&& div.has(e.target).length === 0) {
			$(".submenu_trigger").removeClass("active");
		}
	});

	$(document).click(function (e){
		var div = $(".post_menu_trigger");
		if (!div.is(e.target)
			&& div.has(e.target).length === 0) {
			$(".post_menu ul").removeClass("active");
		}
	});

	if ($(window).width() > 991) {
		$(".top_menu .submenu_trigger .trg").click(function(){
			$(".top_menu .submenu_trigger").removeClass("active");
			$(this).parent('li').addClass("active");
		});
	}

	$(".usr.submenu_trigger").click(function(){
		$(".user_submenu").toggleClass("active");
		$(".top_menu").removeClass("visible_menu");
	});



	$(".post_menu_trigger").click(function(){
		$('.post_menu ul').toggleClass("active");
	});

	$(".post_block .close").click(function(){
		$(this).parent(".post_block").addClass("hidden");
	});


	$(".form_trigger").click(function(){
		$('.hidden_form').addClass("active").find("input").focus();
	});

	$(document).click(function (e){
		var div = $(".search");
		if (!div.is(e.target)
			&& div.has(e.target).length === 0) {
			$(".hidden_form").removeClass("active");
		}
	});

	$(document).click(function (e){
		var div = $(".usr.submenu_trigger");
		if (!div.is(e.target)
			&& div.has(e.target).length === 0) {
			$(".user_submenu").removeClass("active");
		}
	});

	$(".trigger").click(function(){
		$(".trigger").toggleClass("open");
		$(".navigation").toggleClass("white");
		$(".top_menu").toggleClass("visible_menu");
		$(".top_menu .submenu_trigger").removeClass("active");
	});

	$(document).click(function (e){
		var div = $("#menu");
		if (!div.is(e.target)
			&& div.has(e.target).length === 0) {
			$(".top_menu").removeClass("visible_menu");
		}
	});




	$(".top_menu :checkbox").click(function() {

		if($(".top_menu :checked").length > 3) {
			this.checked = false;
		}
		if($(".top_menu :checked").length >= 3) {
			$(".top_menu :checkbox +label").css('color', '#cccccc');
			$(".top_menu :checked +label").css('color', '#333333');
		}
		else{
			$(".top_menu .checkbox label").css('color', '#333333');
		}
		var snum  = $(".top_menu :checked").length;
		if( snum > 0){
			$(".snum").html(snum);
			$(".snum").addClass("checked");
		}
		if( snum == 0){
			$(".snum").html('&nbsp;');
			$(".snum").removeClass("checked");
		}

	});


	$('.content_links a').tooltip();
	$('.tt').tooltip();

	$('.fileinput').bootstrapFileInput();
	$('.commentfileinput').bootstrapFileInput();
	$('.editprofileimage').bootstrapFileInput();


	$(".clear_form").click(function() {
		$('input[type="text"]').val('').focus();
	});

//	$(".star").click(function() {
//		$(this).addClass("marked");
//		$(this).tooltip('show');
//	});
//	$(document).click(function (e){
//		var div = $(".star");
//		if (!div.is(e.target)
//			&& div.has(e.target).length === 0) {
//			$(".star").tooltip('hide');
//		}
//	});

//	$(".mark_read").click(function() {
//		$(".activities .check").addClass("hidden");
//		$(".top_menu .num").addClass("hidden");
//	});
//
//	$(".activities .cancel").click(function() {
//		$(this).parents(".d").addClass("hidden");
//	});
//	$(".activities .check").click(function() {
//		$(this).addClass('marked');
//		var nm = $(".top_menu .num").html()-1;
//		$(".top_menu .num").html(nm);
//	});


	$("#create_account").click(function() {
		$('#remind_pass').modal('hide');
		$('.tmenu a:last').tab('show');

	});

	$(".top_menu .register").click(function() {
		$('.tmenu a:last').tab('show');
	});

	$("#login_link").click(function() {
		$('#remind_pass').modal('hide');
	});

	$(".right_link .login").click(function() {
		$('.tmenu a:first').tab('show');
	});

	$(".right_link .add_post").click(function() {
		$('#login [name=nextAddPost]').val(1);
	});

	$("#remind_pass_link").click(function() {
		$('#login').modal('hide');
	});




	$( ".top_menu input[type='checkbox']" ).change(function(){
	 if($(".top_menu :checked").length==0) {
		$(".selected_categories").removeClass("visible");
	 }
	 else {
		$(".selected_categories").addClass("visible");
	 }
		if($("#facts").prop("checked")){
			$(".menu_facts").addClass('show')
		}
		else{
			$(".menu_facts").removeClass('show')
		}
		if($("#accident").prop("checked")){
			$(".menu_accident").addClass('show')
		}
		else{
			$(".menu_accident").removeClass('show')
		}
		if($("#cutes").prop("checked")){
			$(".menu_cutes").addClass('show')
		}
		else{
			$(".menu_cutes").removeClass('show')
		}
		if($("#cats").prop("checked")){
			$(".menu_cats").addClass('show')
		}
		else{
			$(".menu_cats").removeClass('show')
		}
		if($("#humor").prop("checked")){
			$(".menu_humor").addClass('show')
		}
		else{
			$(".menu_humor").removeClass('show')
		}
		if($("#dogs").prop("checked")){
			$(".menu_dogs").addClass('show')
		}
		else{
			$(".menu_dogs").removeClass('show')
		}
		if($("#video").prop("checked")){
			$(".menu_video").addClass('show')
		}
		else{
			$(".menu_video").removeClass('show')
		}
		if($("#animals").prop("checked")){
			$(".menu_animals").addClass('show')
		}
		else{
			$(".menu_animals").removeClass('show')
		}
		if($("#gifs").prop("checked")){
			$(".menu_gifs").addClass('show')
		}
		else{
			$(".menu_gifs").removeClass('show')
		}
		if($("#history").prop("checked")){
			$(".menu_history").addClass('show')
		}
		else{
			$(".menu_history").removeClass('show')
		}
		if($("#funny").prop("checked")){
			$(".menu_funny").addClass('show')
		}
		else{
			$(".menu_funny").removeClass('show')
		}
		if($("#photo").prop("checked")){
			$(".menu_photo").addClass('show')
		}
		else{
			$(".menu_photo").removeClass('show')
		}
		if($("#diy").prop("checked")){
			$(".menu_diy").addClass('show')
		}
		else{
			$(".menu_diy").removeClass('show')
		}
		if($("#travel").prop("checked")){
			$(".menu_travel").addClass('show')
		}
		else{
			$(".menu_travel").removeClass('show')
		}
		if($("#food").prop("checked")){
			$(".menu_food").addClass('show')
		}
		else{
			$(".menu_food").removeClass('show')
		}
		if($("#design").prop("checked")){
			$(".menu_design").addClass('show')
		}
		else{
			$(".menu_design").removeClass('show')
		}
		if($("#architecture").prop("checked")){
			$(".menu_architecture").addClass('show')
		}
		else{
			$(".menu_architecture").removeClass('show')
		}
		if($("#girls").prop("checked")){
			$(".menu_girls").addClass('show')
		}
		else{
			$(".menu_girls").removeClass('show')
		}
		if($("#nature").prop("checked")){
			$(".menu_nature").addClass('show')
		}
		else{
			$(".menu_nature").removeClass('show')
		}
		if($("#other").prop("checked")){
			$(".menu_other").addClass('show')
		}
		else{
			$(".menu_other").removeClass('show')
		}
	});

	$(".selected_categories .remove").click(function() {
		$(this).parent(".submenu_trigger").removeClass('active');
		$(this).parent("li").removeClass('show');
		if($(this).parent("li").hasClass("menu_facts")){
			$("#facts").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_accident")){
			$("#accident").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_cutes")){
			$("#cutes").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_cats")){
			$("#cats").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_dogs")){
			$("#dogs").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_humor")){
			$("#humor").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_dogs")){
			$("#dogs").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_video")){
			$("#video").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_animals")){
			$("#animals").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_gifs")){
			$("#gifs").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_history")){
			$("#history").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_funny")){
			$("#funny").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_photo")){
			$("#photo").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_diy")){
			$("#diy").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_travel")){
			$("#travel").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_food")){
			$("#food").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_design")){
			$("#design").attr('checked',false);

		}
		if($(this).parent("li").hasClass("menu_architecture")){
			$("#architecture").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_girls")){
			$("#girls").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_nature")){
			$("#nature").attr('checked',false);
		}
		if($(this).parent("li").hasClass("menu_other")){
			$("#other").attr('checked',false);
		}
		if($(".top_menu :checked").length == 0) {
			$(".selected_categories").removeClass("visible");
		}
		if($(".top_menu :checked").length >= 3) {
			$(".top_menu :checkbox +label").css('color', '#cccccc');
			$(".top_menu :checked +label").css('color', '#333333');
		}
		else{
			$(".top_menu .checkbox label").css('color', '#333333');
		}
	});

	if ($(window).width() < 991) {
	   $(".top_menu .submenu_trigger .trg").click(function(){
			$(this).parent('li').toggleClass("active");
		});
	}

    $(".top_menu .submenu_trigger.search_icon .trg").click(function(){
        $(this).parent().find("input").focus();
    });

    $(".has-error").on('focus change', function() {
       $(this).removeClass('has-error').off();
    })
});

function getText(html) {
    return $('<div>').html(html).text();
}

// Fix IE's indexOf Array
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (searchElement) {
		if (this == null) throw new TypeError();
		var t = Object(this);
		var len = t.length >>> 0;
		if (len === 0) return -1;
		var n = 0;
		if (arguments.length > 0) {
			n = Number(arguments[1]);
			if (n != n) n = 0;
			else if (n != 0 && n != Infinity && n != -Infinity) n = (n > 0 || -1) * Math.floor(Math.abs(n));
		}
		if (n >= len) return -1;
		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
		for (; k < len; k++) if (k in t && t[k] === searchElement) return k;
		return -1;
	}
}
// add hasClass support
if (!Element.prototype.hasClass) {
	Element.prototype.hasClass = function (classname) {
		if (this == null) throw new TypeError();
		return this.className.split(' ').indexOf(classname) !== -1;
	}
}

function approve_posts(event) {
    if ($(event.target).attr('checked')) {
        $('.ACTIVE').css('display: none');
    } else {
        $('.ACTIVE').css('display: block');
    }
}

function saveEditedValue(event) {

}

function allowUnfollow() {
	$('.subscribe_button button').mouseover(function(){
		$(this).html("Unfollow");
		$(this).css('color', '#a6afbd');
		$(this).css('border-color', '#a6afbd');
	}).mouseout(function(){
		$(this).html("Following");
		$(this).css('color', '#ff0f64');
		$(this).css('border-color', '#ff0f64');
	}).click(function() {
	    unfollow($(this).data("userid"));
	});
}

function set_background_file(div, file) {
    div.css('background-image', 'url(' + URL.createObjectURL(file) + ')');
}

function setImage(img, file) {
    img.css('content', 'url(' + URL.createObjectURL(file) + ')');
}

function validateImage(event) {
    var file = event.target.files[0]
    var target = event.target
    var fileNameLowerCase = file.name.toLowerCase();
    var valid = false;
    target.accept.toLowerCase().split(",").forEach(function(extension) {
        if (fileNameLowerCase.endsWith(extension)) {
            valid = true;
        }
    });
    if (!valid) {
        alert("Only " + target.accept + " files are accepted");
    } else {
        var isGif = fileNameLowerCase.endsWith(".gif");
        var maxSize = isGif ? MAX_GIF_SIZE : MAX_IMAGE_SIZE;
        if (file.size > maxSize) {
            valid = false;
            alert("Maximum file size for " + target.accept + " is " + (maxSize / 1024) + " kb");
        }
    }
    if (!valid) {
        target.value = null;
        event.preventDefault();
    }
    return valid;
}
