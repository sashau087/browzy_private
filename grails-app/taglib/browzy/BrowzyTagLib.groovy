package browzy

class BrowzyTagLib {

    def securityService

    def postLink = { attrs, body ->
        Post post = attrs.post
        String title = PostUtil.urlTitle(post)
        out << g.link(controller: 'post', action: 'view', id: post.id, params: [title: title], "class": attrs['class']) { body() }
    }

    def userLink = { attrs, body ->
        User user = attrs.user
        out << g.link(controller: 'user', action: 'profile', id: user.id, params: [name: user.username], "class": attrs['class']) { body() }
    }

    def titleHeader = { attrs, body ->
        String tag = params.titleHeader ?: 'h3'
        out << "<${tag}>${body()}</${tag}>"
    }

    def video = { attrs, body ->
        String url = body()
        if (url) {
            def videoInfo = extractVideoInfo(url)
            if (videoInfo.videoId) {
                out << """
                    <div class='embed-container'>
                        <iframe src='${videoInfo.baseUrl}/${videoInfo.videoId}' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>
                    </div>"""
            }
        }
    }

    def videoImage = { attrs, body ->
        String url = body()
        if (url) {
            def videoInfo = extractVideoInfo(url)
            if (videoInfo.videoId) {

                if (videoInfo.baseUrl.indexOf('youtube') >= 0) {
                    String res
                    switch (attrs.size) {
                        case 'small':
                        case 'medium':
                            res = 'mq'
                            break
                        case 'large':
                        default:
                            res = 'maxres'
                            break

                    }
                    out << """
                        <img src='https://img.youtube.com/vi/${videoInfo.videoId}/${res}default.jpg' class='${attrs['class']}'>
                    """

                } else if (videoInfo.baseUrl.indexOf('vimeo') >= 0) {

                    String res = 'small' == attrs.size ? 'medium' : 'large'
                    String id = res + videoInfo.videoId
                    out << """
                        <img name='${id}' class='${attrs['class']}'>
                        <script>
                            if (!window.img${id}Callback) {
                                window.img${id}Callback = function(response) {
                                    console.log('loaded ${id} at ' + new Date().getTime());
                                    if (response && response.length) {
                                        \$('[name=${id}]').attr('src', response[0].thumbnail_${res});
                                    }
                                }
                                \$.get('https://vimeo.com/api/v2/video/${videoInfo.videoId}.json', window.img${id}Callback);
                            }
                        </script>
                    """
                }
            }
        }
    }

    private extractVideoInfo(String url) {
        String baseUrl
        String videoId
        def match = (url =~ /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/)
        if (match.size() > 0) {
            videoId = match[0][7]
            baseUrl = 'https://www.youtube.com/embed'
        }

        if (!videoId) {
            match = (url =~ /^.*(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/)

            if (match.size() > 0) {
                videoId = match[0][3]
                baseUrl = 'https://player.vimeo.com/video'
            }
        }

        return [videoId: videoId, baseUrl: baseUrl]
    }

    def setGlobalVars = { attrs, body ->
        pageScope.currentUser = securityService.loggedInUser
    }

    def cutText = { attrs, body ->
        String fullText = body()
        String text
        int length = attrs.int('length')
        if (fullText.length() > length) {
            text = fullText.take(length)
            if (Character.valueOf(fullText.charAt(length)).isLetter() && text.contains(' ')) {
                text = text.take(text.lastIndexOf(' '))
            }
        }
        out << (text ?: fullText)
    }
}
