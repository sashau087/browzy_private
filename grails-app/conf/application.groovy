// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'browzy.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'browzy.UserRole'
grails.plugin.springsecurity.authority.className = 'browzy.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               	access: ['permitAll']],
	[pattern: '/error',          	access: ['permitAll']],
	[pattern: '/index',          	access: ['permitAll']],
	[pattern: '/index.gsp',      	access: ['permitAll']],
	[pattern: '/shutdown',       	access: ['permitAll']],
	[pattern: '/assets/**',      	access: ['permitAll']],
	[pattern: '/**/js/**',       	access: ['permitAll']],
	[pattern: '/**/css/**',      	access: ['permitAll']],
	[pattern: '/**/images/**',   	access: ['permitAll']],
	[pattern: '/**/favicon.ico', 	access: ['permitAll']],
	[pattern: '/user/profile',		access: ['ROLE_REGISTERED']],
	[pattern: '/user/profile/*',	access: ['ROLE_REGISTERED']],
	[pattern: '/user/edit',			access: ['ROLE_REGISTERED']],
	[pattern: '/user/edit/*',		access: ['ROLE_REGISTERED']],
	[pattern: '/post/add',			access: ['ROLE_REGISTERED']],
	[pattern: '/post/addPost',		access: ['ROLE_REGISTERED']],
	[pattern: '/post/preview',		access: ['ROLE_REGISTERED']],
	[pattern: '/post/saveDraft',	access: ['ROLE_REGISTERED']],
	[pattern: '/post/approve/*',	access: ['ROLE_ADMIN']],
	[pattern: '/user/authorize/*',	access: ['ROLE_ADMIN']],
	[pattern: '/user/login', 	 	access: ['permitAll']],
	[pattern: '/user/register',  	access: ['permitAll']],
	[pattern: '/user/avatar/*',  	access: ['permitAll']],
	[pattern: '/post/view/*', 	 	access: ['permitAll']],
	[pattern: '/post/image/*', 	 	access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]

grails.plugin.springsecurity.rejectIfNoRule = false
grails.plugin.springsecurity.fii.rejectPublicInvocations = false

grails.plugin.springsecurity.rememberMe.cookieName='browzy_remember_me'
grails.plugin.springsecurity.rememberMe.tokenValiditySeconds=31*24*60*60
grails.plugin.springsecurity.rememberMe.key='browzyUniqueCookieKeyDef'

grails.plugin.databasemigration.updateOnStart = true
grails.plugin.databasemigration.updateOnStartFileName = 'changelog.groovy'

mailgun{
	apiKey = 'key-905da06f03454661a08305cc71dca96d'
	domain = 'mg.browzy.com'

	message{
		defaultFrom = 'support@browzy.com'
		defaultSubject = 'Do not reply'
		format = 'html'
	}
}