import browzy.registration.VerificationTask

// Place your Spring DSL code here
beans = {
    verificationTask(VerificationTask){
        emailService = ref('emailService')
    }
}
