import browzy.*
import browzy.registration.VerificationToken
import org.hibernate.engine.spi.BatchFetchQueue
import org.hibernate.engine.spi.EntityKey

class BootStrap implements DbConst {

    def grailsApplication

    def sitemapService

    def init = { servletContext ->

        User admin = User.findByUsername(ADMIN)
        if (!admin) {
            admin = new User(
                    username: ADMIN,
                    password: "password",
                    email: "admin@browzy.com",
                    gender: "m",
                    country: "blr")
            admin.enabled = true
            admin.save(flush: true)
            VerificationToken.findByUser(admin)?.delete(flush: true)
            assert !VerificationToken.findAllByUser(admin)

            UserRole.create(admin, getRole(ROLE_REGISTERED))
            UserRole.create(admin, getRole(ROLE_AUTHOR))
            UserRole.create(admin, getRole(ROLE_ADMIN))
        }

        CATEGORIES.each({ category ->
            if (!Category.findByName(category)) {
                new Category(name: category).save(flush: true)
            }
        })

        User.list().each {
            println "User: " + it.username
        }

        sitemapService.checkSitemap()

        def oldRemoveBatchLoadableEntityKey = BatchFetchQueue.metaClass.&removeBatchLoadableEntityKey
        BatchFetchQueue.metaClass.removeBatchLoadableEntityKey = { EntityKey key ->
            return key ? oldRemoveBatchLoadableEntityKey.invoke(key) : null
        }
    }

    private Role getRole(String authority) {
        Role.findByAuthority(authority) ?: new Role(authority: authority).save()
    }

    def destroy = {
    }
}
