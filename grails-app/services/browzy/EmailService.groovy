package browzy

import browzy.registration.VerificationToken
import grails.plugin.mailgun.MailgunService
import grails.transaction.Transactional
import org.grails.web.servlet.mvc.GrailsWebRequest
import org.grails.web.util.WebUtils
import org.springframework.beans.factory.annotation.Value

import javax.servlet.http.HttpServletRequest

@Transactional
class EmailService {

    def mailService
    def groovyPageRenderer
    @Value('${grails.mail.username}')
    String fromMail
    int emailExpirationTimeInHours

    MailgunService mailgunService

    EmailService() {
        /*
        default value. Admin can set this parameter later.
         */
        emailExpirationTimeInHours = 24
    }

    void sendConfirmationEmail(User user) {
        def token = VerificationToken.findByUser(user)
        if (token) {
            String server = getServerUrl()
            String url = "${server}/registrationConfirm?token=${token.token}"
//            def content = getContentOfView('/mails/verificationEmail', [user: user, server: server, url: url])
//            sendEmail(user.email, content)
            sendEmail(user.email, '/mails/verificationEmail', [user: user, server: server, url: url])
            println 'sent registration verification email to ' + user.email
        } else {
            println 'no registration verification token'
        }
    }

    void sendForgotPasswordEmail(String email, String newPassword) {
//        def content = getContentOfView('/mails/forgotPasswordEmail', [newPassword: newPassword, server: getServerUrl()])
//        sendEmail(email, content)
        sendEmail(email, '/mails/forgotPasswordEmail', [newPassword: newPassword, server: getServerUrl()])
    }

    def checkUserEmails() {
        User.withTransaction { transaction ->
            List unverifiedUsers = VerificationToken.findAllByExpiryDateLessThan(new Date())
            List users = unverifiedUsers*.user
            if (unverifiedUsers.size() > 0 || users.size() > 0)
                try {
                    VerificationToken.deleteAll(unverifiedUsers)
                    UserRole.deleteAll(UserRole.findAllByUserInList(users))
                    User.deleteAll(users)
                } catch (Exception e) {
                    transaction.setRollbackOnly()
                    e.printStackTrace()
                }
        }
    }

    int getEmailExpirationTimeInHours() {
        return emailExpirationTimeInHours
    }

    void setEmailExpirationTimeInHours(int emailExpirationTimeInHours) {
        this.emailExpirationTimeInHours = emailExpirationTimeInHours
    }

    private String getServerUrl() {
        GrailsWebRequest webRequest = WebUtils.retrieveGrailsWebRequest()
        HttpServletRequest request = webRequest.getCurrentRequest()
        return "${request.scheme}://${request.serverName}${request.serverPort != 80 ? ':' + request.serverPort : ''}"
    }

    private void sendEmail(String userEmail, String template, params) {
//        mailService.sendMail {
//            async true
//            to userEmail
//            from fromMail
//            subject 'Do not reply'
//            html content
//        }
        mailgunService.sendMessage([
                'TO': userEmail,
                'SUBJECT': 'Do not reply',
                'BODY': params,
                'TEMPLATE': template
        ])
    }

    private String getContentOfView(String viewName, Map model) {
        return groovyPageRenderer.render(view: viewName, model: model)
    }

    def sendNotificationAboutCommentToPost(Comment commentToPost) {
        String authorEmail = commentToPost.post.author.email
//        String content = getContentOfView('/mails/notificationAboutCommentEmail', [
//                username : commentToPost.user,
//                postTitle: commentToPost.post.title,
//                commentText: commentToPost.text
//        ])
//        sendEmail(authorEmail, content)
        sendEmail(authorEmail, '/mails/notificationAboutCommentEmail', [
                username : commentToPost.user,
                postTitle: commentToPost.post.title,
                commentText: commentToPost.text
        ])
    }

    def sendNotificationAboutReplyToComment(Comment baseComment, Comment reply) {
        String authorEmail = baseComment.post.author.email
//        String content = getContentOfView('/mails/notificationAboutCommentEmail', [
//                username : reply.user,
//                baseCommentText: baseComment.text,
//                commentText: reply.text
//        ])
//        sendEmail(authorEmail, content)
        sendEmail(authorEmail, '/mails/notificationAboutCommentEmail', [
                username : reply.user,
                baseCommentText: baseComment.text,
                commentText: reply.text
        ])
    }

    def sendNotificationAboutSuccessStory(Post post) {
        String authorEmail = post.author.email
//        String content = getContentOfView('/mails/notificationAboutStorySuccess', [
//                postTitle : post.title
//        ])
//        sendEmail(authorEmail, content)
        sendEmail(authorEmail, '/mails/notificationAboutStorySuccess', [
                postTitle : post.title
        ])
    }

}
