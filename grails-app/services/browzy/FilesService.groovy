package browzy

import grails.io.IOUtils
import grails.transaction.Transactional

@Transactional
class FilesService {

    def grailsApplication

    def saveFile(Long userId, String name, String contentType, byte[] content) {

        File dir = new File(grailsApplication.config.browzy.userFilesDir + "/" + userId)
        dir.mkdirs()
        File file = new File(dir, "${System.currentTimeMillis()}_${name}.${contentType}")
        IOUtils.copy(content, file)

        return file.absolutePath
    }

    def deleteFile(String path) {
        new File(path).delete()
    }

    def deletePostFiles(Long userId, postId) {
        File dir = new File(grailsApplication.config.browzy.userFilesDir + "/" + userId + "/posts/" + postId)
        if (dir.exists()) {
            dir.deleteDir()
        }
    }
}
