package browzy

import grails.transaction.Transactional

@Transactional
class MeterService {

    def springSecurityService

    /**
     * Просмотры - количество общих просмотров постов данного автора.
     При достижении 10 000, заменяем тысячи на K.
     * @return
     */
    def viewNumber() {
        User currentUser = springSecurityService.getCurrentUser()
        List posts = Post.findAllByAuthor(currentUser)
        int number = Counter.countByPostInListAndLooked(posts, true)
        if(number>10000){
            return "${(number/1000 as int)}K"
        }
        else return number
    }

    /**
     * Лайки. отображают сколько лайков было поставлено на публикации автора.
     * @param post
     * @return
     */
    def likeNumber(String postId) {
        Post post = Post.get(Long.valueOf(postId))
        return likeNumber(post)
    }
    def likeNumber(Post post) {
        return Counter.countByPostAndLiked(post, true)
    }

    /**
     * Дизлайки. отображают сколько дизлайков было поставлено на публикации автора.
     * @param post
     * @return
     */
    def dislikeNumber(String postId) {
        Post post = Post.get(Long.valueOf(postId))
        return dislikeNumber(post)
    }
    def dislikeNumber(Post post) {
        return Counter.countByPostAndLiked(post, false)
    }

    /**
     * Комментарии http://joxi.ru/4Ak85qMuMWWbWr количество комментариев автора. Те же что и в разделе комменты,
     только тут счетчик а там сами комменты.
     * @return
     */
    def commentNumber(){
        User currentUser = springSecurityService.getCurrentUser()
        return Comment.countByUser(currentUser)
    }

    /**
     * Шаринг. http://joxi.ru/8AnpdqQuqYYwNm сколько постов автора было расшарено.
     * @return
     */
    def shareNumber(){
        User currentUser = springSecurityService.getCurrentUser()
        List posts = Post.findAllByAuthor(currentUser)
        return Counter.countByPostInListAndShared(posts, true)
    }

    /**
     * Избранное. http://joxi.ru/8AnpdqQuqYYwNm сколько постов автора было добавлено в избранные.
     * @return
     */
    def starNumber(){
        User currentUser = springSecurityService.getCurrentUser()
        List posts = Post.findAllByAuthor(currentUser)
        return Counter.countByPostInListAndStared(posts, true)
    }
}
