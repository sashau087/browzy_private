package browzy

import grails.converters.JSON
import grails.transaction.Transactional
import groovy.json.JsonBuilder
import groovy.sql.Sql
import org.grails.web.json.JSONObject

@Transactional
class CommentService {

    def springSecurityService

    /**
     *
     * @param id post id
     * @param text
     * @return
     */
    def commentPost(Long id, String text, DbFile image) {
        Post post = Post.findById(id)
        User user = springSecurityService.getCurrentUser()
        def comment = new Comment(post: post, text: text, user: user, avatar: image, status_read: 'unread')
        comment.save()
        return comment.id
    }

    def commentMore(Long postId, Long lastId) {
        List comments = Comment.executeQuery("select id, date, text, user.username, user.id from Comment where post_id=:postId and id<:lastId order by id desc", [postId: postId, lastId: lastId, max: 3])
        return comments
    }

    /**
     *
     * @param id reply id
     * @param text
     * @return
     */
    def commentAnotherComment(Long postId, Long id, String text, DbFile image) {
        Post post = Post.findById(postId)
        Comment sourceComment = Comment.findById(id)
        User user = springSecurityService.getCurrentUser()
        def comment=sourceComment.addToReplies(post: post, text: text, user: user, avatar: image, status_read: 'unread')
        def x=comment.save(flush: true)
        def idLast=x.replies.getAt(0).id
        for(int i=0; i<x.replies.size(); i++){
            System.out.print(", "+x.replies.getAt(i).id.toString())
            if(idLast<x.replies.getAt(i).id){
                idLast=x.replies.getAt(i).id
            }
        }
        System.out.println("sss2 "+idLast)
        System.out.println("sss3 "+x.replies.id.toString())
        return idLast
    }
}
