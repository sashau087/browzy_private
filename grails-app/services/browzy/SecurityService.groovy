package browzy

import grails.transaction.Transactional
import org.grails.web.servlet.mvc.GrailsWebRequest
import org.grails.web.util.WebUtils
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder

@Transactional
class SecurityService {

    def springSecurityService
    def authenticationManager
    def logoutHandlers
    def rememberMeServices
    def passwordEncoder

    void signIn(String username, String password) {
        try {
            GrailsWebRequest webRequest = WebUtils.retrieveGrailsWebRequest()
            def request = webRequest.getCurrentRequest()
            def response = webRequest.getCurrentResponse()
            def authentication = new UsernamePasswordAuthenticationToken(username, password)
            SecurityContextHolder.context.authentication = authenticationManager.authenticate(authentication)
            def remember = webRequest.params."remember-me"
            if (remember) {
                rememberMeServices.onLoginSuccess(request, response, springSecurityService.getAuthentication())
            }
        } catch (BadCredentialsException e) {
            println "invalid username/password for " + username
            throw new SecurityException("Invalid username/password", e)
        }
    }

    void signOut() {
        def auth = SecurityContextHolder.context.authentication
        if (auth) {
            GrailsWebRequest webRequest = WebUtils.retrieveGrailsWebRequest()
            def request = webRequest.getCurrentRequest()
            def response = webRequest.getCurrentResponse()
            logoutHandlers.each { handler ->
                handler.logout(request, response, auth)
            }
        }
    }

    boolean isUserLoggedIn() {
        springSecurityService.isLoggedIn()
    }

    User getLoggedInUser() {
        springSecurityService.getCurrentUser()
    }

    Boolean isAdminLoggedIn() {
        return getLoggedInUser()?.hasRole(DbConst.ROLE_ADMIN)
    }

    void loginUser(User user) {
        springSecurityService.reauthenticate(user.username)
    }

    boolean isPasswordValid(User user, String password) {
        passwordEncoder.isPasswordValid(user.password, password, null)
    }
}
