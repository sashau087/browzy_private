package browzy

import grails.converters.JSON
import grails.transaction.Transactional
import org.grails.web.json.JSONArray

//@Transactional
class PostService implements Const, DbConst {

    static transactional = false

    static final String SEQUENCE_TOP_PRIORITY = 'topPriority'

    static final int MAX_TOP_PRIORITY = 3

    def filesService
    def imageService
    def notificationService

    /**
     * Get post by id, create or update post depending on params.
     * @param user current user
     */
    Post getPost(params, User user) {

        if (params.id) {
            Post post = Post.get(params.long('id'))
            if (post && params.title) {
                // assume that if params have title then there must be other params and the post should be updated
                updatePost(params, post, user)
            }
            return post
        }

        Post post = new Post(
                title: params.title,
                text: params.text,
                structure: params.structure,
                tags: getTagsFromString(params.tags),
                categories: params.category && params.category != 'null' ? [Category.load(params.category)] : [],
                link: params.link
        )

        post.author = user
        post.status = PostStatus.DRAFT

        if (params.cover && !params.cover.empty) {
            post.cover = new DbFile(
                name: 'cover',
                content: params.cover.bytes,
                contentType: params.cover.contentType,
                fileName: params.cover.filename)
        }

        JSONArray structure = JSON.parse(params.structure) as JSONArray
        def invalidBlocks = []
        structure.each { block ->
            if (block.type == 'text' || block.type == 'link' || block.type == 'video') {
                Text text = new Text(name: block.name)
                text.content = params[block.name]
                if (!text.validate()) {
                    text.errors.allErrors.each { println it }
                }
                post.addToTexts(text)
            } else if (block.type == 'image') {
                def file = params[block.name]
                if (!file.empty &&
                        (file.contentType == 'image/jpeg' || file.contentType == 'image/png')) {
                    addOrUpdateImage(post, file, block.name)
                    addOrUpdateCutImageIfRequired(post, file, block.name)
                } else {
                    invalidBlocks.add(block.name)
                }
            }
        }

        structure.each { block ->
            if (block.type == 'gif') {
                def file = params[block.name]
                if (!file.empty && file.contentType == 'image/gif') {
                    String path = filesService.saveFile(user.id, block.name, file.contentType.split('/')[1], file.bytes)
                    DiskFile diskFile = new DiskFile(
                            name: block.name,
                            fileName: file.filename,
                            path: path,
                            contentType: file.contentType)
                    post.addToVideos(diskFile)
                } else {
                    invalidBlocks.add(block.name)
                }
            }
        }

        if (!invalidBlocks.isEmpty()) {
            structure.removeIf { block -> invalidBlocks.contains(block.name) }
            post.structure = structure.toString()
            println 'new structure: ' + post.structure
        }

        return post
    }

    /**
     * Update post with params.
     * @param params request params
     * @param user current user, post author
     * @return post - updated or just gotten by id or null if not found
     */
    Post updatePost(params, Post post, User user) {

        post.title = params.title
        post.text = params.text
        post.tags = getTagsFromString(params.tags)
        post.categories = params.category && params.category != 'null' ? [Category.load(params.category)] : []
        post.link = params.link
        post.structure = params.structure

        post.author = user

        if (params.cover && !params.cover.empty) {
            post.cover = post.cover ?: new DbFile()
            post.cover.with {
                name = 'cover'
                content = params.cover.bytes
                contentType = params.cover.contentType
                fileName = params.cover.filename
            }
        } else {
            // cover remove is not supported yet
            // allow empty, which can be on post update
//            post.cover?.delete()
        }

        JSONArray structure = JSON.parse(params.structure) as JSONArray

        def invalidBlocks = []
        structure.each { block ->
            if (block.type == 'text' || block.type == 'link' || block.type == 'video') {
                Text text = post.texts?.find({ it.name == block.name })
                if (text) {
                    text.content = params[block.name]
                } else {
                    text = new Text(name: block.name, content: params[block.name])
                    post.addToTexts(text)
                }

            } else if (block.type == 'image') {
                def file = params[block.name]
                if (!file.empty) {
                    if (file.contentType == 'image/jpeg' || file.contentType == 'image/png') {
                        addOrUpdateImage(post, file, block.name)
                        addOrUpdateCutImageIfRequired(post, file, block.name)
                    } else {
                        invalidBlocks.add(block.name)
                    }
                }
            } else if (block.type == 'gif') {
                def file = params[block.name]
                if (!file.empty) {
                    if (file.contentType == 'image/gif') {
                        DiskFile diskFile = post.videos.find({ it.name == block.name })
                        if (diskFile) {
                            filesService.deleteFile(diskFile.path)
                        } else {
                            diskFile = new DiskFile(name: block.name)
                            post.addToVideos(diskFile)
                        }
                        String path = filesService.saveFile(user.id, block.name, file.contentType.split('/')[1], file.bytes)
                        diskFile.fileName = file.filename
                        diskFile.path = path
                        diskFile.contentType = file.contentType
                    } else {
                        invalidBlocks.add(block.name)
                    }
                }
            }
        }

        if (!invalidBlocks.isEmpty()) {
            structure.removeIf { block -> invalidBlocks.contains(block.name) }
            post.structure = structure.toString()
            println 'new structure: ' + post.structure
        }

        // remove obsolete parts
        post.texts?.each { text ->
            if (!structure.find { it.type in ['text', 'link', 'video'] && it.name == text.name }) {
                post.removeFromTexts(text)
                text.delete()
            }
        }
        post.images.each { image ->
            if (!structure.find { it.type == 'image' && it.name == image.name }) {
                post.removeFromImages(image)
                image.delete()
            }
        }
        post.videos.each { diskFile ->
            if (!structure.find { it.type in ['gif'] && it.name == diskFile.name }) {
                post.removeFromVideos(diskFile)
                diskFile.delete()
                filesService.deleteFile(diskFile.path)
            }
        }

        return post
    }

    private Post getPostByParam(postParam) {
        if (!postParam) {
            return null
        } else if (postParam instanceof Post) {
            return postParam
        } else if (Long.isAssignableFrom(postParam.getClass())) {
            return Post.get((Long) postParam)
        } else {
            return null
        }
    }

    def getTagsFromString(String string) {
        return string?.tokenize(',')?.collect {
            String tagText = it.trim().toLowerCase()
            Tag.findByText(tagText) ?: new Tag(text: tagText)
        }
    }

    void addOrUpdateImage(Post post, file, String blockName) {
        DbFile image = post.images?.find({ it.name == blockName })
        if (!image) {
            image = new DbFile(name: blockName)
            post.addToImages(image)
        }
        image.content = file.bytes
        image.contentType = file.contentType
        image.fileName = file.filename
    }

    void addOrUpdateCutImageIfRequired(Post post, file, String blockName) {
        ScaledImage image = post.scaledImages?.find({ it.name == blockName })
        byte[] scaled = imageService.scale(file.bytes, IMAGE_SCALED_WIDTH, 0, file.contentType)
        if (scaled.length == file.bytes.length) {
            if (image) {
                post.removeFromScaledImages(image)
                image.delete()
            }
            return
        }
        if (!image) {
            image = new ScaledImage(name: blockName)
            post.addToScaledImages(image)
        }
        image.width = IMAGE_SCALED_WIDTH
        image.content = scaled
        image.contentType = file.contentType
        image.fileName = file.filename
    }

    def delete(postArg) {
        Post post = getPostByParam(postArg)
        if (post) {
            Post.withTransaction { transaction ->
                try {
                    post.cover?.delete()
                    post.texts?.each { it.delete() }
                    post.images?.each { it.delete() }
                    post.scaledImages?.each { it.delete() }
                    filesService.deletePostFiles(post.author.id, post.id)
                    post.videos?.each { it.delete() }
                    post.comments?.each { it.delete() }
                    post.counters?.each { it.delete() }
                    post.delete(failOnError: true)
                } catch (Exception e) {
                    e.printStackTrace()
                    transaction.setRollbackOnly()
                    throw e
                }
            }
        } else {
            println 'Post to delete is not found'
        }
    }

    Post setStatus(postArg, PostStatus status, boolean save) {
        Post post = getPostByParam(postArg)
        if (!post) {
            return null
        }
        post.status = status
        if (status == PostStatus.ACTIVE || status == PostStatus.PAID) {
            if (!post.approved) {
                post.approved = new Date()
                notificationService.notifyAboutSuccessStory(post)
            }
        }
        if (save) {
            post.save(failOnError: true)
        }
        return post
    }

    def setPriority(long postId, int priority) {
        def sequencePosts = SequencePost.findAllBySequence(SEQUENCE_TOP_PRIORITY, [sort: 'position'])
        SequencePost existingPost = sequencePosts.find { it.post.id == postId }
        if (existingPost) {
            sequencePosts.remove(existingPost)
            existingPost.delete()
        }
        if (priority > 0) {
            Post post = Post.load(postId)
            sequencePosts.add(Math.min(priority - 1, sequencePosts.size()), new SequencePost(sequence: SEQUENCE_TOP_PRIORITY, post: post))
            for (int i = 0; i < sequencePosts.size(); i++) {
                SequencePost sequencePost = sequencePosts[i]
                if (sequencePost) {
                    if (i < MAX_TOP_PRIORITY) {
                        sequencePost.position = i + 1
                        sequencePost.save()
                    } else {
                        sequencePost.delete()
                    }
                }
            }
        }
    }

    def topPriority() {
        SequencePost.findAllBySequence(SEQUENCE_TOP_PRIORITY, [sort: 'position', max: MAX_TOP_PRIORITY])*.post
    }

    /**
     * Find posts by status.
     */
    def timeline(Boolean isAdmin) {
        def posts
        if (isAdmin) {
            posts = Post.withCriteria {
                eq 'status', PostStatus.APPROVE
                order 'created', 'desc'
            }
        } else {
            posts = []
        }
        def approvedPosts = Post.withCriteria {
            inList 'status', [PostStatus.ACTIVE, PostStatus.PAID]
            order 'approvedDay', 'desc'
            order 'approved', 'desc'
            order 'created', 'desc'
        }
        posts += approvedPosts
        return posts
    }

    def timelinePage(Boolean isAdmin, int start, int limit, def topPostsIds) {
//        println "----------------- timeline page --------------------"
//        println "start: " + start
//        println "limit: " + limit
        def posts
        if (isAdmin) {
            posts = Post.withCriteria {
                eq 'status', PostStatus.APPROVE
                order 'created', 'desc'
            }
        } else {
            posts = []
        }
//        println "not approved: " + posts.size()
        if (posts.size() > start) {
            limit = start + limit - posts.size()
            start = 0
        } else {
            start -= posts.size()
            posts.clear()
        }
//        println "new start: " + start
//        println "new limit: " + limit
//        3 3
//        0 1 2 | 3 4  |
//        0 1
        int minApprovedCount = 3 - topPostsIds.size()
        if (limit > 0 || minApprovedCount > 0) {
            def approvedPosts = Post.withCriteria {
                not {
                    inList 'id', topPostsIds
                }
                inList 'status', [PostStatus.ACTIVE, PostStatus.PAID]
                order 'approvedDay', 'desc'
                order 'approved', 'desc'
                order 'created', 'desc'
                maxResults Math.max(limit, minApprovedCount)
                firstResult start
            }
            posts += approvedPosts
        }
        return posts
    }

    /**
     * Get all posts.
     */
    def listAllPosts(Map params) {
        String sort = params.sort
        String order = params.order
        Map sortParams
        if (!sort) sort = 'title'
        if (!order) order = 'asc'
        sortParams = [sort: sort, order: order]
        Post.list(sortParams)
    }

    def listUserPosts(User user, PostStatus status) {
        return Post.withCriteria {
            eq 'author', user
            eq 'status', status
            order 'approvedDay', 'desc'
            order 'approved', 'desc'
            order 'created', 'desc'
        }
    }

    def trending(params) {
        String query = "select c.post, sum(c.looks) as looksSum from Counter c group by c.post order by looksSum desc"
        return (params ? Counter.executeQuery(query, params) : Counter.executeQuery(query))
                .collect { it[0] }
    }

    def latest(params) {
        def fullParams = [sort: 'approved', order: 'desc']
        if (params) {
            fullParams += params
        }
        return Post.findAllByStatus(PostStatus.ACTIVE, fullParams)
    }

    def next(Post post) {
        if (post.approved) {
            Post nextPost = Post.find("from Post as p where p.approved > ? order by p.approved", [post.approved], [max: 1])
            if (!nextPost) {
                nextPost = Post.find("""
                    from Post as p 
                    where p.approved = (
                        select min(approved) from Post
                    ) and p.id != :postId""", [postId: post.id])
            }
            return nextPost
        } else {
            return null
        }
    }

    def previous(Post post) {
        if (post.approved) {
            Post prevPost = Post.find("from Post as p where p.approved < ? order by p.approved desc", [post.approved], [max: 1])
            if (!prevPost) {
                prevPost = Post.find("""
                    from Post as p 
                    where p.approved = (
                        select max(approved) from Post
                    ) and p.id != :postId""", [postId: post.id])
            }
            return prevPost
        } else {
            return null
        }
    }

    def similar(Post post, params = null) {
        if (post.tags && !post.tags.empty) {
            String query =
                    """select p, count(t.id) as tagCount
                        from Post as p
                        inner join p.tags as t
                        where t.id in (:tagsIds) and p.id != :postId and p.status in (:statuses)
                        group by p.id
                        order by tagCount desc"""
            def postsWithTagCount
            def tagsIds = post.tags.collect { it.id }
            if (params) {
                postsWithTagCount = Tag.executeQuery(query, [postId: post.id, tagsIds: tagsIds, statuses: PUBLIC_POST_STATUSES], params)
            } else {
                postsWithTagCount = Tag.executeQuery(query, [postId: post.id, tagsIds: tagsIds, statuses: PUBLIC_POST_STATUSES])
            }
            //println "similar: " + postsWithTagCount
            return postsWithTagCount.collect { it[0] }
        } else {
            return null
        }
    }

    def similarOrRandom(Post post, int retCount) {
        def posts
        if (post.tags && !post.tags.empty) {
            posts = similar(post, [max: retCount])
        }
        if (!posts) {
            posts = []
        }
        if (posts.size < retCount) {
            def randomPosts = Post.findAll("from Post as p where p not in (:posts) and p.status in (:statuses)",
                    [posts: posts + post, statuses: PUBLIC_POST_STATUSES], [max: retCount - posts.size])
            //println "random: " + randomPosts
            posts += randomPosts
        }
        return posts
    }

    def category(String name) {
        return Post.withCriteria {
            categories {
                ilike 'name', name
            }
        }
    }

    def byAuthor(User user) {
        return Post.findAllByAuthorAndStatusInList(user, [PostStatus.ACTIVE, PostStatus.PAID])
    }

    def staredPosts(User user) {

    }
}
