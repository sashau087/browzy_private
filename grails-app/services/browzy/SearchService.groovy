package browzy

import grails.transaction.Transactional

@Transactional
class SearchService {

    /**
     * @param searchString будет разбита по пробелам и запятым и все слова использоваться как ключи к поиску в заголовках
     * @return список постов
     */
    def searchPostsByTitle(String searchString) {
        String[] words = searchString.split(/[,\s]+/)
        return Post.withCriteria {
            or {
                words.each {
                    ilike('title', "%$it%")
                }
            }
        }
    }

    def searchPostsByTags(tagsList) {
        return Post.withCriteria {
            tags {
                or {
                    tagsList.each {
                        ilike 'text', "%$it%"
                    }
                }
            }
        }
    }

    def searchPostsByKeywords(keywordsList) {
        Set posts = Post.withCriteria {
            or {
                keywordsList.each {
                    ilike 'title', "%$it%"
                    ilike 'text', "%$it%"
                }
            }
        }
        def textsPosts = Post.withCriteria {
            texts {
                or {
                    keywordsList.each {
                        ilike 'content', "%$it%"
                    }
                }
            }
        }
        def tagsPosts = Post.withCriteria {
            tags {
                or {
                    keywordsList.each {
                        ilike 'text', "%$it%"
                    }
                }
            }
        }
        def categoryPosts = Post.withCriteria {
            categories {
                or {
                    keywordsList.each {
                        ilike 'name', "%$it%"
                    }
                }
            }
        }
        return posts + textsPosts + tagsPosts + categoryPosts
    }
}
