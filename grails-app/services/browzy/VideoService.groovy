package browzy

import grails.converters.JSON

class VideoService {

    def postService

    def save(params, User user, PostStatus status) {

        def posts = []

        Post.withTransaction { transaction ->
            int size = params.title.length
            for (int i = 0; i < size; i++) {
                println params.title[i]
                println params.text[i]
                println params.videolink[i]
                println params.tags[i]
                println params.source[i]

                def structure = [[type: 'video', name: 'video1', idInType: 1]]

                Post post = new Post(
                        title: params.title[i],
                        text: params.text[i],
                        structure: (structure as JSON).toString(),
                        tags: postService.getTagsFromString(params.tags[i]),
                        categories: [Category.findByName('Video')],
                        link: params.source[i]
                )

                post.type = 'video' + (i + 1)

                post.author = user

                postService.setStatus(post, status, false)

                Text text = new Text(name: 'video1')
                text.content = params.videolink[i]
                post.addToTexts(text)

                post.save(failOnError: true)

                posts << post
            }
        }
        return posts
    }
}
