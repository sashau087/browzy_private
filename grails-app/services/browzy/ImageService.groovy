package browzy

import javax.imageio.ImageIO
import java.awt.Color
import java.awt.Image
import java.awt.image.BufferedImage

class ImageService {

    /**
     * Scales image down uniformly depending on width or height if width is not specified (0 or negative).
     * If scaled image is bigger than the original then returns original.
     */
    byte[] scale(byte[] fileData, int width, int height, String contentType) {
        ByteArrayInputStream inputStream
        ByteArrayOutputStream outputStream
        try {
            inputStream = new ByteArrayInputStream(fileData)
            BufferedImage img = ImageIO.read(inputStream)
            if (width > 0) {
                height = (width * img.getHeight()) / img.getWidth()
            } else if (height > 0) {
                width = (height * img.getWidth()) / img.getHeight()
            } else {
                return fileData
            }
            if (width >= img.getWidth()) {
                return fileData
            }
            Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH)
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null)

            outputStream = new ByteArrayOutputStream()

            ImageIO.write(imageBuff, contentType.contains('png') ? 'png' : 'jpg', outputStream)

            byte[] result = outputStream.toByteArray()
            return result
        } catch (IOException e) {
            e.printStackTrace()
        } finally {
            inputStream?.close()
            outputStream?.close()
        }
    }
}
