package browzy

import browzy.notification.NotificationType
import grails.transaction.Transactional

@Transactional
class NotificationService {

    def emailService
    def securityService

    def turnNotificationAboutCommentPost(Boolean toggle) {
        String type = 'NotificationAboutCommentPost'
        changeNotificationStatus(toggle, type)
    }

    def checkNotificationAboutCommentPost() {
        String type = 'NotificationAboutCommentPost'
        return getNotificationStatus(type)
    }

    boolean getNotificationStatus(String type, User user = null) {
        user = user ?: securityService.loggedInUser
        user?.notificationTypes?.find { it.name == type }
    }

    def turnNotificationAboutReplyToComment(Boolean toggle) {
        String type = 'NotificationAboutReplyToComment'
        changeNotificationStatus(toggle, type)
    }

    def checkNotificationAboutReplyToComment() {
        String type = 'NotificationAboutReplyToComment'
        return getNotificationStatus(type)
    }

    def turnNotificationAboutSuccessStory(Boolean toggle) {
        String type = 'NotificationAboutSuccessStory'
        changeNotificationStatus(toggle, type)
    }

    def checkNotificationAboutSuccessStory(User user) {
        String type = 'NotificationAboutSuccessStory'
        return getNotificationStatus(type, user)
    }

    def notifyAboutReplyToComment(Comment baseComment, Comment reply) {
        if (checkNotificationAboutReplyToComment())
            emailService.sendNotificationAboutReplyToComment(baseComment, reply)
    }

    def notifyAboutCommentPost(Comment comment) {
        if (checkNotificationAboutCommentPost())
            emailService.sendNotificationAboutCommentToPost(comment)
    }

    def notifyAboutSuccessStory(Post post) {
        if (checkNotificationAboutSuccessStory(post.author)) {
            emailService.sendNotificationAboutSuccessStory(post)
        }
    }

    def changeNotificationStatus(Boolean toggle, String name) {
        User user = securityService.loggedInUser
        NotificationType type = NotificationType.findOrSaveWhere(name: name)
        if (toggle)
            user?.notificationTypes?.add(type)
        else
            user?.notificationTypes?.remove(type)
        user.save(flush: true)
    }


}
