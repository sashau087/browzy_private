package browzy

import groovy.util.slurpersupport.NodeChild
import groovy.xml.MarkupBuilder
import groovy.xml.XmlUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

class SitemapService {

    Logger logger = LoggerFactory.getLogger(SitemapService.class)

    static final String SERVER = 'https://browzy.com/'

    static final String ENCODING_UTF8 = "UTF-8"

    def grailsApplication

    def checkSitemap() {
        synchronized (SitemapService.class) {
            try {
                File sitemap = getSitemapIndex()
                if (!sitemap.exists()) {
                    createNewSitemap(sitemap)
                }
            } catch (Exception e) {
                logger.error('Error in checkSitemap', e)
            }
        }
    }

    def postCreated(Post post) {
        synchronized (SitemapService.class) {
            try {
                File sitemapIndex = getSitemapIndex()
                if (!sitemapIndex.exists()) {
                    createNewSitemap(sitemapIndex)
                } else {

                    def sitemapindex = new XmlSlurper(false, false).parse(sitemapIndex)

                    File sitemap = getSitemap(1)
                    if (!sitemap.exists()) {
                        createNewSitemap(sitemapIndex)
                    } else {
                        String newNode = withStringWriter { writer ->
                            writePost(createMarkupBuilder(writer), post)
                        }
                        XmlSlurper xmlSlurper = new XmlSlurper(false, false)
                        def urlset = xmlSlurper.parse(new GZIPInputStream(new FileInputStream(sitemap)))
                        urlset.appendNode(xmlSlurper.parseText(newNode))

                        withZippedFileWriter sitemap, { writer ->
                            XmlUtil.serialize(urlset, writer)
                        }

                        sitemapindex.children().last().lastmod.replaceBody(new Date().format("yyyy-MM-dd"))
                        withFileWriter sitemapIndex, { writer ->
                            XmlUtil.serialize(sitemapindex, writer)
                        }
                    }
                }
            } catch (Exception e) {
                logger.error('Error in postCreated', e)
            }
        }
    }

    def postUpdated(Post post) {
        synchronized (SitemapService.class) {
            try {
                File sitemapIndex = getSitemapIndex()
                if (!sitemapIndex.exists()) {
                    createNewSitemap(sitemapIndex)
                } else {

                    def sitemapindex = new XmlSlurper(false, false).parse(sitemapIndex)

                    File sitemap = getSitemap(1)
                    if (!sitemap.exists()) {
                        createNewSitemap(sitemapIndex)
                    } else {
                        XmlSlurper xmlSlurper = new XmlSlurper(false, false)
                        def urlset = xmlSlurper.parse(new GZIPInputStream(new FileInputStream(sitemap)))
                        String postUrlSuffix = "/${post.id}"
                        boolean changed = false
                        for (NodeChild url : urlset.children()) {
                            if (url.loc.text().endsWith(postUrlSuffix)) {
                                url.loc.replaceBody(getPostUrl(post))
                                changed = true
                                break
                            }
                        }
                        if (!changed) {
                            String newNode = withStringWriter { writer ->
                                writePost(createMarkupBuilder(writer), post)
                            }
                            urlset.appendNode(xmlSlurper.parseText(newNode))
                        }

                        withZippedFileWriter sitemap, { writer ->
                            XmlUtil.serialize(urlset, writer)
                        }

                        sitemapindex.children().last().lastmod.replaceBody(new Date().format("yyyy-MM-dd"))
                        withFileWriter sitemapIndex, { writer ->
                            XmlUtil.serialize(sitemapindex, writer)
                        }
                    }
                }
            } catch (Exception e) {
                logger.error('Error in postUpdated', e)
            }
        }
    }

    def postDeleted(id) {
        synchronized (SitemapService.class) {
            try {

                File sitemapIndex = getSitemapIndex()
                if (!sitemapIndex.exists()) {
                    createNewSitemap(sitemapIndex)
                } else {

                    File sitemap = getSitemap(1)
                    if (!sitemap.exists()) {
                        return
                    }

                    def sitemapindex = new XmlSlurper(false, false).parse(sitemapIndex)

                    XmlSlurper xmlSlurper = new XmlSlurper(false, false)
                    def urlset = xmlSlurper.parse(new GZIPInputStream(new FileInputStream(sitemap)))
                    String postUrlSuffix = "/${id}"
                    boolean deleted = false
                    for (NodeChild url : urlset.children()) {
                        if (url.loc.text().endsWith(postUrlSuffix)) {
                            url.replaceNode {}
                            deleted = true
                            break
                        }
                    }
                    if (deleted) {
                        withZippedFileWriter sitemap, { writer ->
                            XmlUtil.serialize(urlset, writer)
                        }

                        sitemapindex.children().last().lastmod.replaceBody(new Date().format("yyyy-MM-dd"))
                        withFileWriter sitemapIndex, { writer ->
                            XmlUtil.serialize(sitemapindex, writer)
                        }
                    }
                }
            } catch (Exception e) {
                logger.error('Error in postUpdated', e)
            }
        }
    }

    private File getSitemapIndex() {
        String path = grailsApplication.parentContext.servletContext.getRealPath("/sitemap.xml")
        return new File(path)
    }

    private File getSitemap(int index) {
        String path = grailsApplication.parentContext.servletContext.getRealPath("/sitemap-${index}.xml.gz")
        return new File(path)
    }

    private MarkupBuilder createMarkupBuilder(Writer writer) {
        MarkupBuilder markupBuilder = new MarkupBuilder(writer)
        markupBuilder.setDoubleQuotes(true)
        return markupBuilder
    }

    private createNewSitemap(File file) {
        createNewBaseSitemap(getSitemap(0))
        createNewPostsSitemap(getSitemap(1))
        int maxSitemap = 1

        withFileWriter file, { writer ->
            MarkupBuilder markupBuilder = createMarkupBuilder(writer)
            markupBuilder.mkp.xmlDeclaration(version: "1.0", encoding: ENCODING_UTF8)
            markupBuilder.sitemapindex("xmlns": "http://www.sitemaps.org/schemas/sitemap/0.9") {
                Date date = new Date()
                (0 .. maxSitemap).each { index ->
                    sitemap() {
                        loc("${SERVER}sitemap-${index}.xml.gz")
                        lastmod(date.format('yyyy-MM-dd'))
                    }
                }
            }
        }
    }

    private createNewBaseSitemap(File file) {
        withZippedFileWriter file, { writer ->
            MarkupBuilder markupBuilder = createMarkupBuilder(writer)
            markupBuilder.mkp.xmlDeclaration(version: "1.0", encoding: ENCODING_UTF8)
            markupBuilder.urlset("xmlns": "http://www.sitemaps.org/schemas/sitemap/0.9") {
                Date date = new Date()
                url() {
                    loc(SERVER)
                    lastmod(date.format('yyyy-MM-dd'))
                    changefreq('daily')
                }
                def categories = Category.list()
                for (Category category : categories) {
                    writeCategory(markupBuilder, category, date)
                }
            }
        }
    }

    private createNewPostsSitemap(File file) {
        withZippedFileWriter file, { writer ->
            MarkupBuilder markupBuilder = createMarkupBuilder(writer)
            markupBuilder.mkp.xmlDeclaration(version: "1.0", encoding: ENCODING_UTF8)
            markupBuilder.urlset("xmlns": "http://www.sitemaps.org/schemas/sitemap/0.9") {
                def posts = Post.findAllByStatusInList([PostStatus.ACTIVE, PostStatus.PAID])
                for (Post post : posts) {
                    writePost(markupBuilder, post)
                }
            }
        }
    }

    private writeCategory(MarkupBuilder markupBuilder, Category category, Date date) {
        markupBuilder.url() {
            loc("${SERVER}category/${category.name.toLowerCase()}")
            lastmod(date.format("yyyy-MM-dd"))
            changefreq('daily')
        }
    }

    private writePost(MarkupBuilder markupBuilder, Post post) {
        markupBuilder.url() {
            loc(getPostUrl(post))
            lastmod(post.approved.format("yyyy-MM-dd"))
            changefreq('weekly')
        }
    }

    private String getPostUrl(Post post) {
        return "${SERVER}${PostUtil.urlTitle(post)}/${post.id}"
    }

    private withFileWriter(File file, closure) {
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(file), ENCODING_UTF8))
        try {
            closure(writer)
        } finally {
            try {
                writer.close()
            } catch (e) {
                logger.error("Error closing file writer", e)
            }
        }
    }

    private withZippedFileWriter(File file, closure) {
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new GZIPOutputStream(new FileOutputStream(file)), ENCODING_UTF8))
        try {
            closure(writer)
        } finally {
            try {
                writer.close()
            } catch (e) {
                logger.error("Error closing file writer", e)
            }
        }
    }

    private String withStringWriter(closure) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream()
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                baos, ENCODING_UTF8))
        try {
            closure(writer)
            writer.flush()
            return baos.toString(ENCODING_UTF8)
        } finally {
            try {
                writer.close()
            } catch (e) {
                logger.error("Error closing string writer", e)
            }
        }
    }
}
