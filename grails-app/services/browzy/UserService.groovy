package browzy

import grails.transaction.Transactional

@Transactional
class UserService implements DbConst {

    private static final int SCALED_WIDTH = 70

    def imageService

    boolean update(User user, params) {
        params.subMap(USER_FIELDS_SIMPLE_EDIT).findAll { it.value }.each { k, v ->
            user.(k.toString()) = v
        }

        user.socialLinks = params.socialLinks.findAll { it?.trim() }

        if (params.avatar && !params.avatar.empty) {
            user.avatar = user.avatar ?: new DbFile()
            user.avatar.with {
                name = 'avatar'
                content = params.avatar.bytes
                contentType = params.avatar.contentType
                fileName = params.avatar.filename
            }

            byte[] scaled = imageService.scale(params.avatar.bytes, AVATAR_SCALED_WIDTH, 0, params.avatar.contentType)
            user.scaledAvatar = user.scaledAvatar ?: new ScaledImage()
            user.scaledAvatar.with {
                width = AVATAR_SCALED_WIDTH
                name = 'scaledAvatar'
                content = scaled
                contentType = params.avatar.contentType
                fileName = params.avatar.filename
            }
        }

        if (user.validate()) {
            user.save()
            return true
        } else {
            return false
        }
    }

    def listAllUsersForAdminView() {
        return User.list().sort{it.country}.sort{ i1, i2 ->
            (i1.signedOn.clone() as Date).clearTime() <=> (i2.signedOn.clone() as Date).clearTime() ?:
            i2.posts.size() <=> i1.posts.size() ?:
            i1.country <=> i2.country
        }
    }
}
