package browzy

import grails.transaction.Transactional
import org.grails.web.servlet.mvc.GrailsWebRequest
import org.grails.web.util.WebUtils

import javax.servlet.http.HttpSession

@Transactional
class RatingService {

    static final String ATTR_VIEWED_POSTS = 'viewed_posts'

    void opened(User user, Post post) {
        GrailsWebRequest webRequest = WebUtils.retrieveGrailsWebRequest()
        HttpSession session = webRequest.getCurrentRequest().getSession()
        def viewedPosts = session.getAttribute(ATTR_VIEWED_POSTS)
        if (!viewedPosts) {
            viewedPosts = [post.id]
            session.setAttribute(ATTR_VIEWED_POSTS, viewedPosts)
        } else if (post.id in viewedPosts) {
            return
        } else {
            viewedPosts << post.id
        }
        Counter counter = get(user, post)
        counter.looks++
        counter.save(failOnError: true)
    }

    void liked(User user, Post post) {
        Counter counter = get(user, post)
        counter.liked = true
        counter.save()
    }

    void disliked(User user, Post post) {
        Counter counter = get(user, post)
        counter.liked = false
        counter.save()
    }

    void likedComment(User user, Comment comment) {

        CommentCounter commentCounter = getCountComments(user, comment)

        commentCounter.liked = true
        commentCounter.save()

    }

    void dislikedComment(User user, Comment comment) {
        CommentCounter commentCounter = getCountComments(user, comment)

        commentCounter.liked = false
        commentCounter.save()
    }

    void stared(User user, Post post) {
        Counter counter = get(user, post)
        counter.stared = true
        counter.save()
    }

    void read(User user, Post post) {
        Counter counter = get(user, post)
        counter.read = true
        counter.save()
    }

    void draped(User user, Post post) {
        Counter counter = get(user, post)
        counter.draped = true
        counter.save()
    }

    Boolean getLiked(User user, Post post) {
        Counter counter = Counter.findByUserAndPost(user, post)
        return counter ? counter.liked : null
    }

    boolean isStared(User user, Post post) {
        return Counter.findByUserAndPostAndStared(user, post, true) != null
    }

    int getUserPostsAggregate(User user, String postField) {
        return user.posts ? user.posts.sum({ it[postField] }) : 0
    }

    /**
     * @return all posts from follows
     */
    def followsPostsWithCounters(User user) {
//        def posts = Post.findAll('from Post as p where p.author in (:follows) and p.id not in (:readPostsIds) order by p.approved',
//                [follows: user.follows.asList(), readPostsIds: user.readPosts*.id])
        def postsWithCounters = []
        def follows = user.follows.asList()
        if (!follows.empty) {
            def posts = Post.findAll("""
            from Post as p
                where p.author in (:follows)
                    and p.status in ('ACTIVE', 'PAID')
                    and not exists(
                        select 1 from Counter c
                            where c.user = :user and c.post = p and c.draped = true)""",
                    [follows: user.follows.asList(), user: user])

            def counters = Counter.findAllByUserAndPostInList(user, posts)

            for (Post post : posts) {
                Counter counter = counters.find { it.post.id == post.id }
                postsWithCounters << [
                        post: post,
                        stared: counter?.stared,
                        read: counter?.looks > 0 || counter?.read,
                        draped: counter?.draped]
            }
        }
        return postsWithCounters
    }

    def countFollowsPosts(User user) {
        def follows = user.follows.asList()
        if (!follows.empty) {
            def result = Post.executeQuery("""
            select count(*) from Post as p 
                where p.author in (:follows) 
                    and p.status in ('ACTIVE', 'PAID')
                    and not exists(
                        select 1 from Counter c 
                            where c.user = :user and c.post = p and (c.looks > 0 or c.draped = true or c.read = true))""",
                    [follows: user.follows.asList(), user: user])
            return result[0]
        } else {
            return 0
        }
    }

    def readAllFollows(User user) {
        def posts = user.follows*.posts.flatten()
        if (!posts.empty) {
            def statuses = [PostStatus.ACTIVE, PostStatus.PAID]
            Counter.withNewTransaction {
                posts.each {
                    if (it.status in statuses) {
                        read(user, it)
                    }
                }
            }
        }
    }

    private Counter get(User user, Post post) {
        return Counter.findByUserAndPost(user, post) ?: new Counter(user: user, post: post)
    }
    private CommentCounter getCountComments(User user, Comment comment) {
        return CommentCounter.findByUserAndComment(user, comment) ?: new CommentCounter(user: user, comment: comment)
    }
}
