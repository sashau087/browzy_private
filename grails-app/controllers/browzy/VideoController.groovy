package browzy

import grails.plugin.springsecurity.annotation.Secured

@Secured([DbConst.ROLE_ADMIN])
class VideoController {

    def videoService
    def securityService
    def sitemapService

    def add() {
        render view: "edit.gsp", model: [
                action: 'add'
//                user  : securityService.getLoggedInUser()
        ]
    }

    def publish() {
        User user = securityService.getLoggedInUser()
        if (user) {
            def posts = videoService.save(params, user, PostStatus.ACTIVE)
            for (Post post : posts) {
                sitemapService.postCreated(post)
            }
        }
        redirect controller: "front"
    }

    def save() {
        println params
        redirect controller: "front"
    }
}
