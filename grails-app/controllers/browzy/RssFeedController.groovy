package browzy

import groovy.xml.MarkupBuilder

import java.text.DateFormat
import java.text.SimpleDateFormat

class RssFeedController {

    static final String ENCODING_UTF8 = "UTF-8"

    PostService postService

    def index() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream()
        Writer writer = new BufferedWriter(new OutputStreamWriter(baos, ENCODING_UTF8))

        MarkupBuilder markupBuilder = new MarkupBuilder(writer)
        markupBuilder.setDoubleQuotes(true)
        markupBuilder.mkp.xmlDeclaration(version: '1.0', encoding: ENCODING_UTF8)
        markupBuilder.mkp.pi('xml-stylesheet': [title:'XSL_formatting', type:'text/xsl', href:'feed.xsl'])
        markupBuilder.rss('version': '2.0') {
            def posts = postService.timeline(false)
            DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z")
            channel() {
                title('Browzy.com - Experience the power of content')
                description('Browzy.com empowers bloggers, writers and businesses to share interesting stories, posts, videos and exciting products to the world.')
                delegate.link(createLink(controller: 'front', absolute: true).replace('http://b','https://b'))
                ttl('240')
                if (!posts.empty) {
                    pubDate(dateFormat.format(posts[0].approved))
                }
                posts.each { Post post ->
                    item() {
                        title(post.title)
                        description(post.text)
                        delegate.link(createLink(controller: 'post', action: 'view', id: post.id,
                                params: [title: PostUtil.urlTitle(post)], absolute: true).replace('http://b','https://b'))
                        pubDate(dateFormat.format(post.approved))
                    }
                }
            }
        }

        render text: baos.toString(ENCODING_UTF8), contentType: 'text/xml'
    }
}
