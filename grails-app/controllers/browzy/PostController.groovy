package browzy

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.apache.catalina.connector.ClientAbortException
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class PostController {

    Logger logger = LoggerFactory.getLogger(PostController.class)

    def securityService
    def postService
    def ratingService
    def sitemapService

    def assetResourceLocator

    @Secured([DbConst.ROLE_REGISTERED])
    def add() {
        render view: "edit.gsp", model: [
                action: 'add',
                user  : securityService.getLoggedInUser()
        ]
    }

    @Secured([DbConst.ROLE_REGISTERED])
    def edit() {
        User user = securityService.getLoggedInUser()
        Post post = Post.get(params.long('id'))
        if (!user || !post || (user.id != post.author.id && !user.hasRole(DbConst.ROLE_ADMIN))) {
            redirect controller: "front"
        }
        render view: "edit.gsp", model: [
                action   : 'edit',
                user     : user,
                post     : post,
                structure: post ? JSON.parse(post.structure) : null
        ]
    }

    @Secured([DbConst.ROLE_ADMIN])
    def delete() {
        postService.delete(params.long('id'))
        sitemapService.postDeleted(params.long('id'))
        redirect controller: 'front'
    }

    @Secured([DbConst.ROLE_REGISTERED])
    def preview() {
        User user = securityService.getLoggedInUser()
        Post post = savePost(params, user, PostStatus.DRAFT)
        if (post) {
            redirect action: "viewDraft", id: post.id, params: [title: PostUtil.urlTitle(post)]
        }
    }

    @Secured([DbConst.ROLE_REGISTERED])
    def viewDraft() {
        return view()
    }

    @Secured([DbConst.ROLE_REGISTERED])
    def saveDraft() {
        User user = securityService.getLoggedInUser()
        if (savePost(params, user, PostStatus.DRAFT)) {
            redirect controller: "front"
        }
    }


    @Secured([DbConst.ROLE_REGISTERED])
    def addPost() {
        User user = securityService.getLoggedInUser()
        PostStatus status = user.hasRole(DbConst.ROLE_AUTHOR) ? PostStatus.ACTIVE : PostStatus.APPROVE
        if (savePost(params, user, status)) {
            if (status == PostStatus.APPROVE) {
                flash.message = "Thank you. Your post is under review. It may take up to 6 hours. Please wait. " +
                        "You can also <a href='${createLink(controller: 'user', action: 'profile')}'>edit</a> your post on your user account."
            }
            redirect controller: "front"
        }
    }

    private Post savePost(params, User user, PostStatus status) {
        try {
            Post post = postService.getPost(params, user)
            if (post.validate()) {
                logger.info("Post ${post.title} validated")

                postService.setStatus(post, status, false)

                boolean postCreated = post.id == null
                boolean titleChanged = post.isDirty('title')

                post.save(flush: true, failOnError: true)
                logger.info("Post ${post.title} (${post.id}) saved")

                if (post.published) {
                    if (postCreated) {
                        sitemapService.postCreated(post)
                    } else if (titleChanged) {
                        sitemapService.postUpdated(post)
                    }
                    postService.setPriority(post.id, 1)
                }

                return post
            } else {
                logger.info("Post ${post.title} invalid")
                // server side validation in edit process is incorrect
                //String tempPostId = String.valueOf(-System.currentTimeMillis())
                //session[tempPostId] = post
                render view: 'edit', model: [
                        action   : post.id ? 'edit' : 'add',
                        user     : user,
                        post     : post,
                        structure: post ? JSON.parse(post.structure) : null
                        //,tempPostId: tempPostId
                ]
                return null
            }
        } catch (Exception e) {
            logger.error("Error saving post ${post?.title}", e)
            throw e
        }
    }

    def view() {
        User user = securityService.getLoggedInUser()
        Post post = Post.get(params.long('id'))
        if (post == null) {
            redirect controller: "front"
        } else {
            if (post.status in [PostStatus.ACTIVE, PostStatus.PAID]) {
                ratingService.opened(user, post)
            }
            def similar = postService.similarOrRandom(post, 6)
            Boolean liked = user ? ratingService.getLiked(user, post) : null
            render view: 'view', model: [post     : post,
                                         structure: JSON.parse(post.structure),
                                         user     : user,
                                         liked    : liked != null ? liked : false,
                                         disliked : liked != null ? !liked : false,
                                         stared   : user ? ratingService.isStared(user, post) : false,
                                         nextPost : postService.next(post),
                                         prevPost : postService.previous(post),
                                         similar  : similar]
        }
    }

    def like() {
        User user = securityService.getLoggedInUser()
        Post post = Post.get(params.long('id'))
        if (user && post) {
            ratingService.liked(user, post)
        }
        render post ? "${post.likes},${post.dislikes}" : ""
    }

    def dislike() {
        User user = securityService.getLoggedInUser()
        Post post = Post.get(params.long('id'))
        if (user && post) {
            ratingService.disliked(user, post)
        }
        render post ? "${post.likes},${post.dislikes}" : ""
    }

    def star() {
        User user = securityService.getLoggedInUser()
        Post post = Post.get(params.long('id'))
        if (user && post) {
            ratingService.stared(user, post)
        }
        render post ? post.stars : ""
    }

    def cover() {
        Post post = getPost()
        DbFile cover = post?.cover
        try {
            render cover ? [file: cover.content, contentType: cover.contentType] :
                    [file: assetResourceLocator.findAssetForURI('inner_top.jpg')?.getInputStream()?.bytes, contentType: "image/jpeg"]
        } catch (Exception e) {
            handleFileRenderError(e)
        }
    }

    def image() {
        Post post = getPost()
        DbFile image
        String imageName = params.imageName
        if (!imageName && post?.images && !post.images.empty) {
            imageName = post.images.min { it.id }.name
        }
        if (imageName) {
            image = post?.scaledImages?.find { it.name == imageName }
            if (!image) {
                image = post?.images?.find { it.name == imageName }
            }
        }
        try {
            render file: image?.content ?: assetResourceLocator.findAssetForURI('img.png')?.getInputStream()?.bytes, contentType: image?.contentType ?: "image/png"
        } catch (Exception e) {
            handleFileRenderError(e)
        }
    }

    def originalImage() {
        Post post = getPost()
        DbFile image
        String imageName = params.imageName
        if (!imageName && post?.images && !post.images.empty) {
            image = post.images[0]
        } else {
            image = post?.images?.find { it.name == imageName }
        }
        try {
            render file: image?.content ?: assetResourceLocator.findAssetForURI('img.png')?.getInputStream()?.bytes, contentType: image?.contentType ?: "image/png"
        } catch (Exception e) {
            handleFileRenderError(e)
        }
    }

    private handleFileRenderError(Exception e) {
        if (e instanceof ClientAbortException || (e.cause != null && e.cause instanceof ClientAbortException)) {
            logger.warn("client aborted file load")
        } else {
            logger.error("error sending file", e)
        }
    }

    def video() {
        Post post = getPost()
        DiskFile video = post?.videos?.find({ it?.name == params?.videoName })
        if (video != null) {
            render file: new File(video.path), contentType: video.contentType
        }
    }

    def downloadVideo() {
        Post post = getPost()
        DiskFile video = post?.videos?.find({ it?.name == params?.videoName })
        if (video != null) {
            render text: video.path, contentType: 'text/html'
        }
    }

    def list() {
        User currentUser = securityService.getLoggedInUser()
        User user = params.userId ? User.get(params.long('userId')) : currentUser
        PostStatus qStatus = PostStatus.getStatus(params.status)
        def posts = postService.listUserPosts(user, qStatus)
        if (!posts.empty) {
            render template: "/post/postTemplate", collection: posts, var: "post", model: [currentUser: currentUser]
        } else {
            render ""
        }
    }

    def stared() {
        User currentUser = securityService.getLoggedInUser()
        User user = params.userId ? User.get(params.long('userId')) : currentUser

    }

    private Post getPost() {
//        long id = params.long('id')
//        Post post = id > 0 ? Post.get(id) : session[String.valueOf(id)]
        return Post.get(params.long('id'))
    }
}
