package browzy

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller:"front")

        "/login/auth" (controller: 'front', action: 'auth')
        "/registrationConfirm"(controller: 'user', action: [GET: "registrationConfirm"])

        "/user/profile/$name/$id" (controller: 'user', action: 'profile')

        "/$title/$id" (controller: 'post', action: 'view')
        "/add-new-post" (controller: 'post', action: 'add')
        "/preview/$title/$id" (controller: 'post', action: 'viewDraft')

        "/saveComment" (controller: 'comment', action: 'saveComment')
        "/commentMore" (controller: 'comment', action: 'commentMore')
        "/generateTemplate" (controller: 'comment', action: 'generateTemplate')
        "/saveReplyComment" (controller: 'comment', action: 'saveReplyComment')
        "/changestatus" (controller: 'comment', action: 'changestatus')
        "/deleteComment" (controller: 'comment', action: 'deleteComment')

        "/activity"(controller: 'activity')
        "/trending" (controller: 'front', action: 'trending')
        "/latest" (controller: 'front', action: 'latest')
        "/category/$id" (controller: 'front', action: 'category')

        "/search" (controller: 'search', action: 'posts')
        "/search/$query"(controller: 'search', action: 'result')

        "/terms-and-conditions" (view: '/pages/termsAndConditions')
        "/privacy-policy" (view: '/pages/privacyPolicy')

        "/rssfeed" (controller: 'rssFeed')

        "500"(view:'/error')
        "404"(view:'/404')

    }
}
