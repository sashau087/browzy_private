package browzy

class FrontController implements Const, DbConst {

    def securityService
    def postService

    def all() {
        def topPosts = postService.topPriority()
        def posts = postService.timeline(securityService.isAdminLoggedIn())

        int i = 0
        while (topPosts.size < 3 && i < posts.size) {
            if (posts[i].published) {
                topPosts << posts[i]
            }
            i++
        }

        def topPostsIds = topPosts.collect { it.id }
        posts.removeAll { it.id in topPostsIds }

        def headers = getHeaders(posts)

        render view: "/index", model: [
                topPosts: topPosts,
                posts   : posts,
                headers : headers
        ]
    }

    private getHeaders(posts) {
        def headers = []
        Date todayDate = new Date()
        String today = todayDate.format('yyyy-MM-dd')
        String yesterday = (todayDate - 1).format('yyyy-MM-dd')
        String prevApprovedOn
        boolean haveNotApproved = false
        posts.eachWithIndex { Post it, index ->
            if (it.approvedDay) {
                if (it.approvedDay != prevApprovedOn) {
                    prevApprovedOn = it.approvedDay
                    if (it.approvedDay != today) {
                        if (it.approvedDay == yesterday) {
                            headers << [header: 'Yesterday', post: it]
                        } else {
                            String date = new Date().parse('yyyy-MM-dd', it.approvedDay).format('dd MMMM, EEEE')
                            headers << [header: date, post: it]
                        }
                    } else if (haveNotApproved) {
                        headers << [header: 'Today', post: it]
                    }
                }
            } else if (prevApprovedOn || index == 0) {
                haveNotApproved = true
                prevApprovedOn = null
                headers << [header: 'Not approved', post: it]
            }
        }
        return headers
    }

    def index() {
        def topPosts = postService.topPriority()
        def topPostsIds = topPosts.collect { it.id }
        def posts = postService.timelinePage(securityService.isAdminLoggedIn(), 0, POSTS_PAGE_LIMIT, topPostsIds)

        int i = 0
        while (topPosts.size < 3 && i < posts.size) {
            if (posts[i].published) {
                topPosts << posts[i]
            }
            i++
        }

        topPostsIds = topPosts.collect { it.id }
        posts.removeAll { it.id in topPostsIds }

        def headers = getHeaders(posts)

        render view: "/index", model: [
                topPosts   : topPosts,
                topPostsIds: topPostsIds.join(','),
                posts      : posts,
                headers    : headers,
                lastHeader : headers.last()?.header
        ]
    }

    def indexPage() {
        int start = params.int('start')
        def topPostsIds = params['topPostsIds'].split(',').collect { Long.valueOf(it) }
        def posts = postService.timelinePage(securityService.isAdminLoggedIn(), start, POSTS_PAGE_LIMIT, topPostsIds)
        def headers = getHeaders(posts)
        String prevLastHeader = params['lastHeader']
        if (!headers.empty && prevLastHeader == headers.first()?.header) {
            headers.removeAt(0)
        }
        render view: "/indexPage", model: [
                start      : start + POSTS_PAGE_LIMIT,
                topPostsIds: params['topPostsIds'],
                posts      : posts,
                headers    : headers,
                lastHeader : !headers.empty ? headers.last().header : prevLastHeader
        ]
    }

    def auth() {
        flash.showLogin = true
        redirect action: "index"
    }

    def sidebar() {
        render view: 'sidebar'
    }

    def trendingSidebar() {
        def trendingPosts = postService.trending([max: 3])
        render view: 'trendingSidebar', model: [
                trending: trendingPosts
        ]
    }

    def latestSidebar() {
        def latestPosts = postService.latest([max: 3])
        render view: 'latestSidebar', model: [
                latest: latestPosts
        ]
    }

    def trending() {
        def allTrendingPosts = postService.trending()
        render view: '/search/result', model: [
                posts : allTrendingPosts,
                header: 'Trending'
        ]
    }

    def latest() {
        def allLatestPosts = postService.latest()
        render view: '/search/result', model: [
                posts : allLatestPosts,
                header: 'Latest'
        ]
    }

    def category() {
        def categoryPosts = postService.category(params.id)
        render view: '/search/result', model: [
                posts : categoryPosts,
                header: params.id
        ]
    }

    def similar() {
        def postId = params.id ? params.long('id') : 0
        Post post = postId > 0 ? Post.read(postId) : null
        def posts = post ? postService.similar(post) : []
        if (posts?.empty) {
            posts = Post.findAllByIdNotEqual(postId)
        }
        render view: '/search/result', model: [
                posts : posts,
                header: 'It may be interesting'
        ]
    }

    def author() {
        User user = User.get(params.long('id'))
        def posts = postService.byAuthor(user)
        render view: '/search/result', model: [
                posts : posts,
                header: "All posts by ${user.displayName}"
        ]
    }

    def terms() {
        render view: 'terms'
    }
}
