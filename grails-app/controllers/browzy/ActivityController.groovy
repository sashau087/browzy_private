package browzy

import grails.plugin.springsecurity.annotation.Secured

@Secured([DbConst.ROLE_REGISTERED])
class ActivityController {

    def securityService
    def ratingService

    def index() {
        User user = securityService.loggedInUser

        def postsWithCounters = ratingService.followsPostsWithCounters(user)

        render view: "followsPosts.gsp",
                model: [postsWithCounters: postsWithCounters]
    }

    def countUnread() {
        User user = securityService.loggedInUser
        render ratingService.countFollowsPosts(user)
    }

    def markRead() {
        User user = securityService.loggedInUser
        Post post = Post.get(params.long('id'))
        ratingService.read(user, post)
        render 1
    }

    def drape() {
        User user = securityService.loggedInUser
        Post post = Post.get(params.long('id'))
        ratingService.draped(user, post)
        render 1
    }

    /**
     * Mark all posts from follows as read.
     */
    def readAll() {

        User user = securityService.loggedInUser

        ratingService.readAllFollows(user)

        redirect controller: "front"
    }
}
