package browzy

import browzy.registration.VerificationToken
import com.megatome.grails.RecaptchaService
import grails.validation.ValidationException

class UserController implements DbConst {

    def securityService
    def userService
    def notificationService
    def emailService

    def assetResourceLocator

    RecaptchaService recaptchaService

    static allowedMethods = [register: 'POST', login: 'POST', registrationConfirm: 'GET']

    def register() {
        User.withTransaction { transaction ->
            try {
                User user = new User(params)
                def recaptchaOK = true
                if (!recaptchaService.verifyAnswer(session, request.getRemoteAddr(), params)) {
                    recaptchaOK = false
                }
                if (user.validate() && params.terms_agreed && recaptchaOK) {
                    recaptchaService.cleanUp(session)
                    user.save(flush: true, failOnError: true)
                    UserRole.create(user, Role.findByAuthority(ROLE_REGISTERED))
                    emailService.sendConfirmationEmail(user)
//                securityService.signIn(user.username, params.password as String)
                    flash.message = 'Thank you. Please click on the link sent to your email to complete registration process.'
                } else {
                    flash.termsNotAgreed = !params.terms_agreed
                    flash.recaptchaFailed = !recaptchaOK
                    flash.invalidRegistrationUser = user
                }
                redirect controller: "front"
            } catch (Exception e) {
                transaction.setRollbackOnly()
                render e
            }
        }
    }

    def registerWithoutConfirmation() {
        User.withTransaction { transaction ->
            try {
                User user = new User(params)
                def recaptchaOK = true
                if (!recaptchaService.verifyAnswer(session, request.getRemoteAddr(), params)) {
                    recaptchaOK = false
                }
                if (user.validate() && params.terms_agreed && recaptchaOK) {
                    recaptchaService.cleanUp(session)
                    user.enabled = true
                    user.save(flush: true, failOnError: true)
                    UserRole.create(user, Role.findByAuthority(ROLE_REGISTERED))
                    VerificationToken verificationToken = VerificationToken.findByUser(user)
                    verificationToken.delete(flush: true)
                    securityService.signIn(user.username, params.password as String)
                } else {
                    flash.termsNotAgreed = !params.terms_agreed
                    flash.recaptchaFailed = !recaptchaOK
                    flash.invalidRegistrationUser = user
                }
                redirect controller: "front"
            } catch (Exception e) {
                transaction.setRollbackOnly()
                render e
            }
        }
    }

    def registrationConfirm() {
        String token = params.token
        if (token) {
            VerificationToken verificationToken = VerificationToken.findByToken(token)
            if (!verificationToken || verificationToken.expiryDate < new Date())
                return failEmailVerification()
            else {
                def user = verificationToken.user
                user.enabled = true
                user.save(flush: true)
                verificationToken.delete(flush: true)
                assert User.get(user.id).enabled
                assert !VerificationToken.findByToken(token)
                securityService.loginUser(user)
                if (!flash.message) {
                    flash.message = 'Thank you. Your account is confirmed.'
                }
                redirect controller: "front"
            }
        } else
            return failEmailVerification()
    }

    private failEmailVerification() {
        render view: "/user/badUser"
    }

    def forgotPasswordEmail() {
        User.withTransaction { transaction ->
            User user = User.findByEmail(params.email)
            String password = UUID.randomUUID().toString()
            user.password = password
            user.save(flush: true)
            emailService.sendForgotPasswordEmail(user.email, password)
            flash.message = 'An email is sent to your Email address that includes your new password. Please, use it to login and change the password.'
        }
        redirect controller: 'front'
    }

    def login() {
        loginUser(params.username as String, params.password as String)
        boolean loggedIn = securityService.isUserLoggedIn()
        if (params.nextAddPost && loggedIn) {
            redirect controller: 'post', action: 'add'
        } else {
            if (!loggedIn) {
                flash.nextAddPost = params.nextAddPost
            }
            redirect controller: 'front'
        }
    }

    private loginUser(String username, String password) {
        try {
            securityService.signIn(username, password)
        } catch (SecurityException e) {
            flash.invalidLogin = true
        } catch (Exception e) {
            println "login exception: " + e
            flash.message = e.message
        }
    }

    def handleValidationException(ValidationException exception) {
        response.outputStream << exception
    }

    def handleSecurityException(SecurityException exception) {
        response.outputStream << exception
    }

    def logout() {
        securityService.signOut()
        redirect controller: "front"
    }

    def authorize() {
        UserRole.create(User.get(params.long('id')), Role.findByAuthority(ROLE_AUTHOR))
        redirect action: "profile", id: params.id
    }

    def follow() {
        User currentUser
        User selectedUser
        if (params.id &&
                (currentUser = securityService.getLoggedInUser()) &&
                (selectedUser = User.load(params.long('id'))) &&
                currentUser.id != selectedUser.id) {
            currentUser.addToFollows(selectedUser)
            currentUser.save(failOnError: true)
            redirect action: "profile", params: [tab: "follows"]
        } else {
            redirect controller: "front"
        }
    }

    def unfollow() {
        User currentUser
        User selectedUser
        if (params.id &&
                (currentUser = securityService.getLoggedInUser()) &&
                (selectedUser = User.load(params.long('id'))) &&
                currentUser.id != selectedUser.id) {
            currentUser.removeFromFollows(selectedUser)
            currentUser.save(failOnError: true)
        }
        forward action: "follows"
    }

    def profile() {
        List<Post> post=Post.getAll()
        User currentUser = securityService.getLoggedInUser()
        User user = params.id ? User.read(params.long('id')) : currentUser
        String tab = params.tab && params.tab in ["posts", "follows"] ? params.tab : "posts"
        [user: user, ownProfile: user.id == currentUser.id, tab: tab, posts: post]
    }

    def follows() {
        User user = params.id ? User.read(params.long('id')) : securityService.getLoggedInUser()
        render view: "/user/follows", model: [users: user.follows]
    }

    def followers() {
        User user = params.id ? User.read(params.long('id')) : securityService.getLoggedInUser()
        render view: "/user/followers", model: [users: user.followers]
    }

    def edit() {
        User user = securityService.getLoggedInUser()
        [user: user]
    }

    def avatar() {
        User user = User.get(params.long('id'))
        DbFile avatar
        if (params.width && params.int('width') <= AVATAR_SCALED_WIDTH) {
            avatar = user?.scaledAvatar
        } else {
            avatar = user?.avatar
        }
        render file: avatar?.content ?: assetResourceLocator.findAssetForURI('profile.jpg')?.getInputStream()?.bytes,
                contentType: avatar?.contentType ?: "image/jpeg"
    }

    def saveSettings() {
        User user = securityService.getLoggedInUser()
        if (userService.update(user, params)) {
            boolean emailReplyToggle = params.emailReply
            boolean emailCommentToggle = params.emailComment
            boolean emailSuccessToggle = params.emailSuccess
            notificationService.turnNotificationAboutCommentPost(emailCommentToggle)
            notificationService.turnNotificationAboutReplyToComment(emailReplyToggle)
            notificationService.turnNotificationAboutSuccessStory(emailSuccessToggle)
            redirect action: 'profile', params: [id: user.id]
        } else {
            flash.invalidUser = user
            redirect action: 'edit'
        }
    }

    def changePassword() {
        User user = securityService.getLoggedInUser()
        if (!securityService.isPasswordValid(user, params.old_password)) {
            flash.changePasswordError = 'Old password is invalid'
        } else if (!params.new_password) {
            flash.changePasswordError = 'Please enter a new password'
        } else if (params.repeat_password != params.new_password) {
            flash.changePasswordError = 'Passwords do not match'
        } else {
            user.password = params.new_password
            user.save(flush: true)
            flash.message = 'The new password has been saved'
        }
        redirect action: 'edit'
    }

}