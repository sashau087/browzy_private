package browzy

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured([DbConst.ROLE_ADMIN])
class AdminController {

    def userService
    def postService
    def sitemapService

    def admin() {
//        List<User> users = userService.listAllUsersForAdminView()
//        List<Post> posts = postService.listAllPosts(params)
//        [users: users, posts: posts]
    }

    def userList() {
        List<User> users = userService.listAllUsersForAdminView()

        //данные для тестирования
        users.push([username: 'Иванов Иван Иванович', email: 'ivanov.ivan@i.ua', country: 'Country 1'])
        users.push([username: 'Петренко Тарас Васильович', email: 'Petrenko.Taras@mail.ru', country: 'Country 2'])
        users.push([username: 'Гришин Петр Иванович', email: 'grishin@mail.by', country: 'Country 3'])

        render([users: users] as JSON)
    }

    def postList() {
        List<Post> posts = postService.listAllPosts(params)

        //данные для тестирования
        posts.push([title: 'Title1', autor: params.author, categories: 'Categories1', tags: 'Tags1', posted: 'Posted on1', status: 'Status1', looks: '1', likes: '1', dislikes: '1'])
        posts.push([title: 'Title2', autor: params.author, categories: 'Categories2', tags: 'Tags2', posted: 'Posted on2', status: 'Status2', looks: '2', likes: '2', dislikes: '2'])
        posts.push([title: 'Title3', autor: params.author, categories: 'Categories3', tags: 'Tags3', posted: 'Posted on3', status: 'Status3', looks: '3', likes: '3', dislikes: '3'])

        render([posts: posts] as JSON)
    }

    def setStatus() {
        Post post = postService.setStatus(params.long('id'), PostStatus.getStatus(params.status), true)
        if (post && post.published) {
            sitemapService.postUpdated(post)
            postService.setPriority(post.id, 1)
        }
        redirect controller: "front"
    }

    def priority() {
        postService.setPriority(params.long('id'), params.int('priority'))
        render ''
    }

    def files() {
        render view: 'files', model: [
                files: DbFile.list()
        ]
    }
}
