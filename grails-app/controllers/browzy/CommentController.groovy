package browzy

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.apache.catalina.connector.ClientAbortException
import org.codehaus.groovy.runtime.powerassert.SourceText
import org.imgscalr.Scalr
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.imageio.ImageIO
import java.awt.Image as AWTImage
import javax.swing.ImageIcon
import java.awt.Graphics2D
import java.awt.image.BufferedImage

class CommentController {

    Logger logger = LoggerFactory.getLogger(CommentController.class)

    def securityService
    def commentService
    def ratingService
    def sitemapService

    def assetResourceLocator

    @Secured([DbConst.ROLE_REGISTERED])
    def saveComment() {

        def image
        if(params.file!='undefined') {
            def imageInByte=resize(params.file.getBytes())

            image = new DbFile(version: 0, content: imageInByte, contentType: params.file.contentType.toString(), fileName: params.file.name.toString(), name: "avatar", class: "browzy.DbFile")
            image.save(flush: true)
        }else{
            image=null
        }
        def id=commentService.commentPost(params.long("postId"), params.text, image)
        return render (id.toString())
    }
    @Secured([DbConst.ROLE_REGISTERED])
    def saveReplyComment() {

        def image
        if(params.file!='undefined') {
            def imageInByte=resize(params.file.getBytes())
            image = new DbFile(version: 0, content: imageInByte, contentType: params.file.contentType.toString(), fileName: params.file.name.toString(), name: "avatar", class: "browzy.DbFile")
            image.save(flush: true)
        }else{
            image=null
        }

        def id=commentService.commentAnotherComment(params.long("postId"), params.long("id"), params.text, image)
        return render (id.toString())
    }
    def commentMore() {
        return render(commentService.commentMore(params.long("postId"), params.long("lastId")) as JSON)
    }
    def generateTemplate() {
        Post post = Post.get(params.long('postId'))
        Comment comment = Comment.get(params.long('commentId'))
        def class1
        def class2
        if(params.long('class1')!=null){
            class1=' level2'
            class2=' additional'
        }else {
            class1=''
            class2=''

        }
        return render(template: '/post/comment', model:[comments: post.comments, p: comment, user:getAuthenticatedUser(), class1: class1, class2: class2])

    }
    def likeComment() {
        User user = securityService.getLoggedInUser()
        Comment comment = Comment.get(params.long('id'))
        System.out.println(params.long('id'))
        if (user && comment) {
            ratingService.likedComment(user, comment)
        }
        render comment ? "${comment.likesComment},${comment.dislikesComment}" : ""
    }
    def avatar() {
        Comment comment = Comment.get(params.long('id'))
        DbFile avatar
        avatar = comment?.avatar
        render file: avatar?.content ?: assetResourceLocator.findAssetForURI('profile.jpg')?.getInputStream()?.bytes,
                contentType: avatar?.contentType ?: "image/jpeg"
    }
    def dislikeComment() {
        User user = securityService.getLoggedInUser()
        Comment comment = Comment.get(params.long('id'))
        if (user && comment) {
            ratingService.dislikedComment(user, comment)
        }
        render comment ? "${comment.likesComment},${comment.dislikesComment}" : ""
    }
    def changestatus() {
        Comment comment = Comment.get(params.long('id'))
        comment.status_read = "read"
        comment.save()
    }
    byte[] resize(image){
        InputStream inputImage = new ByteArrayInputStream(image)
        BufferedImage originalImage = ImageIO.read(inputImage)
        BufferedImage thumbnail = Scalr.resize(originalImage, 500)
        ByteArrayOutputStream baos = new ByteArrayOutputStream()
        ImageIO.write( thumbnail, "jpeg", baos )
        baos.flush()
        byte[] imageInByte = baos.toByteArray()
        baos.close()
        return imageInByte
    }
    def deleteComment(){
        Comment comment = Comment.get(params.long('id'))
        deleteReplies(comment, params.long('id'))

//        for(int i=0; i<comment.getReplies().size(); i++) {
//
//            comment.getReplies().get.delete(flush: true)
//        }
        comment.getReplies().removeAll()
        comment.save()
        comment.delete(flush: true)
    }
    def deleteReplies(Comment comment, Long id){
        List<Comment> comments=Comment.getAll()
        for(int i=0; i<comments.size(); i++){
            for(int i2=0; i2<comments.get(i).replies.size(); i2++) {
                if (comments.get(i).replies[i2].id == id) {
                    System.out.print("qqq" + 1)
                    comments.get(i).removeFromReplies(comment)

                    comments.get(i).save(flush: true)

                }
            }
        }
    }
}
