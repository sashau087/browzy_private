package browzy

import grails.converters.JSON

class SearchController {

    def searchService

    /**
     * параметры - tags и searchString
     * tags - предположительно строка с тегами, разделёнными запятыми
     * searchString - строка с ключевыми словами для поиска
     * @return найденные посты в представлении JSON. Изменить в зависимости от требований
     */
    def search() {
        List tags = ((params.tags).split(',') as List<String>)*.trim()
        String searchString = params.searchString
        List posts = searchService.searchPostsByTags(tags)
        posts += searchService.searchPostsByTitle(searchString)
        render(posts as JSON)
    }

    def posts() {
        redirect action: 'result', params: params
    }

    def result() {
        def keywords = (params.query).split(',')*.trim()
        def posts = searchService.searchPostsByKeywords(keywords)
        render view: "result", model: [
                posts : posts,
                header: "Search result for \"${params.query}\""]
    }
}
