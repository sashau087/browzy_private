    changeSet(author: "Alex (generated)", id: "14891270001823-1") {
        addColumn(tableName: "comment") {
            column(name: "status_read", type: "varchar(255)", defaultValue: 'unread') {
                constraints(nullable: "false")
            }
        }
    }
