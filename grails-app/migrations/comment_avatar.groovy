databaseChangeLog = {

    changeSet(author: "Alex (generated)", id: "6f5bd05d149bd56d93df086a6b12675f") {
        addColumn(tableName: "comment") {
            column(name: "avatar_id", type: "BIGINT") {
                constraints(nullable: "true")
            }
        }
        createIndex(indexName: "FK_7fa4pvw0j0flw5046dwoieddd", tableName: "comment") {
            column(name: "avatar_id")
        }
        addForeignKeyConstraint(baseColumnNames: "avatar_id", baseTableName: "comment", constraintName: "FK_7fa4pvw0j0flw5046dwoieddd", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "db_file")
    }
}
