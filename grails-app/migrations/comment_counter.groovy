databaseChangeLog = {
    changeSet(author: "Alex (generated)", id: "a37c616d2cf4aecbb10484149803cbe4") {
        createTable(tableName: "comment_counter") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "liked", type: "BIT(1)")

            column(name: "comment_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }
    changeSet(author: "Alex (generated)", id: "148912700018322423423-1") {
        addColumn(tableName: "comment") {
            column(name: "status_read", type: "varchar(255)", defaultValue: 'unread') {
                constraints(nullable: "false")
            }
        }
    }
    changeSet(author: "Alex (generated)", id: "7:a77aff026dda767e09ba2236b708ccee") {
        createIndex(indexName: "FK_l1o46g3je8aav6nbi73b9voio", tableName: "comment_counter") {
            column(name: "comment_id")
        }
    }

    changeSet(author: "Alex (generated)", id: "5ce54c3869f2f645e6a2e09a2d52f006") {
        createIndex(indexName: "FK_7fa4pvw0j0flw5046dwoie91j", tableName: "comment_counter") {
            column(name: "user_id")
        }
    }

    changeSet(author: "Alex (generated)", id: "4bb5e5095772e6e7a806b5068dc3f83b") {
        addForeignKeyConstraint(baseColumnNames: "comment_id", baseTableName: "comment_counter", constraintName: "FK_l1o46g3je8aav6nbi73b9voio", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "comment")
    }
    changeSet(author: "Alex (generated)", id: "0bce0c81b072402831fd021a0200cb9a") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "comment_counter", constraintName: "FK_7fa4pvw0j0flw5046dwoie91j", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }
}