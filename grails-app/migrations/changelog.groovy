databaseChangeLog = {

    changeSet(author: "Avelar (generated)", id: "1487014420846-1") {
        createTable(tableName: "category") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-2") {
        createTable(tableName: "comment") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "date", type: "datetime(6)") {
                constraints(nullable: "false")
            }

            column(name: "post_id", type: "BIGINT")

            column(name: "text", type: "VARCHAR(2000)") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-3") {
        createTable(tableName: "comment_comment") {
            column(name: "comment_replies_id", type: "BIGINT")

            column(name: "comment_id", type: "BIGINT")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-4") {
        createTable(tableName: "counter") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "draped", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "liked", type: "BIT(1)")

            column(name: "looks", type: "INT") {
                constraints(nullable: "false")
            }

            column(name: "post_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "was_read", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "shared", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "stared", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-5") {
        createTable(tableName: "db_file") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "content", type: "MEDIUMBLOB") {
                constraints(nullable: "false")
            }

            column(name: "content_type", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "file_name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "class", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "width", type: "INT")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-6") {
        createTable(tableName: "disk_file") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "content_type", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "file_name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "path", type: "VARCHAR(512)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-7") {
        createTable(tableName: "notification_type") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-8") {
        createTable(tableName: "post") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "approved", type: "datetime(6)")

            column(name: "author_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "cover_id", type: "BIGINT")

            column(name: "created", type: "datetime(6)") {
                constraints(nullable: "false")
            }

            column(name: "link", type: "VARCHAR(512)")

            column(name: "status", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "structure", type: "VARCHAR(3600)") {
                constraints(nullable: "false")
            }

            column(name: "text", type: "VARCHAR(3600)") {
                constraints(nullable: "false")
            }

            column(name: "title", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-9") {
        createTable(tableName: "post_categories") {
            column(name: "post_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "category_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-10") {
        createTable(tableName: "post_db_file") {
            column(name: "post_images_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "db_file_id", type: "BIGINT")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-11") {
        createTable(tableName: "post_disk_file") {
            column(name: "post_videos_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "disk_file_id", type: "BIGINT")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-12") {
        createTable(tableName: "post_scaled_image") {
            column(name: "post_scaled_images_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "scaled_image_id", type: "BIGINT")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-13") {
        createTable(tableName: "post_tags") {
            column(name: "post_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-14") {
        createTable(tableName: "post_text") {
            column(name: "post_texts_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "text_id", type: "BIGINT")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-15") {
        createTable(tableName: "role") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "authority", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-16") {
        createTable(tableName: "sequence_post") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "position", type: "INT") {
                constraints(nullable: "false")
            }

            column(name: "post_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "sequence", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-17") {
        createTable(tableName: "tag") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "text", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-18") {
        createTable(tableName: "text") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "content", type: "VARCHAR(3600)") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-19") {
        createTable(tableName: "user") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "about", type: "VARCHAR(500)")

            column(name: "account_expired", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "account_locked", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "avatar_id", type: "BIGINT")

            column(name: "country", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "email", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "enabled", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "gender", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(20)")

            column(name: "password", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "password_expired", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "scaled_avatar_id", type: "BIGINT")

            column(name: "username", type: "VARCHAR(13)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-20") {
        createTable(tableName: "user_notification_type") {
            column(name: "user_notification_types_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "notification_type_id", type: "BIGINT")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-21") {
        createTable(tableName: "user_role") {
            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "role_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-22") {
        createTable(tableName: "user_social_links") {
            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "social_links_string", type: "VARCHAR(255)")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-23") {
        createTable(tableName: "user_user") {
            column(name: "user_follows_id", type: "BIGINT")

            column(name: "user_id", type: "BIGINT")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-24") {
        createTable(tableName: "verification_token") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "expiry_date", type: "datetime(6)") {
                constraints(nullable: "false")
            }

            column(name: "token", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-25") {
        addPrimaryKey(columnNames: "post_id, category_id", constraintName: "PRIMARY", tableName: "post_categories")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-26") {
        addPrimaryKey(columnNames: "post_id, tag_id", constraintName: "PRIMARY", tableName: "post_tags")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-27") {
        addPrimaryKey(columnNames: "user_id, role_id", constraintName: "PRIMARY", tableName: "user_role")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-28") {
        addUniqueConstraint(columnNames: "name", constraintName: "UK_gj2fy3dcix7ph7k8684gka40c", tableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-29") {
        addUniqueConstraint(columnNames: "authority", constraintName: "UK_irsamgnera6angm0prq1kemt2", tableName: "role")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-30") {
        addUniqueConstraint(columnNames: "text", constraintName: "UK_jb0t6ld04abk4k750kep9le16", tableName: "tag")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-31") {
        addUniqueConstraint(columnNames: "email", constraintName: "UK_ob8kqyqqgmefl0aco34akdtpe", tableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-32") {
        addUniqueConstraint(columnNames: "username", constraintName: "UK_sb8bbouer5wak8vyiiy4pf2bx", tableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-33") {
        createIndex(indexName: "FK_1j785jmwrbib0nx9u0f38ip7y", tableName: "comment_comment") {
            column(name: "comment_replies_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-34") {
        createIndex(indexName: "FK_1wch7xpck1m4hv6371lijpnq3", tableName: "user") {
            column(name: "avatar_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-35") {
        createIndex(indexName: "FK_5ogq1abt8ajqlxj2a1jh4onsx", tableName: "post_text") {
            column(name: "text_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-36") {
        createIndex(indexName: "FK_7fa4pvw0j0flw5046dwcwi91j", tableName: "counter") {
            column(name: "user_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-37") {
        createIndex(indexName: "FK_80xbvau7obodrow5jla8cgts4", tableName: "user_user") {
            column(name: "user_follows_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-38") {
        createIndex(indexName: "FK_bx1eavma9pxa38duguvqw121d", tableName: "post_scaled_image") {
            column(name: "scaled_image_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-39") {
        createIndex(indexName: "FK_f1sl0xkd2lucs7bve3ktt3tu5", tableName: "comment") {
            column(name: "post_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-40") {
        createIndex(indexName: "FK_h9opbcw4hu79l9kqdy7yw90ja", tableName: "comment_comment") {
            column(name: "comment_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-41") {
        createIndex(indexName: "FK_hr583vq485e6y9asq5yy3mx0q", tableName: "post_disk_file") {
            column(name: "disk_file_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-42") {
        createIndex(indexName: "FK_ik65bluepv8oxdfvgbj5qdcsj", tableName: "post") {
            column(name: "author_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-43") {
        createIndex(indexName: "FK_it77eq964jhfqtu54081ebtio", tableName: "user_role") {
            column(name: "role_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-44") {
        createIndex(indexName: "FK_kntu7op3r2aknkn09oobggicp", tableName: "user_notification_type") {
            column(name: "notification_type_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-45") {
        createIndex(indexName: "FK_l1o46g3je8aav6nbi73b9xopi", tableName: "counter") {
            column(name: "post_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-46") {
        createIndex(indexName: "FK_lcejr42g5li2nrfih2vqn5oou", tableName: "post_db_file") {
            column(name: "db_file_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-47") {
        createIndex(indexName: "FK_lew3sxka65cx9ichkheda3m4p", tableName: "post") {
            column(name: "cover_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-48") {
        createIndex(indexName: "FK_m21jxh0j1dxg6do2by36bauav", tableName: "post_categories") {
            column(name: "category_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-49") {
        createIndex(indexName: "FK_mxoojfj9tmy8088avf57mpm02", tableName: "comment") {
            column(name: "user_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-50") {
        createIndex(indexName: "FK_n8k2owli9ecanh4phj01mddvv", tableName: "post_tags") {
            column(name: "tag_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-51") {
        createIndex(indexName: "FK_na2t6ln4orum2ppmix879c6x6", tableName: "user_notification_type") {
            column(name: "user_notification_types_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-52") {
        createIndex(indexName: "FK_o5p464b7m79mx7iel2xj21aa7", tableName: "post_disk_file") {
            column(name: "post_videos_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-53") {
        createIndex(indexName: "FK_o732uu6c99h3tgfpharkgfo8y", tableName: "sequence_post") {
            column(name: "post_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-54") {
        createIndex(indexName: "FK_p511721rmu2u1fnr6fi3pcd3g", tableName: "user_social_links") {
            column(name: "user_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-55") {
        createIndex(indexName: "FK_pqdtdfot9rxtaw057fjc0mqy6", tableName: "post_text") {
            column(name: "post_texts_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-56") {
        createIndex(indexName: "FK_pxvowf1yt37w7h3ubrw95ulwy", tableName: "post_db_file") {
            column(name: "post_images_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-57") {
        createIndex(indexName: "FK_q6jibbenp7o9v6tq178xg88hg", tableName: "verification_token") {
            column(name: "user_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-58") {
        createIndex(indexName: "FK_qfcbcmlf09s0dy969f3e4mi3u", tableName: "post_scaled_image") {
            column(name: "post_scaled_images_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-59") {
        createIndex(indexName: "FK_s7pwk9swjfqs0w7ub9tt5s47y", tableName: "user_user") {
            column(name: "user_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-60") {
        createIndex(indexName: "FK_t0l3hp2lq14603h9fggm2bgc4", tableName: "user") {
            column(name: "scaled_avatar_id")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-61") {
        createIndex(indexName: "approved_idx", tableName: "post") {
            column(name: "approved")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-62") {
        createIndex(indexName: "created_idx", tableName: "post") {
            column(name: "created")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-63") {
        createIndex(indexName: "title_idx", tableName: "post") {
            column(name: "title")
        }
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-64") {
        addForeignKeyConstraint(baseColumnNames: "comment_replies_id", baseTableName: "comment_comment", constraintName: "FK_1j785jmwrbib0nx9u0f38ip7y", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "comment")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-65") {
        addForeignKeyConstraint(baseColumnNames: "avatar_id", baseTableName: "user", constraintName: "FK_1wch7xpck1m4hv6371lijpnq3", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "db_file")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-66") {
        addForeignKeyConstraint(baseColumnNames: "text_id", baseTableName: "post_text", constraintName: "FK_5ogq1abt8ajqlxj2a1jh4onsx", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "text")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-67") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "counter", constraintName: "FK_7fa4pvw0j0flw5046dwcwi91j", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-68") {
        addForeignKeyConstraint(baseColumnNames: "user_follows_id", baseTableName: "user_user", constraintName: "FK_80xbvau7obodrow5jla8cgts4", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-69") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_role", constraintName: "FK_apcc8lxk2xnug8377fatvbn04", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-70") {
        addForeignKeyConstraint(baseColumnNames: "scaled_image_id", baseTableName: "post_scaled_image", constraintName: "FK_bx1eavma9pxa38duguvqw121d", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "db_file")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-71") {
        addForeignKeyConstraint(baseColumnNames: "post_id", baseTableName: "post_categories", constraintName: "FK_byp1y1bji7sdo2w0l94m2uum2", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "post")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-72") {
        addForeignKeyConstraint(baseColumnNames: "post_id", baseTableName: "comment", constraintName: "FK_f1sl0xkd2lucs7bve3ktt3tu5", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "post")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-73") {
        addForeignKeyConstraint(baseColumnNames: "comment_id", baseTableName: "comment_comment", constraintName: "FK_h9opbcw4hu79l9kqdy7yw90ja", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "comment")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-74") {
        addForeignKeyConstraint(baseColumnNames: "disk_file_id", baseTableName: "post_disk_file", constraintName: "FK_hr583vq485e6y9asq5yy3mx0q", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "disk_file")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-75") {
        addForeignKeyConstraint(baseColumnNames: "author_id", baseTableName: "post", constraintName: "FK_ik65bluepv8oxdfvgbj5qdcsj", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-76") {
        addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "user_role", constraintName: "FK_it77eq964jhfqtu54081ebtio", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "role")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-77") {
        addForeignKeyConstraint(baseColumnNames: "notification_type_id", baseTableName: "user_notification_type", constraintName: "FK_kntu7op3r2aknkn09oobggicp", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "notification_type")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-78") {
        addForeignKeyConstraint(baseColumnNames: "post_id", baseTableName: "counter", constraintName: "FK_l1o46g3je8aav6nbi73b9xopi", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "post")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-79") {
        addForeignKeyConstraint(baseColumnNames: "db_file_id", baseTableName: "post_db_file", constraintName: "FK_lcejr42g5li2nrfih2vqn5oou", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "db_file")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-80") {
        addForeignKeyConstraint(baseColumnNames: "cover_id", baseTableName: "post", constraintName: "FK_lew3sxka65cx9ichkheda3m4p", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "db_file")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-81") {
        addForeignKeyConstraint(baseColumnNames: "category_id", baseTableName: "post_categories", constraintName: "FK_m21jxh0j1dxg6do2by36bauav", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "category")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-82") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "comment", constraintName: "FK_mxoojfj9tmy8088avf57mpm02", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-83") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "post_tags", constraintName: "FK_n8k2owli9ecanh4phj01mddvv", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "tag")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-84") {
        addForeignKeyConstraint(baseColumnNames: "user_notification_types_id", baseTableName: "user_notification_type", constraintName: "FK_na2t6ln4orum2ppmix879c6x6", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-85") {
        addForeignKeyConstraint(baseColumnNames: "post_videos_id", baseTableName: "post_disk_file", constraintName: "FK_o5p464b7m79mx7iel2xj21aa7", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "post")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-86") {
        addForeignKeyConstraint(baseColumnNames: "post_id", baseTableName: "sequence_post", constraintName: "FK_o732uu6c99h3tgfpharkgfo8y", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "post")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-87") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_social_links", constraintName: "FK_p511721rmu2u1fnr6fi3pcd3g", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-88") {
        addForeignKeyConstraint(baseColumnNames: "post_texts_id", baseTableName: "post_text", constraintName: "FK_pqdtdfot9rxtaw057fjc0mqy6", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "post")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-89") {
        addForeignKeyConstraint(baseColumnNames: "post_images_id", baseTableName: "post_db_file", constraintName: "FK_pxvowf1yt37w7h3ubrw95ulwy", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "post")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-90") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "verification_token", constraintName: "FK_q6jibbenp7o9v6tq178xg88hg", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-91") {
        addForeignKeyConstraint(baseColumnNames: "post_scaled_images_id", baseTableName: "post_scaled_image", constraintName: "FK_qfcbcmlf09s0dy969f3e4mi3u", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "post")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-92") {
        addForeignKeyConstraint(baseColumnNames: "post_id", baseTableName: "post_tags", constraintName: "FK_rf0kr7eqk5xoalmc4gigdwg3p", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "post")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-93") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_user", constraintName: "FK_s7pwk9swjfqs0w7ub9tt5s47y", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "Avelar (generated)", id: "1487014420846-94") {
        addForeignKeyConstraint(baseColumnNames: "scaled_avatar_id", baseTableName: "user", constraintName: "FK_t0l3hp2lq14603h9fggm2bgc4", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "db_file")
    }

    include file: 'counter_class.groovy'
    include file: 'counter_class_value.groovy'
    include file: 'counter-user-nullable.groovy'
    include file: 'post_type.groovy'
    include file: 'comment_avatar.groovy'
    include file: 'comment_counter.groovy'
}
