databaseChangeLog = {

    changeSet(author: "Valery_Miskevich (generated)", id: "1489127000183-1") {
        addColumn(tableName: "counter") {
            column(name: "class", type: "varchar(255)") {
                constraints(nullable: "false")
            }
        }
    }
}
