databaseChangeLog = {

    changeSet(author: "Valery_Miskevich (generated)", id: "1491204355944-60") {
        dropNotNullConstraint(columnDataType: "bigint", columnName: "user_id", tableName: "counter")
    }
}
