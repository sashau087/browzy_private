databaseChangeLog = {

    changeSet(author: "Valery_Miskevich (generated)", id: "1492176202974-1") {
        addColumn(tableName: "post") {
            column(name: "type", type: "varchar(255)")
        }
    }
}
