package browzy

class DiskFile {

    String name

    String fileName

    String path

    String contentType

    static constraints = {
        path maxSize: 512
    }
}
