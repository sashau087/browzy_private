package browzy

import grails.gorm.DetachedCriteria
import groovy.transform.ToString

import org.apache.commons.lang.builder.HashCodeBuilder

@ToString(cache=true, includeNames=true, includePackage=false)
class UserRole implements Serializable {

	User user
	Role role

	@Override
	boolean equals(other) {
		if (other instanceof UserRole) {
			other.userId == user?.id && other.roleId == role?.id
		}
		false
	}

	@Override
	int hashCode() {
		def builder = new HashCodeBuilder()
		if (user) builder.append(user.id)
		if (role) builder.append(role.id)
		builder.toHashCode()
	}

	static UserRole get(long userId, long roleId) {
		criteriaFor(userId, roleId).get()
	}

	static boolean exists(long userId, long roleId) {
		criteriaFor(userId, roleId).count()
	}

	private static DetachedCriteria criteriaFor(long userId, long roleId) {
		where {
			user == User.load(userId) &&
			role == Role.load(roleId)
		}
	}

	static UserRole create(User user, Role role) {
		def instance = new UserRole(user: user, role: role)
		instance.save()
		instance
	}

	static boolean remove(User u, Role r) {
		if (u != null && r != null) {
			where { user == u && role == r }.deleteAll()
		}
        false
	}

	static int removeAll(User u) {
		u == null ? 0 : where { user == u }.deleteAll()
	}

	static int removeAll(Role r) {
		r == null ? 0 : where { role == r }.deleteAll()
	}

	static constraints = {
		role validator: { Role r, UserRole ur ->
			if (ur.user?.id) {
				withNewSession {
					if (exists(ur.user.id, r.id)) {
						return ['userRole.exists']
					}
				}
			}
		}
	}

	static mapping = {
		id composite: ['user', 'role']
		version false
	}
}
