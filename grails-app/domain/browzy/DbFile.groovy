package browzy

class DbFile {

    String name

    String fileName

    byte[] content

    String contentType

//    String getBase64() {
//        content.encodeBase64()
//    }

    static constraints = {
        content maxSize: DbConst.MAX_IMAGE_SIZE
    }

}
