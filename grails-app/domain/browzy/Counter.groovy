package browzy

class Counter {

    static belongsTo = [post: Post]

    User user

    Boolean liked
    int looks = 0
    // marked as read in follows activity
    boolean read
    boolean stared
    boolean shared
    // removed from follows activity
    boolean draped

    static constraints = {
        liked nullable: true
        user nullable: true
    }

    static mapping = {
        read column: 'was_read'
    }
}
