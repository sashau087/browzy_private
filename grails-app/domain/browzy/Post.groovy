package browzy

class Post {

    static belongsTo = [author: User]

    static hasMany = [
            texts       : Text,
            images      : DbFile,
            scaledImages: ScaledImage,
            videos      : DiskFile,
            tags        : Tag,
            categories  : Category,
            comments    : Comment,
            counters    : Counter
    ]

    String type

    String title
    String text
    String structure

    DbFile cover

    String link

    Date created = new Date()
    String createdDay

    Date approved
    String approvedDay

    PostStatus status

    static transients = ['looks', 'likes', 'dislikes', 'stars', 'shares']

    static constraints = {
        type nullable: true
        title size: 1..DbConst.MAX_POST_TITLE_SIZE
        text maxSize: DbConst.MAX_TEXT_SIZE
        structure maxSize: DbConst.MAX_TEXT_SIZE
        cover nullable: true
        link nullable: true, maxSize: DbConst.MAX_LINK_SIZE
        categories maxSize: DbConst.MAX_CATEGORIES, minSize: 0
        approved nullable: true
        approvedDay nullable: true
    }

    static mapping = {
        images lazy: false
        texts lazy: false
        tags lazy: false
        title index: 'title_idx'
        created index: 'created_idx'
        approved index: 'approved_idx'
        //createdDay formula: "DATE_FORMAT(created, '%Y-%m-%d')" // mysql
        //createdDay formula: "FORMATDATETIME(created, 'yyyy-MM-dd')" // h2
        createdDay formula: "DATE_FORMAT(created, '%Y-%m-%d')", index: 'created_day_idx' // mysql
        approvedDay formula: "DATE_FORMAT(approved, '%Y-%m-%d')", index: 'approved_day_idx'
        comments sort: 'id', order: 'desc'

    }

    int getLooks() {
//        return Counter.countByPost(this)
        def count = Counter.executeQuery('select sum(c.looks) from Counter c where c.post = ?', this)
        return count && count[0] ? count[0] : 0
    }

    int getLikes() {
        return Counter.countByPostAndLiked(this, true)
    }

    int getDislikes() {
        return Counter.countByPostAndLiked(this, false)
    }

    int getStars() {
        return Counter.countByPostAndStared(this, true)
    }

    int getShares() {
        return Counter.countByPostAndShared(this, true)
    }

    boolean isPublished() {
        return status == PostStatus.ACTIVE || status == PostStatus.PAID
    }
}
