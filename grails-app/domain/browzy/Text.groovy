package browzy

class Text {

    String name
    String content

    static constraints = {
        content maxSize: DbConst.MAX_TEXT_SIZE, blank: true
    }
}
