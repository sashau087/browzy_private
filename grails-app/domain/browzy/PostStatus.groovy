package browzy

enum PostStatus {
    DRAFT("DRAFT"), APPROVE("APPROVE"), ACTIVE("ACTIVE"), PAID("PAID")

    String value

    PostStatus(String value) {
        this.value = value
    }

    static PostStatus getStatus(String value) {
        values().find({it.value.equalsIgnoreCase(value)})
    }
}