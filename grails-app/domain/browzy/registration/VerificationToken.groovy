package browzy.registration

import browzy.User
import org.apache.commons.lang.time.DateUtils

class VerificationToken {

    def emailService
    String token
    User user
    Date expiryDate

    static constraints = {
    }

    void setExpiryDate(Date expiryDate) {
        this.expiryDate = calculateExpiryDate(emailService.getEmailExpirationTimeInHours())
    }

    void setUser(User user) {
        this.user = user
        this.token = UUID.randomUUID().toString()
        setExpiryDate(null)
    }

    private Date calculateExpiryDate(int expiryTimeInHours) {
        return DateUtils.addHours(new Date(), expiryTimeInHours)
    }
}
