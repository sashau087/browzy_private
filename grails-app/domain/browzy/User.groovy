package browzy

import browzy.notification.NotificationType
import browzy.registration.VerificationToken
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes = 'username')
@ToString(includes = 'username', includeNames = true, includePackage = false)
class User implements Serializable {

    transient springSecurityService
    transient ratingService

    static hasMany = [
            posts: Post,
            comments: Comment,
            notificationTypes: NotificationType,
            follows: User,
            socialLinks: String
    ]

    User(){
        setSignedOn(new Date())
    }

    String username
    String password
    String email

    DbFile avatar
    ScaledImage scaledAvatar
    String name
    String about

    String country
    String gender


    boolean enabled = false
    boolean accountExpired = false
    boolean accountLocked = false
    boolean passwordExpired = false

    Date signedOn

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this)*.role
    }

    private void setSignedOn(Date signedOn) {
        this.signedOn = signedOn
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    def afterInsert() {
        withNewSession { session ->
            def token = new VerificationToken(user: this)
            token.save(flush: true)
            log.info(token?.toString())
        }
    }

    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
    }

    static constraints = {
        password blank: false, password: true
        username blank: false, unique: true, maxSize: DbConst.MAX_USER_LOGIN_SIZE
        email blank: false, unique: true, email: true
        name nullable: true, unique: true, maxSize: DbConst.MAX_USER_NAME_SIZE
        about nullable: true, maxSize: DbConst.MAX_USER_ABOUT_SIZE
        avatar nullable: true
        scaledAvatar nullable: true
    }

    static mapping = {
        password column: '`password`'
        avatar lazy: false
    }

    def hasRole(String role) {
        hasRole(Role.findByAuthority(role))
    }

    def hasRole(Role role) {
        role in getAuthorities()
    }

    def int getRating() {
        if (likes != null && dislikes != null)
            likes - dislikes
        else return 0
    }

    int getLooks() {
        ratingService.getUserPostsAggregate(this, 'looks')
    }

    int getLikes() {
        ratingService.getUserPostsAggregate(this, 'likes')
    }

    int getDislikes() {
        ratingService.getUserPostsAggregate(this, 'dislikes')
    }

    int getShares() {
        ratingService.getUserPostsAggregate(this, 'shares')
    }

    int getStars() {
        ratingService.getUserPostsAggregate(this, 'stars')
    }

    String getDisplayName() {
        name ?: username
    }

    int getFollowersCount() {
        return User.where {
            follows { id == this.id }
        }.count()
    }

    def getFollowers() {
        return User.where {
            follows { id == this.id }
        }.list()
    }

    String getFacebookLink() {
        return getSocialLink { it =~ /((http|https):\/\/)?(www[.])?facebook.com\/.+/ }
    }

    String getTwitterLink() {
        return getSocialLink { it =~ /((http|https):\/\/)?(www[.])?twitter.com\/.+/ }
    }

    String getPinterestLink() {
        return getSocialLink { it =~ /((http|https):\/\/)?(www[.])?pinterest.com\/.+/ }
    }

    String getGoogleLink() {
        return getSocialLink { it =~ /((http|https):\/\/)?plus.google.com\/.+/ }
    }

    private String getSocialLink(Closure closure) {
        String link = socialLinks.find(closure)
        if (link && !link.startsWith('http')) {
            link = "https://${link}"
        }
        return link
    }
}
