package browzy

class Category {

    static belongsTo = Post

    static hasMany = [posts: Post]

    String name

    static constraints = {
    }
}
