package browzy

class Tag {

    static belongsTo = Post
    static hasMany = [posts: Post]

    String text

    static constraints = {
        text unique: true
    }

    static mapping = {
        text index: 'text_idx'
    }
}
