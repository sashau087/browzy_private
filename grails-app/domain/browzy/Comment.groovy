package browzy

class Comment {

    def notificationService

    static belongsTo = [post: Post, user: User]

    static hasMany = [replies: Comment]

    static hasOne = [user: User]

    String text

    String status_read

    DbFile avatar

    Date date = new Date()

    Set replies = []

    static transients = ['likes', 'dislikes']

    static constraints = {
        text maxSize: 2000, nullable: true
        post nullable: true
        avatar nullable: true
    }

    static mapping = {
        replies nullable: true
        avatar nullable: true
    }

    Comment addToReplies(Comment comment) {
        System.out.println(comment)
        replies.add(comment)
        getNotificationService().notifyAboutReplyToComment(this, comment)
        return this
    }

    Comment addToReplies(Map params) {
        System.out.println(2+"Comment")
        return addToReplies(new Comment(params))
    }

    def afterInsert() {
        getNotificationService().notifyAboutCommentPost(this)
    }
    int getLikesComment() {
        return CommentCounter.countByCommentAndLiked(this, true)
    }
    List<Comment> getReplyComments(Long id) {
        return findAllById(id)
    }
    int getLikesCommentUser(User user) {
        return CommentCounter.countByCommentAndUserAndLiked(this, user, true)
    }
    int getDislikesCommentUser(User user) {
        return CommentCounter.countByCommentAndUserAndLiked(this, user, false)
    }
    int getDislikesComment() {
        return CommentCounter.countByCommentAndLiked(this, false)
    }
}
