package browzy

class CommentCounter {

    static belongsTo = [comment: Comment]

    User user

    Boolean liked

    static constraints = {
        liked nullable: true
        user nullable: true
    }
}
