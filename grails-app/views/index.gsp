<%@ page import="browzy.Const" %>
<!doctype html>
<html>
<head>
    <title>Browzy.com - Experience the power of content</title>
    <meta name="description" content="Browzy.com empowers bloggers, writers and businesses to share interesting stories, posts, videos and exciting products to the world."/>
</head>
<body>
    <g:setGlobalVars/>

    <div id="header_block">
        <div class="container-fluid">
            <div class="row">
                <g:if test="${topPosts.size() > 0}">
                    <g:set var="post" value="${topPosts.get(0)}" />
                    <div class="col-md-6 left_block"
                            style="background: url(${assetPath(src: 'filter.png')}) top center repeat,
                                url(${createLink(controller:'post', action:'image', id:post?.id)}) top center no-repeat;
                                background-size: cover;">
                        <div class="text_container">
                            <div class="block_header"><g:postLink post="${post}"><h2>${post.title}</h2></g:postLink></div>
                            <div class="icons_block">
                                <span class="looks">${post.looks}</span>
                                <span class="likes">${post.likes}</span>
                                <span class="dislikes">${post.dislikes}</span>
                                <span class="comments">${post.comments.size()}</span>
                            </div>
                            <g:postLink post="${post}" class="more_button">read more</g:postLink>
                        </div>
                    </div>
                </g:if>
                <div class="col-md-6 right_block">
                    <g:if test="${topPosts.size() > 1}">
                        <g:set var="post" value="${topPosts.get(1)}" />
                        <div class="top_subblock col-md-12 col-sm-6"
                                style="background: url(${assetPath(src: 'filter.png')}) top center repeat,
                                    url(${createLink(controller:'post', action:'image', id:post?.id)}) center center no-repeat;
                                    background-size: cover;">
                            <div class="text_container">
                                <div class="block_header"><g:postLink post="${post}"><h2>${post.title}</h2></g:postLink></div>
                                <div class="icons_block">
                                    <span class="looks">${post.looks}</span>
                                    <span class="likes">${post.likes}</span>
                                    <span class="dislikes">${post.dislikes}</span>
                                    <span class="comments">${post.comments.size()}</span>
                                </div>
                                <g:postLink post="${post}" class="more_button">read more</g:postLink>
                            </div>
                        </div>
                    </g:if>
                    <g:if test="${topPosts.size() > 2}">
                        <g:set var="post" value="${topPosts.get(2)}" />
                        <div class="bottom_subblock col-md-12 col-sm-6"
                                style="background: url(${assetPath(src: 'filter.png')}) top center repeat,
                                    url(${createLink(controller:'post', action:'image', id:post?.id)}) center center no-repeat;
                                    background-size: cover;">
                            <div class="text_container">
                                <div class="block_header"><g:postLink post="${post}"><h2>${post.title}</h2></g:postLink></div>
                                <div class="icons_block">
                                    <span class="looks">${post.looks}</span>
                                    <span class="likes">${post.likes}</span>
                                    <span class="dislikes">${post.dislikes}</span>
                                    <span class="comments">${post.comments?.size()}</span>
                                </div>
                                <g:postLink post="${post}" class="more_button">read more</g:postLink>
                            </div>
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div>

    <div id="main_block">
        <div class="container">
            <div class="row">
                <div class="col-md-8 left_pane">

                    <g:render template="/post/postTemplate" var="post" collection="${posts}" model="['headers': headers]"/>

                    <g:if test="${posts.size == Const.POSTS_PAGE_LIMIT}">
                        <div id="posts_loading">Loading...</div>
                        <script>
                            window.loadPostsUrl = "${raw(createLink(controller: 'front', action: 'indexPage',
                                params: [start: Const.POSTS_PAGE_LIMIT, topPostsIds: topPostsIds, lastHeader: lastHeader]))}";
                        </script>
                    </g:if>

                </div>

                <g:include controller="front" action="sidebar"/>

            </div>
        </div>
    </div>
</body>
</html>
