<%@ page import="browzy.DbConst" %>
<!doctype html>
<html>
<head>
    <asset:stylesheet src="bootstrap-tagsinput.css"/>

    <asset:javascript src="bootstrap-tagsinput.min.js"/>

    <script>
        var MAX_POST_TITLE_SIZE = ${DbConst.MAX_POST_TITLE_SIZE}
        var MAX_TEXT_SIZE = ${DbConst.MAX_TEXT_SIZE}
        var MAX_CATEGORIES = ${DbConst.MAX_CATEGORIES}
        var MAX_LINK_SIZE = ${DbConst.MAX_LINK_SIZE}
    </script>
</head>
<body>
<div id="inner_page">
    <div class="add_video_header" id ="navigation">
        <div role="presentation" data-count = "4">4 SECTIONS</div>
        <div role="presentation" data-count = "6">6 SECTIONS</div>
        <div role="presentation" data-count = "8">8 SECTIONS</div>
    </div>
    <div class="container">
        <g:render template="post"/>
        <g:uploadForm controller="video">
            <div class="clearfix"></div>
            <div class="sortable ui-sortable" id="videoBlocks">
            </div>

            <div class="row add_post_buttons add_video">
                <div class="col-sm-3 col-sm-offset-3 publish_button">
                    <button name="_action_publish">Publish</button>
                </div>
                <div class="col-sm-3 save_button">
                    <button name="_action_save">Save</button>
                </div>
            </div>
        </g:uploadForm>
    </div>
</div>

<asset:javascript src="video.js"/>

</body>
</html>