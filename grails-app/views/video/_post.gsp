<%@ page import="browzy.DbConst" %>
<div class="post_block video_block add_video ui-sortable-handle ${it ? 'visible' : ''}">
    <asset:image src="red_arrow.png" class="red_arrow" alt="" />
    <div class="row">
        <div class="gray_rounded_block col-sm-12 col-md-3">
            <asset:image src="vid.png" class="center-block" alt="" />
            <iframe style="display:none;" frameborder='0' webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <div class="col-sm-12 col-md-8 inputs">
            <p>Title</p>
            <input type="text" class="form-control input" name="title"
                   placeholder="Maximum title length is ${DbConst.MAX_POST_TITLE_SIZE}" maxlength="${DbConst.MAX_POST_TITLE_SIZE}"/>
            <p>Description</p>
            <textarea class="form-control" name="text"
                      placeholder="Maximum description length is ${DbConst.MAX_TEXT_SIZE}" maxlength="${DbConst.MAX_TEXT_SIZE}"></textarea>
            <p>Video link</p>
            <input type="text" class="form-control input" name="videolink" maxlength="${DbConst.MAX_TEXT_SIZE}" />
            <p>Tags</p>
            <div class="video-tags-input">
                <input type="text" class="form-control input" name="tags"/>
            </div>
            <p>Source link, if required</p>
            <input type="text" class="form-control input" name="source"
                   placeholder="Maximum link length is ${DbConst.MAX_LINK_SIZE}" maxlength="${DbConst.MAX_LINK_SIZE}" />
        </div>
    </div>
</div>
