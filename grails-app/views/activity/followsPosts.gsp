<!doctype html>
<html>
<head>
    <script>
        function star(button, postId) {
            $.get("${createLink(controller: 'post', action: 'star', id:'postId')}".replace('postId', postId),
                function(response) {
                    $(button).addClass('marked').prop('title', 'Post marked as favorite');
                });
        }

        function markRead(button, postId) {
            $.get("${createLink(controller: 'activity', action: 'markRead', id:'postId')}".replace('postId', postId),
                function(response) {
                    $(button).addClass('marked').prop('title', 'Post marked as read');
                    reduceActivityCounter();
                });
        }

        function remove(button, postId, wasRead) {
            $.get("${createLink(controller: 'activity', action: 'drape', id:'postId')}".replace('postId', postId),
                function(response) {
                    $(button).parents(".d").addClass("hidden");
                    if (!wasRead) {
                        reduceActivityCounter();
                    }
                });
        }
    </script>
</head>
<body>
<div id="page_block">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="activity_header">Activity</div>
                <g:link controller="activity" action="readAll">
                    <button class="mark_read">Mark all as read</button>
                </g:link>
                <!--<div class="today_block">-->
                    <!--<span>Today</span>-->
                <!--</div>-->
                <div class="row row-flex">
                    <g:render template="followsPostTemplate" collection="${postsWithCounters}" var="postWithCounters" />
                </div>
            </div>
            <g:include controller="front" action="sidebar"/>
        </div>
    </div>
</div>
</body>
</html>