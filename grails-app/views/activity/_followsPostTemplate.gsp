<%@ page import="browzy.PostUtil" %>
<g:set var="post" value="${postWithCounters.post}"/>
<div class="col-sm-6 col-xs-12 d">
    <div class="activity_block">
        <!--<div class="popular_label">popular</div>-->
        <g:set var="firstMedia" value="${PostUtil.firstMedia(post)}"/>
        <g:if test="${'image' == firstMedia?.type}">
            <img src="${createLink(controller:'post', action:'image', id:post.id)}"  class="img-responsive center-block img" alt="" />
        </g:if>
        <g:elseif test="${'video' == firstMedia?.type}">
            <div style="position:relative;">
                <g:postLink post="${post}" class="videolink" />
                <g:videoImage class="img-responsive center-block img" size="large">${firstMedia.content}</g:videoImage>
            </div>
        </g:elseif>
        <div class="txt">
            <div class="subheader"><g:postLink post="${post}">${post.title}</g:postLink></div>
            <div class="date"><g:formatDate date="${post.created}" type="date" style="MEDIUM"/></div>
            <div class="user_image">
                <g:userLink user="${post.author}">
                    <img src="${createLink(controller:'user', action:'avatar', id:post.author.id)}" style="width: 30px; height: 30px" class="img-circle" alt=""/>
                </g:userLink>
            </div>
            <div class="user_name"><g:userLink user="${post.author}" class="name">${post.author.displayName}</g:userLink></div>
            <div class="activities">
                <a title="${ !postWithCounters.stared ? 'Add to favorites' : 'Post marked as favorite'}" class="star${postWithCounters.stared ? ' marked' : ''}"
                    onclick="${ !postWithCounters.stared ? 'star(this, ' + post.id + '); return false;' : '' }"></a>
                <a title="${ !postWithCounters.read ? 'Mark as read' : 'Post marked as read'}" class="check${postWithCounters.read ? ' marked' : ''}"
                    onclick="markRead(this, ${post.id}); return false;"></a>
                <a title="Remove from this list" class="cancel"
                    onclick="remove(this, ${post.id}, ${postWithCounters.read}); return false"></a>
            </div>
        </div>
    </div>
</div>
