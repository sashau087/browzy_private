<html>
<head>
	<meta name="layout" content="standalone"/>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width = device-width, initial-scale = 1, user-scalable = no">
	<asset:stylesheet src="adaptive.css"/>
	<asset:stylesheet src="404.css"/>

</head>
<body>
<div id="p404">
	<div id="floater"></div>
	<div class="container">
		<div class="tx1">Oops<span>.</span></div>
		<div class="tx404"></div>
		<div class="tx2">seems this page is not found</div>
		<div class="bt_404"><g:link controller="front" action="index" alt="back home"><p>back home</p></g:link></div>
	</div>
</div>
<div class="right_line"></div>
<div class="bottom_line"></div>
<div class="logo_404"></div>
</body>
</html>
