<%@ page import="browzy.DbConst" %>
<%@ page import="browzy.User" %>
<%@ page import="browzy.CategoryUtil" %>

<g:setGlobalVars/>
<g:set var="isAdmin" value="${currentUser && currentUser.hasRole(DbConst.ROLE_ADMIN)}"/>

<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Browzy.com"/>
    </title>
    <meta name="viewport" content="width = device-width, initial-scale = 1, user-scalable = no">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800' rel='stylesheet' type='text/css'>

    <asset:stylesheet src="application.css"/>

    <asset:link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon.png"/>
    <asset:link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32"/>
    <asset:link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16"/>
    <asset:link rel="manifest" href="favicon/manifest.json"/>
    <asset:link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5"/>
    <asset:link rel="shortcut icon" href="favicon/favicon.ico"/>
    <meta name="msapplication-config" content="${assetPath(src: 'favicon/browserconfig.xml')}"/>
    <meta name="theme-color" content="#ffffff"/>

    <asset:javascript src="jquery-2.2.0.min.js"/>
    <asset:javascript src="jquery-ui.js"/>
    <asset:javascript src="jquery.contenteditable.js"/>
    <asset:javascript src="bootstrap.js"/>
    <asset:javascript src="bootstrap.file-input.js"/>
    <asset:javascript src="bootstrap-select.js"/>
    <asset:javascript src="file-input.js"/>
    <asset:javascript src="script.js"/>
    <asset:javascript src="utils.js"/>

    <g:layoutHead/>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-43413914-3', 'auto');
      ga('send', 'pageview');
    </script>

    <script>
        function selectCategory(el) {
            location = "${createLink(controller:'front', action:'category', id:'categoryId')}"
                .replace('categoryId', el.id);
        }

        <g:if test="${isAdmin}">
            function setPostStatus(postId, status) {
               location = "${createLink(controller:'admin', action:'setStatus', id:'postId')}?status=".replace('postId', postId) + status;
            }

            function setPostPriority(postId, priority) {
                $.get("${createLink(controller:'admin', action:'priority', id:'postId')}?priority=".replace('postId', postId) + priority);
            }

            function doPostAction(postId, action) {
                if (action != '-') {
                    if (action == 'delete' && !confirm('Delete post?')) {
                        return;
                    }
                    location = "${createLink(controller:'post', action:'action', id:'postId')}".replace('postId', postId).replace('action', action);
                }
            }
        </g:if>

        function likePost(parentElSelector, postId) {
            $.get("${createLink(controller: 'post', action: 'like', id:'postId')}".replace('postId', postId), function(response) {
                likeDislikeResponseHandler($(parentElSelector), response);
            });
        }

        function dislikePost(parentElSelector, postId) {
            $.get("${createLink(controller: 'post', action: 'dislike', id:'postId')}".replace('postId', postId), function(response) {
                likeDislikeResponseHandler($(parentElSelector), response);
            });
        }

        function starPost(elSelector, postId) {
            $(elSelector).load("${createLink(controller: 'post', action: 'star', id:'postId')}".replace('postId', postId), function() {
                starResponseHandler($(elSelector), response);
            });
        }
    </script>

    <sec:ifNotLoggedIn>
        <asset:stylesheet src="terms.css"/>
        <script>
            function showTerms() {
                $('#message').load("${createLink(controller: 'front', action: 'terms')}", function() {
                    $('#messageDialog').modal('show');
                });
            }
        </script>
    </sec:ifNotLoggedIn>

</head>

<body>
<sec:ifLoggedIn>
<g:set var="countReplies" value="${0}"/>
<g:each in="${post?.comments}" var="countComments">
    <g:each in="${countComments?.replies}" var="countRepliesEach">
        <g:if test="${countRepliesEach.user?.id == user.id && countRepliesEach.status_read=='unread'}">
            <g:set var="countReplies" value="${countReplies+1}"/>
        </g:if>
    </g:each>
</g:each>
</sec:ifLoggedIn>
<div id="menu">
    <div class="container">
        <div class="logo">
            <g:link controller="front" action="index"><asset:image src="logo.png" alt=""/></g:link>
        </div>

        <div class="trigger"><asset:image src="trigger.png" alt=""/></div>

        <sec:ifLoggedIn>
            <div class="usr submenu_trigger">
                <img src="${createLink(controller: 'user', action: 'avatar', id:currentUser.id, params:[width:30])}"
                     class="img-circle" alt="" />
                <ul class="user_submenu">
                    <li class="selected"><g:link controller="post" action="add">New post</g:link></li>
                    <li><g:link controller="user" action="profile">My profile</g:link></li>
                    <li><g:link controller="user" action="profile">Comments</g:link></li>
                    <li><g:link controller="user" action="edit">Settings</g:link></li>
                    <li><g:link controller="user" action="profile">Notifications</g:link></li>
                    <li><g:link controller="user" action="logout">Sign out</g:link></li>
                </ul>
            </div>

            <div class="add_pst"><a href="add_post.html"></a></div>
        </sec:ifLoggedIn>

        <ul class="top_menu">
            <sec:ifLoggedIn>
                <li><g:link controller="activity">Activity<span class="num"><g:include controller="activity" action="countUnread"/></span></g:link></li>
            </sec:ifLoggedIn>
            %{--<li class="submenu_trigger"><span class="trg">Most popular</span>
                <ul class="submenu popular">
                    <li><a href="page.html">Most Viewed</a></li>
                    <li><a href="page.html">Most Discussed</a></li>
                    <li><a href="page.html">Latest</a></li>
                </ul>
            </li>--}%
            <li class="submenu_trigger"><span class="trg">Categories  <i class="snum"></i></span>
                <g:set var="categories" value="${CategoryUtil.list()}"/>
                <div class="submenu selected_categories">
                    <ul>
                        <g:each var="category" in="${categories}">
                            <li class="menu_${category.name.toLowerCase()}">${category.name}
                                <span class="remove"><asset:image src="remove.png" alt=""/></span>
                            </li>
                        </g:each>
                    </ul>
                </div>

                <div class="submenu categories">
                    <g:each var="category" in="${categories}">
                        <div class="checkbox">
                            <input type="checkbox" id="${category.name.toLowerCase()}" onclick="selectCategory(this)" />
                            <label for="${category.name.toLowerCase()}">
                                ${category.name}
                            </label>
                        </div>
                    </g:each>
                </div>
            </li>
            <sec:ifLoggedIn>
                <li class="rating"><a href="rating.html">Authors rating</a></li>
                <li class="hidden_wide"><g:link controller="user" action="profile">My profile</g:link></li>
            </sec:ifLoggedIn>
            <li class="submenu_trigger search_icon"><asset:image src="search_icon.png" class="trg" alt=""/>
                <span class="hv trg">Search</span>

                <div class="submenu search">
                    <g:form controller="search" action="posts" method="post">
                        <div class="row">
                            <div class="col-xs-8">
                                <input type="text" placeholder="Find the post, tags, keyword etc." name="query"/>

                                <div class="clear_form"></div>
                            </div>

                            <div class="col-xs-4"><button>Search</button></div>
                        </div>
                    </g:form>
                </div>
            </li>
            <sec:ifNotLoggedIn>
                <li class="login_text">
                    %{--<a data-toggle="modal" data-target="#login">Login</a> or--}%
                    %{--<a class="register" data-toggle="modal" data-target="#login">Sign up</a>--}%
                </li>
            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
                <li class="message_link submenu_trigger">
                    <span class="countreplies" style="padding-bottom: 13px;" onclick="openReplies(${countReplies})"><asset:image src="envelop.png" alt=""/><br/> <b>${countReplies}</b></span>
                    <div class="submenu search commentsBlockForReplies">
                        <form>
                            <g:each in="${post?.comments}" var="countComments">
                                <g:each in="${countComments?.replies}" var="countRepliesEach">
                                    <g:if test="${countRepliesEach.user?.id == user.id && countRepliesEach.status_read=='unread'}">
                                        <div class="row">
                                            <div class="col-xs-4"><a class="commentsUnreaded" onclick="scrollToComment(${countRepliesEach.id}, this); return false;">Commen №${countRepliesEach.id}</a></div>
                                        </div><br>
                                    </g:if>
                                </g:each>
                            </g:each>

                        </form>
                    </div>
                </li>
                <li class="submenu_trigger usr">
                    <img src="${createLink(controller: 'user', action: 'avatar', id:currentUser.id, params:[width:30])}"
                         class="img-circle user_image trg" alt="" />
                    <ul class="submenu user_submenu">
                        <li class="selected"><g:link controller="post" action="add">New post</g:link></li>
                        <li><g:link controller="user" action="profile">My profile</g:link></li>
                        <li><g:link controller="user" action="profile">Comments</g:link></li>
                        <li><g:link controller="user" action="edit">Settings</g:link></li>
                        <li><g:link controller="user" action="profile">Notifications</g:link></li>
                        <li><g:link controller="user" action="logout">Sign out</g:link></li>
                    </ul>
                </li>
                <li>
                    <g:link controller="user" action="profile" class="profile_login">
                        <sec:username/>
                        <span>
                            <asset:image src="like_ico.png" alt=""/>
                            ${currentUser?.likes}
                        </span>
                    </g:link>
                </li>
                <li class="hidden_wide"><a href="#">Notification Settings</a></li>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <li class="right_link"><a class="login" data-toggle="modal" data-target="#login">Login</a></li>
            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
                <li class="right_link"><g:link class="login" controller="user" action="logout">Logout</g:link></li>
            </sec:ifLoggedIn>
            <sec:ifLoggedIn>
                <li class="right_link">
                    <g:if test="${isAdmin}">
                        <a href="#" class="add_post" data-toggle="dropdown"> Add post <span class="caret"></span></a>
                        <ul class="dropdown-menu submenu popular nav nav-tabs">
                            <li><g:link controller="post" action="add" class="add_post">Add post</g:link></li>
                            <li><g:link controller="video" action="add" class="add_post">Add video</g:link></li>
                        </ul>
                    </g:if>
                    <g:else>
                        <g:link controller="post" action="add" class="add_post">Add post</g:link>
                    </g:else>
                </li>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <li class="right_link"><a class="add_post" data-toggle="modal" data-target="#login">Add post</a></li>
            </sec:ifNotLoggedIn>
        </ul>
    </div>
</div>

<g:layoutBody/>

<div class="modal fade" id="login" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            <div class="clearfix"></div>
            <ul class="nav nav-tabs tmenu">
                <li class="active log"><a href="#log" data-toggle="tab">Login <span>or</span></a></li>
                <li class="reg"><a href="#reg" data-toggle="tab"><span>or</span>Registration</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="log">
                    %{--
                    <div class="txt">Choose one of the following methods</div>

                    <div class="row share">
                        <div class="col-xs-4">
                            <a href="#" class="fb">Facebook</a>
                        </div>

                        <div class="col-xs-4">
                            <a href="#" class="g">Google +</a>
                        </div>

                        <div class="col-xs-4">
                            <a href="#" class="tw">Twitter</a>
                        </div>
                    </div>

                    <div class="txt">Or</div>
                    --}%
                    <g:form controller="user" action="login">
                        <g:hiddenField name="nextAddPost" value="${flash.nextAddPost}" />
                        <div class="row input">
                            <input type="text" class="form-control" placeholder="Login" name="username"/>
                            <span class="form-control-feedback"></span>
                            <div class="error"></div>
                        </div>

                        <div class="row input">
                            <input type="password" class="form-control" placeholder="Password" name="password"/>
                            <span class="form-control-feedback"></span>
                            <div class="error"></div>
                        </div>

                        <div class="checkbox">
                            <input type="checkbox" id="remember-me-login" name="remember-me"/>
                            <label for="remember-me-login">Remember me next time</label>
                        </div>

                        <g:if test="${flash.invalidLogin}">
                            <div class="error" style="text-align:center;">Invalid username/password</div>
                        </g:if>

                        <div class="button">
                            <button onclick="return validateLogin()">Login</button>
                        </div>
                        <a id="remind_pass_link" data-toggle="modal"
                           data-target="#remind_pass">Forgot your password?</a>
                    </g:form>

                </div>

                <div class="tab-pane" id="reg">
                    %{--
                    <div class="txt">Choose one of the following methods</div>

                    <div class="row share">
                        <div class="col-xs-4">
                            <a href="#" class="fb">Facebook</a>
                        </div>

                        <div class="col-xs-4">
                            <a href="#" class="g">Google +</a>
                        </div>

                        <div class="col-xs-4">
                            <a href="#" class="tw">Twitter</a>
                        </div>
                    </div>

                    <div class="txt">Or</div>
                    --}%
                    <g:form name="registrationForm" controller="user" action="register">
                        <g:set var="errorUser" value="${flash.invalidRegistrationUser}"/>
                        <g:set var="errors" value="${errorUser?.errors}"/>
                        <div class="row input${errors?.hasFieldErrors('username') ? ' has-error' : ''}">
                            <input type="text" name="username" class="form-control" placeholder="Login"
                                   value="${errorUser?.username}"
                                   maxlength="${browzy.User.constrainedProperties['username']['maxSize']}"/>
                            <span class="form-control-feedback"></span>
                            <div class="error"><g:message error="${errors?.getFieldError('username')}" /></div>
                        </div>

                        <div class="row input${errors?.hasFieldErrors('password') ? ' has-error' : ''}">
                            <input type="password" name="password" class="form-control" placeholder="Password"/>
                            <span class="form-control-feedback"></span>
                            <div class="error"><g:message error="${errors?.getFieldError('password')}" /></div>
                        </div>

                        <div class="row input${errors?.hasFieldErrors('email') ? ' has-error' : ''}">
                            <input type="email" name="email" class="form-control" placeholder="Email"
                                   value="${errorUser?.email}"/>
                            <span class="form-control-feedback"></span>
                            <div class="error"><g:message error="${errors?.getFieldError('email')}" /></div>
                        </div>

                        <div class="row select${errors?.hasFieldErrors('country') ? ' has-error' : ''}">
                            <g:countrySelect class="selectpicker form-control select_border" name="country"
                                             value="${errorUser?.country}"
                                             noSelection="['': '-Choose your country-']"/>
                            <div class="error"><g:message error="${errors?.getFieldError('country')}" /></div>
                        </div>

                        <div class="row input${errors?.hasFieldErrors('gender') ? ' has-error' : ''}">
                            <div class="col-sm-2 col-xs-12 label_block">
                                Gender
                            </div>

                            <div class="col-sm-2 col-xs-6 ">
                                <div class="radio gender_radio">
                                    <g:radio id="male" name="gender"
                                             checked="${'m' == errorUser?.gender}" value="m"/>
                                    <label for="male">Male</label>
                                </div>
                            </div>

                            <div class="col-sm-2 col-xs-6">
                                <div class="radio gender_radio">
                                    <g:radio id="female" name="gender"
                                             checked="${'f' == errorUser?.gender}" value="f"/>
                                    <label for="female">Female</label>
                                </div>
                            </div>
                            <div class="error" style="clear:both;"><g:message error="${errors?.getFieldError('gender')}" /></div>
                        </div>

                        <div class="checkbox" style="margin-top:20px;">
                            <input type="checkbox" id="terms_agreed" name="terms_agreed"/>
                            <label for="terms_agreed">I agree to the <a href="#" onclick="showTerms()">terms of use</a></label>
                            <g:if test="${flash.termsNotAgreed}">
                                <div class="error" style="clear:both;">Please check if you agree to the terms</div>
                            </g:if>
                        </div>

                        <recaptcha:ifEnabled>
                            <recaptcha:recaptcha theme="light"/>
                            <g:if test="${flash.recaptchaFailed}">
                                <div class="error">Please confirm that you are not a robot</div>
                            </g:if>
                        </recaptcha:ifEnabled>

                        <div class="button">
                            <button>Registration</button>
                        </div>
                    </g:form>
                    <g:if env="development">
                        <script>
                            $("#registrationForm").find(".button").attr("oncontextmenu", "registerWithoutConfirmation(); return false;");
                            function registerWithoutConfirmation() {
                                $("#registrationForm").attr("action", "${createLink(controller:'user', action:'registerWithoutConfirmation')}").submit();
                            }
                        </script>
                    </g:if>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="messageDialog" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            <div class="clearfix"></div>

            <div id="message"></div>
        </div>
    </div>
</div>

%{--
<div class="modal fade" id="share" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            <div class="clearfix"></div>

            <div class="share_header_block">
                Like it? Share post!
            </div>

            <div class="row share">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="#" class="fb">Facebook</a>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="#" class="g">Google +</a>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="#" class="tw">Twitter</a>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="#" class="pt">Pinterest</a>
                </div>
            </div>
        </div>
    </div>
</div>
--}%

<div class="modal fade" id="like" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            Have something to say? Write!
        </div>
    </div>
</div>

<div class="modal fade" id="remind_pass" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            <div class="block_subheader">Password reset</div>

            <p class="text">An email will be sent to your Email address that includes a password reset link. Please, click the link in your email to reset.</p>

            <g:form controller="user" action="forgotPasswordEmail">
                <label>Email</label>
                <input type="text" class="form-control" name="email"/>

                <div class="row">
                    <div class="button col-sm-2">
                        <button>Send</button>
                    </div>
                    <sec:ifNotLoggedIn>
                        <div class="col-sm-10 link_block">
                            <p>Have an account? <a id="login_link" data-toggle="modal" data-target="#login">Login</a>
                            </p>

                            <p>Don't have an account? <a id="create_account" data-toggle="modal"
                                                        data-target="#login">Sign up</a></p>
                        </div>
                    </sec:ifNotLoggedIn>
                </div>
            </g:form>
        </div>
    </div>
</div>
<script>
    <g:if test="${flash.invalidRegistrationUser}">
        $('#login').modal('show');
        $('.tmenu a:last').tab('show');
    </g:if>
    <g:if test="${flash.invalidLogin || flash.showLogin}">
        $('#login').modal('show');
        $('.tmenu a:first').tab('show');
    </g:if>
    <g:if test="${flash.message}">
        showMessage('${flash.message}')
    </g:if>
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58bd81470a140418"></script>
</body>
</html>
