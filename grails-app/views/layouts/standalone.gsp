<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>
        <g:layoutTitle default="Browzy.com"/>
    </title>
    <g:layoutHead/>
</head>
<body>
    <g:layoutBody />
</body>
</html>