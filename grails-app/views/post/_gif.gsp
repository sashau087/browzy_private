<%@ page import="browzy.Const" %>
<div class="post_block add_gif ${it ? 'visible' : ''}">
    <div class="close"><asset:image src="close.png" alt="" name="delete_block"/></div>
    <asset:image src="red_arrow.png" alt="" class="red_arrow" />
    <div class="gray_rounded_block">
        <div class="dashed_rounded_block"
                style="${it ? 'background-image: url(' + createLink(controller:'post', action:'video', id:post?.id ?: tempPostId, params:[videoName:it.name]) + ')' : ''}">
            <div>Add gif</div>
            <asset:image src="gif.png"  class="center-block" alt=""/>
            <div class="or">Drop gif</div>
            <div id="upload_overall" class="upload"></div>
            <div class="or">or</div>
            <div class="button_container">
                <input type="file" class="form-control" data-filename-placement="inside"
                        accept="${Const.GIF_TYPE}" onChange="set_image(event)"
                        title="${it ? it.fileName : 'Upload gif'}"
                        ${it ? "name=${it.name}" : ''} />
            </div>
        </div>
    </div>
</div>
<g:if test="${it}">
<script>
    employBlock(
        $("[name='${it.name}']").closest('.post_block'),
        'gif',
        "${it.name.drop(3)}",
        'input');
</script>
</g:if>