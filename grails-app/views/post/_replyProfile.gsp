<div class="row  comment_block level2  com${comment.id}">
    <div class="col-sm-1 col-xs-2"><a href="/user/profile/${comment.user?.username.toString()}/${comment.user?.id.toString()}"><img src="/user/avatar/${comment.user?.id.toString()}?width=30"></a></div>
    <div class="col-sm-11 col-xs-10">
        <div class="col-md-8 col-sm-6 col-xs-12">
            <div class="name"><a href="/user/profile/${comment.user?.username.toString()}/${comment.user?.id.toString()}">${comment.user.username.toString()}</a> <span>${comment.date.toString()}</span></div>
            <div class="text">
                <p><span style="color: grey; text-decoration: underline;">${commentParent.user.username.toString()}</span>, <a href="/${post?.title}/${post?.id}#com${comment.id}">${comment.text.toString()}</a></p>
                <g:if test="${comment.avatarId!=null}">
                    <img src="${createLink(controller: 'comment', action: 'avatar', id: comment.id.toString())}" alt="" class="img-responsive img_comment" style="margin-bottom: 10px"/>
                </g:if>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 reply_block"  id="${comment.id.toString()}">
            <a class="likes likesComment${comment.id.toString()}
            <g:if test="${comment.getLikesCommentUser(user)==1}">likesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="likeComment(${comment.id.toString()})"</sec:ifLoggedIn><sec:ifNotLoggedIn> data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                ${comment.likesComment}
            </a>
            <a class="dislikes dislikesComment${comment.id.toString()}
            <g:if test="${comment.getDislikesCommentUser(user)==1}">dislikesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="dislikeComment(${comment.id.toString()})"</sec:ifLoggedIn><sec:ifNotLoggedIn> data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                ${comment.dislikesComment}
            </a>
        </div>
        <sec:ifLoggedIn>
            <div class="row add_post_form replyBlock${comment.id}" style="display: none;">
                <g:form enctype="multipart/form-data" name="fileinfo">
                    <div class="col-md-8 col-sm-9 textarea_container">
                        <textarea class="form-control commentBox${comment.id}" name="text" placeholder="Аdd comment"></textarea>
                    </div>
                    <div class="col-md-2 col-sm-3 file_container">
                        <input id="${comment.id}" type="file" class="form-control commentfileinput commentfile${comment.id} commentFileReply" data-filename-placement="inside" name="image${comment.id}" title="Upload image" />
                    </div>
                    <div id="post_errors" class="error"></div>
                    <div class="col-md-2 col-sm-4 col-md-offset-0 col-sm-offset-8">
                        <button class="add_comment_reply additional" onclick="add_comment_reply(${comment.id}, event)">add comment</button>
                    </div>
                    <div class="img_comment${comment.id}" style="width: 300px !important;"></div>
                </g:form>
            </div><br>
        </sec:ifLoggedIn>
    </div>
</div>

<g:each in="${comment.replies}" var="child">
    <g:render template="/post/replyProfile" model="${[comment: child, commentParent: comment]}" />
</g:each>


