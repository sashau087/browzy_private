<%@ page import="browzy.PostUtil" %>
<%@ page import="browzy.PostStatus" %>
<%@ page import="browzy.DbConst" %>
<!doctype html>
<html>
<head>

    <title>Browzy.com - ${post.title}</title>
    <meta name="description" content="${post.text}"/>

    <meta property="og:url" content="${createLink(controller:'post', action:'view', id:post.id, params:[title: PostUtil.urlTitle(post)], absolute:true).replace('http://b', 'https://b')}" />
    <meta property="og:title" content="${post.title}" />
    <meta property="og:description" content="${post.text}" />
    <meta property="og:image" content="${createLink(controller:'post', action:'image', id:post.id, absolute:true)}" />
    <meta property="og:image:secure_url" content="${createLink(controller:'post', action:'image', id:post.id, absolute:true).replace('http://b', 'https://b')}" />
    <asset:javascript src="comment.js"/>
    <script>
        function like() {
            $.get("${createLink(controller: 'post', action: 'like', id:post.id)}", function(response) {
                likeDislikeResponseHandler($(".post_info_block"), response);
                setLiked();
            });
        }

        function dislike() {
            $.get("${createLink(controller: 'post', action: 'dislike', id:post.id)}", function(response) {
                likeDislikeResponseHandler($(".post_info_block"), response);
                setDisliked();
            });
        }

        function star() {
            $(".post_info_block .star2").load("${createLink(controller: 'post', action: 'star', id:post.id)}", function(response) {
                starResponseHandler($(".post_info_block .star2"), response);
                setStared();
            });
        }

        $(document).ready(function() {
            $(".likes2").click(like);
            $(".dislikes2").click(dislike);

            <% if (liked) { %>
                setLiked();
            <% } %>
            <% if (disliked) { %>
                setDisliked();
            <% } %>

            <% if (!stared) { %>
                $('.star').click(star);
            <% } else { %>
                setStared();
            <% } %>
        });
        // ${liked} ${disliked}
    </script>
</head>
<body>


    <g:set var="preview" value="${PostStatus.DRAFT == post.status && post.author.id == user?.id}" />
    <div id="post_block" style="background: #f0f2f5 url(${createLink(controller:'post', action:'cover', id:post.id)}) top center no-repeat; background-size: 100% 400px;">
        <div class="container relative_block">
            <div class="left_line"></div>
            <div class="right_line"></div>
            <g:if test="${prevPost}">
                <g:postLink post="${prevPost}" class="prv fixed">
                    <div>
                        <g:set var="firstMedia" value="${PostUtil.firstMedia(prevPost)}"/>
                        <g:if test="${'image' == firstMedia?.type}">
                            <img src="${createLink(controller:'post', action:'image', id:(prevPost.id))}" />
                        </g:if>
                        <g:elseif test="${'video' == firstMedia?.type}">
                            <g:videoImage size="small">${firstMedia.content}</g:videoImage>
                        </g:elseif>
                        <p class="prev_title">${prevPost.title}</p>
                    </div>
                </g:postLink>
            </g:if>
            <div style="float: right">
                <g:if test="${nextPost}">
                    <g:postLink post="${nextPost}" class="nxt fixed">
                        <div>
                            <g:set var="firstMedia" value="${PostUtil.firstMedia(nextPost)}"/>
                            <g:if test="${'image' == firstMedia?.type}">
                                <img src="${createLink(controller:'post', action:'image', id:(nextPost.id))}" />
                            </g:if>
                            <g:elseif test="${'video' == firstMedia?.type}">
                                <g:videoImage size="small">${firstMedia.content}</g:videoImage>
                            </g:elseif>
                            <p class="next_title">${nextPost.title}</p>
                        </div>
                    </g:postLink>
                </g:if>
                <g:if test="${user}">
                    <a class="tt likes2 fixed" title="If you like click here" data-placement="left"></a>
                    <a class="tt dislikes2 fixed" title="If you dislike click here" data-placement="left"></a>
                    <sec:ifNotLoggedIn>
                        <a class="tt comments2 fixed" data-toggle="modal" data-target="#login" title="If you want to comment click here" data-placement="left"></a>
                    </sec:ifNotLoggedIn>
                    <sec:ifLoggedIn>
                        <a class="tt comments2 fixed leave_comment"  title="If you want to comment click here" data-placement="left"></a>
                    </sec:ifLoggedIn>

                    <a class="tt share2 fixed"  data-toggle="modal" data-target="#share" title="If you want to share click here" data-placement="left"></a>
                    <a class="tt star fixed" data-html="true" title="If you want to favorite click here" data-placement="left"></a>
                </g:if>
            </div>
            <div class="post_header_block">
                <div class="block_header">
                    <h1>${post.title}</h1>
                    <g:if test="${preview}">
                        <g:link controller="post" action="edit" id="${post.id}">
                            <button class="preview">Back to edit</button>
                        </g:link>
                    </g:if>
                </div>
                <div class="user">
                    <div class="addthis_inline_share_toolbox" style="float:right;"
                         data-url="${createLink(controller:'post', action:'view', id:post.id, params:[title: PostUtil.urlTitle(post)], absolute:true)}"
                         data-title="${post.title}"
                         data-description="${post.text}"
                         data-media="${createLink(controller:'post', action:'image', id:post.id, absolute:true)}"></div>
                    <div class="date">${post.approved?.getDateString()}</div>
                    %{--<g:set var="userdata" value="${post?.author ?: author}"/>--}%
                    <div><g:userLink user="${post.author}"><img src="${createLink(controller:'user', action:'avatar', id: post.author.id, params:[width:30])}" style="width: 30px; height: 30px" class="img-circle" alt=""/></g:userLink></div>
                    <div><g:userLink user="${post.author}" class="name">${post.author.displayName}</g:userLink></div>
                    <div class="add"><g:link controller="user" action="follow" id="${post?.author?.id}"/></div>
                </div>

                <div class="tag_line">
                    tags:
                    <g:link controller="search" action="posts" params="[query: post.categories[0].name]">${post.categories[0].name}</g:link>
                    <g:each var="tag" in="${post.tags}">
                        <g:link controller="search" action="posts" params="[query: tag.text]">${tag.text}</g:link>
                    </g:each>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="post_content">
                        <p style="white-space:pre-line;">${post.text}</p>
                        <g:each var="block" in="${structure}">
                            <g:if test="${block.type == 'text'}">
                                <p style="white-space:pre-line;">${post.texts.find({it.name == block.name}).content}</p>
                            </g:if>
                            <g:elseif test="${block.type == 'image'}">
                                <g:set var="image" value="${post.images.find({it.name == block.name})}"/>
                                <p><img src="${createLink(controller:'post', action:'image', id:post.id, params:[imageName:block.name])}" class="img-responsive" alt=""/></p>
                            </g:elseif>
                            <g:elseif test="${block.type == 'video'}">
                                <g:video>${post.texts.find({it.name == block.name}).content}</g:video>
                            </g:elseif>
                            <g:elseif test="${block.type == 'gif'}">
                                <p><img src="${createLink(controller:'post', action:'video', id:post.id, params:[videoName:block.name])}" class="img-responsive" alt=""/></p>
                            </g:elseif>
                            <g:elseif test="${block.type == 'link'}">
                                <g:set var="link" value="${post.texts.find({it.name == block.name}).content}"/>
                                <p><a href="${link}">${link}</a></p>
                            </g:elseif>
                        </g:each>
                        <g:if test="${post.link}">
                            Source link: <a href="${post.link}" target="_blank">${post.link}</a>
                        </g:if>
                    </div>
                    <g:if test="${!preview}">
                        <div class="post_block">
                            <div class="addthis_inline_share_toolbox"
                                 data-url="${createLink(controller:'post', action:'view', id:post.id, params:[title: PostUtil.urlTitle(post)], absolute:true)}"
                                 data-title="${post.title}"
                                 data-description="${post.text}"
                                 data-media="${createLink(controller:'post', action:'image', id:post.id, absolute:true)}"></div>
                        </div>

                        <div class="post_info_block">
                            <div class="row">
                            <div class="col-sm-4 col-xs-6 col-md-2">
                                <span class="looks">${post.looks}</span>
                            </div>
                            <div class="col-sm-4 col-xs-6 col-md-2">
                                <sec:ifLoggedIn>
                                    <span class="likes active" onclick="like()">${post.likes}</span>
                                </sec:ifLoggedIn>
                                <sec:ifNotLoggedIn>
                                    <span class="likes active" data-toggle="modal" data-target="#login">${post.likes}</span>
                                </sec:ifNotLoggedIn>
                            </div>
                            <div class="col-sm-4 col-xs-6 col-md-2">
                                <sec:ifLoggedIn>
                                    <span class="dislikes active red" onclick="dislike()">${post.dislikes}</span>
                                </sec:ifLoggedIn>
                                <sec:ifNotLoggedIn>
                                    <span class="dislikes active red" data-toggle="modal" data-target="#login">${post.dislikes}</span>
                                </sec:ifNotLoggedIn>
                            </div>
                                <div class="col-sm-4 col-xs-6 col-md-2">
                                <span class="comments">${post.comments?.size()}</span>
                            </div>
                            <div class="col-sm-4 col-xs-6 col-md-2">
                                <span class="share">${post.shares}</span>
                            </div>
                            <div class="col-sm-4 col-xs-6 col-md-2">
                                <sec:ifLoggedIn>
                                    <span class="star2 active red" onclick="star()">${post.stars}</span>
                                </sec:ifLoggedIn>
                                <sec:ifNotLoggedIn>
                                    <span class="star2 active red" data-toggle="modal" data-target="#login">${post.stars}</span>
                                </sec:ifNotLoggedIn>
                            </div>

                            </div>
                        </div>
                    </g:if>
                </div>
                <div class="col-md-4 right_pane">
                    <g:if test="${user && user.hasRole(DbConst.ROLE_ADMIN) && post.status == PostStatus.APPROVE}">
                        <div class="pane_block">
                            <div class="block_header">
                                <g:link class="more_button" controller="post" action="approve" id="${post.id}">Approve</g:link>
                            </div>
                        </div>
                    </g:if>
                    <div class="pane_block">
                        <div class="other_posts">
                            <p>Other posts by this author</p>
                            <g:link class="more_button" controller="front" action="author" id="${post.author.id}">Other posts</g:link>
                        </div>
                    </div>
                    <div class="pane_block">
                        <div  class="block_header">Authors of the month</div>
                        <span>Coming soon</span>
                        %{--TODO: authors of the month--}%
                        %{--<div class="row author_item">
                            <div class="col-md-3 author_image"><a href="profile.html"><asset:image src="u1.png" class="img-responsive" alt=""/></a></div>
                            <div class="col-md-9 author_text">
                                <div class="name"><a href="profile.html">Joe Doe</a></div>
                                <span class="looks">40 638</span>
                                <span class="likes">8 637</span>
                                <span class="subscribers">1 038</span>
                            </div>
                        </div>
                        <div class="row author_item">
                            <div class="col-md-3 author_image"><a href="profile.html"><asset:image src="u2.png" class="img-responsive" alt=""/></a></div>
                            <div class="col-md-9 author_text">
                                <div class="name"><a href="profile.html">Marina Gondra Alonso</a></div>
                                <span class="looks">93 849</span>
                                <span class="likes">10 847</span>
                                <span class="subscribers">537</span>
                            </div>
                        </div>
                        <div class="row author_item">
                            <div class="col-md-3 author_image"><a href="profile.html"><asset:image src="u3.png" class="img-responsive" alt=""/></a></div>
                            <div class="col-md-9 author_text">
                                <div class="name"><a href="profile.html">Cristy Fox</a></div>
                                <span class="looks">11 793</span>
                                <span class="likes">6 030</span>
                                <span class="subscribers">420</span>
                            </div>
                        </div>
                        <div class="row author_item">
                            <div class="col-md-3 author_image"><a href="profile.html"><asset:image src="u4.png" class="img-responsive" alt=""/></a></div>
                            <div class="col-md-9 author_text">
                                <div class="name"><a href="profile.html">Michael Smith</a></div>
                                <span class="looks">15 538</span>
                                <span class="likes">2 367</span>
                                <span class="subscribers">674</span>
                            </div>
                        </div>
                        <div class="row author_item">
                            <div class="col-md-3 author_image"><a href="profile.html"><asset:image src="u5.png" class="img-responsive" alt=""/></a></div>
                            <div class="col-md-9 author_text">
                                <div class="name"><a href="profile.html">Diana Snow</a></div>
                                <span class="looks">9 002</span>
                                <span class="likes">1 690</span>
                                <span class="subscribers">230</span>
                            </div>
                        </div>--}%
                    </div>

                    <g:include controller="front" action="trendingSidebar" params="[titleHeader: 'h2']"/>

                </div>
            </div>
            <div class="white_block posts_mobile">
                <div class="row">
                    <div class="col-sm-6"><p>Other posts by this author</p></div>
                    <div class="col-sm-6"><g:userLink user="${post.author}" class="more_button">Other posts</g:userLink></div>
                        %{--<a class="more_button" href="profile.html">Other posts</a></div>--}%
                </div>
            </div>

                    <g:set var="maxId" value="${0}"/>
                    <g:set var="comments" value="${0}"/>
                    <g:set var="count" value="${0}"/>
                    <g:each in="${post?.comments?}" var="p">
                        <g:if test="${p.likesComment>=10}">
                            <g:if test="${count==0}">
                                <g:set var="maxId" value="${p.id}"/>
                                <g:set var="comments" value="${p.likesComment}"/>
                            </g:if>

                            <g:if test="${p.likesComment>comments}">
                                <g:set var="maxId" value="${p.id}"/>
                                <g:set var="comments" value="${p.likesComment}"/>
                            </g:if>
                            <g:set var="count" value="${count+1}"/>
                        </g:if>
                    </g:each>
                    <g:each in="${post?.comments?}" var="p">
                        <g:if test="${p.likesComment>=10 && p.id==maxId}">
                            <div class="white_block">
                                <div class="subheader">Top comment</div>
                                <div class="row top_comment">
                                    <div class="col-md-1 col-xs-3"><a href="/user/profile/${p?.user?.username}/${p?.user?.id}"><img src="/user/avatar/${p?.user?.id}?width=30" class=" center-block img-circle" alt=""/></a></div>
                                    <div  class="col-md-2 col-xs-9 person_name">
                                        <p><a href="/user/profile/${p?.user?.username}/${p?.user?.id}">${p?.user?.username}</a></p>
                                        <p><span>${p?.date}</span></p>
                                    </div>
                                    <div  class="col-md-7 col-xs-12">
                                        <div class="comment_text">${p?.text}</div>
                                    </div>
                                    <div  class="col-md-2 col-xs-12 reply_block">
                                        <a class="likes likesComment${p?.id}
                                        <g:if test="${p?.getLikesCommentUser(user)==1}">likesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="likeComment(${p?.id})"</sec:ifLoggedIn> <sec:ifNotLoggedIn>data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                                            ${p?.likesComment}
                                        </a>
                                        <a class="dislikes dislikesComment${p?.id}
                                        <g:if test="${p?.getDislikesCommentUser(user)==1}">dislikesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="dislikeComment(${p.id})"</sec:ifLoggedIn><sec:ifNotLoggedIn> data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                                            ${p?.dislikesComment}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </g:if>

                    </g:each>
                    <g:if test="${count==0}">

                    </g:if>

            <g:if test="${!preview}">
                <div class="white_block">
                    <div class="subheader">Partner's news</div>
                    <span>Coming soon</span>
                    %{--TODO: partners news--}%
                    %{--<div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="preview col-xs-6"><a href="post.html"><asset:image src="i12.jpg" class="img-responsive center-block" alt=""/></a></div>
                            <div class="description col-xs-6"><a href="post.html">Johnny Times – Abyssinian Cat Johnny’s Daily Adventure</a></div>
                        </div>
                        <div class="col-md-6  col-xs-12">
                            <div class="preview col-xs-6"><a href="post.html"><asset:image src="i13.jpg"  class="img-responsive  center-block" alt=""/></a></div>
                            <div class="description col-xs-6"><a href="post.html">I Create Funny Portraits Of Dogs In A Comics Style</a></div>
                        </div>
                    </div>--}%
                </div>
                %{--${post?.comments.getAt(2).avatarId}--}%
                    <div class="white_block" id="leave_comment">
                        <div class="subheader">What do you think ?</div>
                        <div class="row add_post_form">
                            <g:form enctype="multipart/form-data" name="fileinfo">
                            <div class="col-md-8 col-sm-9 textarea_container">
                                <sec:ifNotLoggedIn>
                                    <textarea class="form-control" name="text" placeholder="Register please to leave a comment"></textarea>
                                </sec:ifNotLoggedIn>
                                <sec:ifLoggedIn>
                                    <textarea class="form-control commentBox" name="text" placeholder="Аdd comment"></textarea>
                                </sec:ifLoggedIn>
                            </div>
                            <div class="col-md-2 col-sm-3 file_container">
                                <sec:ifNotLoggedIn>
                                    <input type="file" id="openForbidden" class="form-control commentfileinput" data-toggle="modal" data-target="#login" title="Upload image" />
                                </sec:ifNotLoggedIn>
                                <sec:ifLoggedIn>
                                    <input type="file" class="form-control commentFile commentfileinput" data-filename-placement="inside" name="image" title="Upload image" />

                                </sec:ifLoggedIn>
                            </div>

                            <div id="post_errors" class="error"></div>
                            <div class="col-md-2 col-sm-4 col-md-offset-0 col-sm-offset-8">
                                <sec:ifNotLoggedIn>
                                    <button data-toggle="modal" id="sendForbidden" data-target="#login">add comment</button>
                                </sec:ifNotLoggedIn>
                                <sec:ifLoggedIn>
                                    <button class="add_comment">add comment</button>
                                </sec:ifLoggedIn>
                            </div>
                            </g:form>
                            <div class="img_comment" style="width: 300px !important;"></div>
                        </div>
                    </div>
                <g:set var="last" value="0"/>
                <g:set var="countReplies" value="${0}"/>
                <g:if test="${post?.comments?.size()>0}">
                    <g:each in="${post?.comments?}" var="p2">
                        <g:each in="${p2.replies}" var="p3">
                            <g:set var="countReplies" value="${countReplies+1}"/>
                        </g:each>
                    </g:each>
                </g:if>
                <g:if test="${post?.comments?.size()>0}">
                    <g:each in="${post?.comments?}" var="p2">
                        <g:each in="${p2.replies}" var="p3">
                            <g:if test="${p3.id!=p2.id}">
                                <g:set var="last" value="${post?.comments?.take(10+countReplies).last().id}"/>
                            </g:if>
                        </g:each>
                    </g:each>
                </g:if>
                <div class="white_block" id="block_of_comments" postId="${post?.id}" lastId="${last}">
                    <div class="subheader subComment"><h3>Other comments</h3></div>
                    <g:if test="${post?.comments?.size()==0}">
                        <span class="noComments">No comment yet. Be the first!
                            <sec:ifNotLoggedIn>
                                <a data-toggle="modal" data-target="#login" style="cursor: pointer">Leave comment</a>
                            </sec:ifNotLoggedIn>
                            <sec:ifLoggedIn>
                                <a class="leave_comment" style="cursor: pointer">Leave comment</a>
                            </sec:ifLoggedIn>
                        </span>
                    </g:if>
                    <g:each in="${post?.comments?.take(10+countReplies)}" var="p">
                        <g:set var="replyAccess" value="true"/>
                        <g:each in="${post?.comments?}" var="p2">
                            <g:each in="${p2.replies}" var="p3">
                                <g:if test="${p3.id==p.id}">
                                    <g:set var="replyAccess" value="false"/>
                                </g:if>
                            </g:each>
                        </g:each>
                        <g:if test="${replyAccess=='true'}">
                            <div class="row comment_block com${p?.id}">
                        <div class="col-sm-1 col-xs-2" id="user" ><a href="/user/profile/${p?.user?.username}/${p?.user?.id}" class="comUser"><img src="/user/avatar/${p?.user?.id}?width=30" class="img-circle center-block" alt=""/></a></div>
                        <div class="col-sm-11 col-xs-10">
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <div class="name"><a href="/user/profile/${p?.user?.username}/${p?.user?.id}">${p?.user?.username}</a> <span id="comment_date">${p?.date}</span></div>
                                <div class="text">${p?.text}</div>
                                <g:if test="${p?.avatarId!=null}">
                                        <img src="${createLink(controller: 'comment', action: 'avatar', id: p?.id)}" alt="" class="img-responsive img_comment" style="margin-bottom: 10px"/>
                                </g:if>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 reply_block" id="${p?.id}">
                                <g:if test="${user?.hasRole(DbConst.ROLE_ADMIN)}">
                                    <button class="deleteComment${p?.id}" onclick="deleteComment(${p?.id})">Delete</button>
                                </g:if>
                                <sec:ifNotLoggedIn>
                                    <button data-toggle="modal" data-target="#login">Reply</button>
                                </sec:ifNotLoggedIn>
                                <sec:ifLoggedIn>
                                    <button class="replyButton${p?.id}" onclick="reply(${p?.id})">Reply</button>
                                </sec:ifLoggedIn>

                                <a class="likes likesComment${p?.id}
                                    <g:if test="${p?.getLikesCommentUser(user)==1}">likesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="likeComment(${p?.id})"</sec:ifLoggedIn><sec:ifNotLoggedIn> data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                                    ${p?.likesComment}
                                </a>
                                <a class="dislikes dislikesComment${p?.id}
                                   <g:if test="${p?.getDislikesCommentUser(user)==1}">dislikesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="dislikeComment(${p.id})"</sec:ifLoggedIn><sec:ifNotLoggedIn> data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                                    ${p?.dislikesComment}
                                </a>
                            </div>
                            <sec:ifLoggedIn>
                                <div class="row add_post_form replyBlock${p?.id}" style="display: none;">
                                    <g:form enctype="multipart/form-data" name="fileinfo">
                                        <div class="col-md-8 col-sm-9 textarea_container">
                                                <textarea class="form-control commentBox${p?.id}" name="text" placeholder="Аdd comment"></textarea>
                                        </div>
                                        <div class="col-md-2 col-sm-3 file_container">
                                                <input id="${p?.id}" type="file" class="form-control commentfileinput commentfile${p?.id} commentFileReply" data-filename-placement="inside" name="image${p?.id}" title="Upload image" />
                                        </div>
                                        <div id="post_errors" class="error"></div>
                                        <div class="col-md-2 col-sm-4 col-md-offset-0 col-sm-offset-8">
                                                <button class="add_comment_reply" onclick="add_comment_reply(${p?.id}, event)">add comment</button>
                                        </div>
                                        <div class="img_comment${p?.id}" style="width: 300px !important;"></div>
                                    </g:form>
                                </div><br>
                            </sec:ifLoggedIn>

                            <g:each in="${p?.replies}" var="media">
                                <g:render template="step" model="${[comment: media, commentParent: p, u: user, dbconst: DbConst]}" />
                            </g:each>
                        </div>
                    </div>
                        </g:if>
                        </g:each>
                    <div style="display: none" class="com">
                    </div>
                    <g:if test="${post?.comments?.size()>(10+countReplies)}">
                        <a class="more_button mtop" id="more_comments">More comments</a>
                    </g:if>
                </div>
                <div class="white_block">
                    <div class="white_block_header"><h3>It may be interesting</h3></div>
                    <div class="row">
                        <g:each var="similarPost" in="${similar}">
                            <div class="col-md-4">
                                <div class="preview">
                                    <g:postLink post="${similarPost}">
                                        <g:set var="firstMedia" value="${PostUtil.firstMedia(similarPost)}"/>
                                        <g:if test="${'image' == firstMedia?.type}">
                                            <img src="${createLink(controller:'post', action:'image', id:(similarPost.id))}"
                                                 class="img-responsive center-block similar" alt=""/>
                                        </g:if>
                                        <g:elseif test="${'video' == firstMedia?.type}">
                                            <img class="videolink" />
                                            <g:videoImage class="img-responsive center-block similar" size="large">${firstMedia.content}</g:videoImage>
                                        </g:elseif>
                                    </g:postLink>
                                </div>
                                <div class="description">
                                    <g:postLink post="${similarPost}">${similarPost.title}</g:postLink>
                                </div>
                            </div>
                        </g:each>
                    </div>
                    <g:link controller="front" action="trending" class="more_button">More posts</g:link>
                </div>
            </g:if>
            <g:else>
                <g:link controller="post" action="edit" id="${post.id}" style="font-size:40px; font-weight:bold; line-height:44px;">
                    <button class="preview">Back to edit</button>
                </g:link>
            </g:else>
        </div>
    <script>
    document.getElementById('openForbidden').onclick = function (e) { e.preventDefault(); };
    document.getElementById('sendForbidden').onclick = function (e) { e.preventDefault(); };
    var s=1;

    </script>
</body>
</html>
