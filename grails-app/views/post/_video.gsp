<div class="post_block add_video ${it ? 'visible' : ''}">
<div class="close"><asset:image src="close.png" alt="" name="delete_block"/></div>
<asset:image src="red_arrow.png" alt="" class="red_arrow" />
<div class="link_rounded_block">
    <div>Add link on Youtube.com, Vimeo.com</div>
    <asset:image src="vid.png"  class="center-block" alt=""/>
    <div class="input">
        <input type="text" class="form-control" placeholder="Add link"
        ${it ? "name=${it.name} value=${it.content}" : ''} />
        <span class="form-control-feedback"></span>
    </div>
</div>
</div>
<g:if test="${it}">
<script>
    employBlock(
        $("[name='${it.name}']").closest('.post_block'),
        'video',
        "${it.name.drop(5)}",
        'input');
</script>
</g:if>