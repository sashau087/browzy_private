<g:set var="replyAccess" value="true"/>
<g:each in="${comments}" var="p2">
    <g:each in="${p2.replies}" var="p3">
        <g:if test="${p3.id==p.id}">
            <g:set var="replyAccess" value="false"/>
        </g:if>
    </g:each>
</g:each>
<g:if test="${class1!=''}">
    <g:set var="replyAccess" value="true"/>
    </g:if>
<g:if test="${replyAccess=='true'}">
    <div class="row comment_block${class1} com${p?.id}">
        <div class="col-sm-1 col-xs-2" id="user" ><a href="/user/profile/${p?.user?.username}/${p?.user?.id}" class="comUser"><img src="/user/avatar/${p?.user?.id}?width=30" class="img-circle center-block" alt=""/></a></div>
        <div class="col-sm-11 col-xs-10">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="name"><a href="/user/profile/${p?.user?.username}/${p?.user?.id}">${p?.user?.username}</a> <span id="comment_date">${p?.date}</span></div>
                <div class="text">${p?.text}</div>
                <g:if test="${p?.avatarId!=null}">
                    <img src="${createLink(controller: 'comment', action: 'avatar', id: p?.id)}" alt="" class="img-responsive img_comment" style="margin-bottom: 10px"/>
                </g:if>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 reply_block" id="${p?.id}">
                <sec:ifNotLoggedIn>
                    <button data-toggle="modal" data-target="#login">Reply</button>
                </sec:ifNotLoggedIn>
                <sec:ifLoggedIn>
                    <button class="replyButton${p?.id}" onclick="reply(${p?.id})">Reply</button>
                </sec:ifLoggedIn>

                <a class="likes likesComment${p?.id}
                <g:if test="${p?.getLikesCommentUser(user)==1}">likesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="likeComment(${p?.id})"</sec:ifLoggedIn><sec:ifNotLoggedIn> data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                    ${p?.likesComment}
                </a>
                <a class="dislikes dislikesComment${p?.id}
                <g:if test="${p?.getDislikesCommentUser(user)==1}">dislikesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="dislikeComment(${p.id})"</sec:ifLoggedIn><sec:ifNotLoggedIn> data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                    ${p?.dislikesComment}
                </a>
            </div>
            <sec:ifLoggedIn>
                <div class="row add_post_form replyBlock${p?.id}" style="display: none;">
                    <g:form enctype="multipart/form-data" name="fileinfo">
                        <div class="col-md-8 col-sm-9 textarea_container">
                            <textarea class="form-control commentBox${p?.id}" name="text" placeholder="Аdd comment"></textarea>
                        </div>
                        <div class="col-md-2 col-sm-3 file_container">
                            <input type="file" id="${p?.id}" class="form-control commentfileinput commentinput commentfile${p?.id} commentFileReply" data-filename-placement="inside" name="image${p?.id}" title="Upload image"/>
                        </div>
                        <div id="post_errors" class="error"></div>
                        <div class="col-md-2 col-sm-4 col-md-offset-0 col-sm-offset-8">
                            <button class="add_comment_reply${class2}" onclick="add_comment_reply(${p?.id}, event)">add comment</button>
                        </div>
                        <div class="img_comment${p?.id}" style="width: 300px !important;"></div>
                    </g:form>
                </div><br>
            </sec:ifLoggedIn>

            <g:each in="${p?.replies}" var="media">
                <g:render template="/post/step" model="${[comment: media, commentParent: p]}" />
            </g:each>
        </div>
    </div>
</g:if>
<script>
    $('.commentinput').bootstrapFileInput();
    $(".commentFileReply").each(function () {
        $(this).change(function(){

            readURL(this, $(this).attr('id'));
        });
    })
</script>