<%@ page import="browzy.DbConst" %>
<div class="post_block add_text ${it ? 'visible' : ''}">
    <div class="close"><asset:image src="close.png" alt="" name="delete_block"/></div>
    <asset:image src="red_arrow.png" alt="" class="red_arrow" />
    <div class="textarea">
        <textarea  class="form-control" placeholder="Add text" ${it ? "name=${it.name}" : ''}>${it?.content}</textarea>
        <div class="constraint">Maximum text length is ${DbConst.MAX_TEXT_SIZE}</div>
    </div>
</div>
<g:if test="${it}">
<script>
    employBlock(
        $("[name='${it.name}']").closest('.post_block'),
        'text',
        "${it.name.drop(4)}",
        'textarea');
</script>
</g:if>