<%@ page import="grails.util.Environment" %>
<%@ page import="browzy.DbConst" %>
<%@ page import="browzy.PostUtil" %>
<%@ page import="browzy.PostStatus" %>
<g:if test="${headers}">
    <g:set var="header" value="${headers.find {it.post.id == post.id} }" />
    <g:if test="${header}">
        <div class="index_subheader"><span>${header.header}</span></div>
    </g:if>
</g:if>
<div id="post${post.id}" class="pane_block ${post.status}">
    <div class="rounded_image">
        <g:if test="${currentUser && currentUser.id == post.author.id && (post.status == PostStatus.DRAFT || post.status == PostStatus.APPROVE)}">
            <g:link controller="post" action="edit" id="${post.id}"><span class="edit_post">EDIT</span></g:link>
        </g:if>
        <g:set var="firstMedia" value="${PostUtil.firstMedia(post)}"/>
        <g:if test="${'image' == firstMedia?.type}">
            <img src="${createLink(controller:'post', action:'image', id:post.id)}" class="img-responsive center-block post-img" alt=""/>
        </g:if>
        <g:elseif test="${'video' == firstMedia?.type}">
            <g:postLink post="${post}" class="videolink" />
            <g:videoImage class="img-responsive center-block post-img">${firstMedia.content}</g:videoImage>
        </g:elseif>
        %{--TODO: video/image icon--}%
        %{--<asset:image src="video2.png" class="img-responsive center-block" alt=""/>--}%
        %{--<asset:image src="video.png" class="icon" alt=""/>--}%
        <div class="add_items">
            %{--<a class="share" data-toggle="modal" data-target="#share">129</a>--}%
            %{--<a class="star" data-toggle="tooltip" data-placement="top" data-trigger="click" data-html="true" data-title="Post marked as <a href='profile4.html'>favorite</a> ">240</a>--}%
            <a class="star">${post.stars}</a>
        </div>
    </div>
    <div class="text_block">
        <h2 class="subheader">
            <g:postLink post="${post}">${post.title}</g:postLink>
            <!--<span class="popular">${post.status}</span>-->
        </h2>
        <div class="date"><g:formatDate date="${post.approved ?: post.created}" type="date" style="MEDIUM"/></div>
        <div class="user">
            <div><g:userLink user="${post.author}"><img src="${createLink(controller:'user', action:'avatar', id:post.author.id, params:[width:30])}" style="width: 30px; height: 30px" class="img-circle" alt=""/></g:userLink></div>
            <div><g:userLink user="${post.author}" class="name">${post.author.displayName}</g:userLink></div>
            <g:if test="${currentUser && post.author.id != currentUser.id}">
                <div class="add"><g:link controller="user" action="follow" id="${post.author.id}"/></div>
            </g:if>
            <g:else>
                <div style="float:none; height:30px; display:inline-block;"></div>
            </g:else>
        </div>
        <div class="txt"><g:cutText length="220">${post?.text}</g:cutText> <g:postLink post="${post}">read more</g:postLink></div>
        <div class="additional_info">
            <a class="looks disabled">${post.looks}</a>
            <sec:ifLoggedIn>
                <a class="likes disabled" onclick="likePost('#post${post.id}', ${post.id})">${post.likes}</a>
                <a class="dislikes disabled" onclick="dislikePost('#post${post.id}', ${post.id})">${post.dislikes}</a>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <a class="likes disabled" data-toggle="modal" data-target="#login">${post.likes}</a>
                <a class="dislikes disabled" data-toggle="modal" data-target="#login">${post.dislikes}</a>
            </sec:ifNotLoggedIn>
            %{--<a class="comments disabled">${post.comments?.size()}</a>--}%
            <div class="right_items visible_wide">
                %{--<a class="share">${post.shares}</a>--}%
                <sec:ifLoggedIn>
                    <a class="star" onclick="starPost('#post${post.id} .star', ${post.id})">${post.stars}</a>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <a class="star" data-toggle="modal" data-target="#login">${post.stars}</a>
                </sec:ifNotLoggedIn>
            </div>
        </div>
        <hr/>
        <div class="addthis_inline_share_toolbox"
             data-url="${createLink(controller:'post', action:'view', id:post.id, params:[title: PostUtil.urlTitle(post)], absolute:true)}"
             data-title="${post.title}"
             data-description="${post.text}"
             data-media="${createLink(controller:'post', action:'image', id:post.id, absolute:true)}"></div>
        <g:if test="${currentUser && currentUser.hasRole(DbConst.ROLE_ADMIN)}">
            <hr/>
            Status:
            <g:select name="status${post.id}" from="${PostStatus.values()}" onchange="setPostStatus(${post.id}, this.value)" value="${post.status.toString()}" />
            Priority:
            <g:select name="priority${post.id}" from="${0..3}" onchange="setPostPriority(${post.id}, this.value)" />
            Action:
            <g:select name="action${post.id}" from="${['-', 'edit', 'delete']}" onchange="doPostAction(${post.id}, this.value); this.value = '-'" />
        </g:if>
    </div>
</div>