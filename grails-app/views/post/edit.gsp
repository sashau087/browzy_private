<%@ page import="browzy.CategoryUtil" %>
<%@ page import="browzy.Const" %>
<%@ page import="browzy.DbConst" %>
<!doctype html>
<html>
<head>
    <asset:stylesheet src="bootstrap-tagsinput.css"/>

    <asset:javascript src="bootstrap-tagsinput.min.js"/>
    <asset:javascript src="post.js"/>

    <script>
        var IMAGE_TYPES = "${Const.IMAGE_TYPES}";
        var MAX_IMAGE_SIZE = ${DbConst.MAX_IMAGE_SIZE};
        var MAX_GIF_SIZE = ${DbConst.MAX_GIF_SIZE};
        var MAX_POST_TITLE_SIZE = ${DbConst.MAX_POST_TITLE_SIZE}
        var MAX_TEXT_SIZE = ${DbConst.MAX_TEXT_SIZE}
        var MAX_CATEGORIES = ${DbConst.MAX_CATEGORIES}
        var MAX_LINK_SIZE = ${DbConst.MAX_LINK_SIZE}
    </script>
</head>
<body>
    <div id="inner_page">
        <div class="container">
            <g:uploadForm controller="post">

                %{-- FORM HEADER --}%
                <div class="add_post_header_block">
                    <div class="add_post_page_header">${action ?: 'add'} post</div>
                    <div class="assist_switch">
                        <div class="assist_label">Assist mode</div>
                        <div class="onoffswitch">
                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked="checked"/>
                            <label class="onoffswitch-label" for="myonoffswitch">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>


                <div class="clearfix"></div>

                %{-- POST HEADER --}%
                <div class="post_block">

                    %{-- ADD TITLE --}%
                    <div class="row input">
                        <input type="text" class="form-control" id="title" name="title" placeholder="Add title" value="${post?.title}"/>
                        <div class="constraint">Maximum title length is ${DbConst.MAX_POST_TITLE_SIZE}</div>
                        <span class="form-control-feedback"></span>
                        <span class="required_field"><asset:image src="marker.png" alt=""/></span>
                    </div>

                    %{-- ADD TEXT --}%
                    <div class="row textarea">
                        <textarea class="form-control" id="text" name="text"  placeholder="Add text" >${post ? post.text : ''}</textarea>
                        <div class="constraint">Maximum text length is ${DbConst.MAX_TEXT_SIZE}</div>
                        <span class="required_field"><asset:image src="marker.png" alt=""/></span>
                    </div>

                </div>

                %{-- POST STRUCTURE --}%
                <div class="sortable">

                    <g:render template="text"/>
                    <g:render template="image"/>
                    <g:render template="video"/>
                    <g:render template="gif"/>
                    <g:render template="link"/>

                    <g:if test="${structure}">
                        <g:each var="block" in="${structure}">
                            <g:if test="${block.type == 'text'}">
                                <g:render template="text" bean="${post?.texts?.find {it.name == block.name} }"/>
                            </g:if>
                            <g:elseif test="${block.type == 'image'}">
                                <g:render template="image" bean="${post?.images?.find {it.name == block.name} }"/>
                            </g:elseif>
                            <g:elseif test="${block.type == 'video'}">
                                <g:render template="video" bean="${post?.texts?.find {it.name == block.name} }"/>
                            </g:elseif>
                            <g:elseif test="${block.type == 'gif'}">
                                <g:render template="gif" bean="${post?.videos?.find {it.name == block.name} }"/>
                            </g:elseif>
                            <g:elseif test="${block.type == 'link'}">
                                <g:render template="link" bean="${post?.texts?.find {it.name == block.name} }"/>
                            </g:elseif>
                        </g:each>
                    </g:if>

                </div>

                %{-- ADD CONTENT --}%
                <div class="post_block">
                    <div class="add_content_subheader">Add content</div>
                    <div class="content_links">
                        <a href="#" class="text" data-toggle="tooltip" title="Add text" data-placement="top"></a>
                        <a href="#" class="image" data-toggle="tooltip" title="Add image" data-placement="top"></a>
                        <a href="#" class="video" data-toggle="tooltip" title="Add video" data-placement="top"></a>
                        <a href="#" class="gif" data-toggle="tooltip" title="Add gif" data-placement="top"></a>
                        <a href="#" class="link" data-toggle="tooltip" title="Add link" data-placement="top"></a>
                    </div>
                </div>

                %{-- TAGS, CATEGORY, SOURCE --}%
                <div class="post_block">

                    %{-- ADD TAGS --}%
                    <div class="tags-input">
                        <input type="text" class="form-control" id="tags" name="tags" placeholder="Add tags"
                               value="${post?.tags?.collect({it.text})?.join(',') }" />
                        <span class="form-control-feedback"></span>
                    </div>

                    %{-- SELECT CATEGORY --}%
                    <div class="row select">
                        <g:select class="selectpicker form-control select_border" id="category" name="category"
                                  from='${CategoryUtil.list()}' optionKey="id" optionValue="name"
                                  noSelection="${['null':'Select category...']}" value="${post ? post.categories[0]?.id : null}"/>
                        <script>
                            $('#category option').each(styleCategoryOption);
                        </script>
                    </div>

                    %{-- CHOOSE SOURCE --}%
                    <div class="row link_block">
                        <div class="col-md-2 col-sm-3 col-xs-6">
                            <div class="radio">
                                <input type="radio" id="my_post" name="post" ${post?.link ? '' : 'checked'} onchange="clear_source()"/>
                                <label for="my_post">My post</label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                            <div class="radio">
                                <input type="radio" id="other_source" name="post" ${post?.link ? 'checked' : ''} />
                                <label for="other_source">Other source</label>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="source_link" name="link" onchange="change_source(event)"
                                   placeholder="Link on source post" value="${post?.link}"/>
                            <div class="constraint">Maximum link length is ${DbConst.MAX_LINK_SIZE}</div>
                            <span class="form-control-feedback"></span>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>

                %{-- ADD COVER --}%
                <div id="cover_block" class="post_block">
                    <div class="gray_rounded_block"
                         style="${post?.cover ? 'background-image: url(' + createLink(controller:'post', action:'cover', id:post.id ?: tempPostId) + ')' : ''}">
                        <div class="dashed_rounded_block">
                            <div>Add cover</div>
                            <asset:image src="img.png" class="center-block" alt=""/>
                            <div class="or">Drop image</div>
                            <div id="upload_overall4" class="upload"></div>
                            <div class="or">or</div>
                            <div class="button_container">
                                <input type="file" name="cover" class="form-control" data-filename-placement="inside"
                                       title="${post?.cover ? post.cover.fileName : 'Upload image'}" accept="${Const.IMAGE_TYPES}" onchange="return set_image(event)"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footnote">Fields marked with "circle" are required</div>
                <div id="post_errors" class="error"></div>
                <div class="row add_post_buttons">
                    <input type="hidden" name="structure" value="${post?.structure}"/>
                    <input type="hidden" name="id" value="${post?.id}"/>
                    <!--<input type="hidden" name="tempPostId" value="${tempPostId}"/>-->
                    <div class="col-sm-4">
                        <button class="preview" name="_action_preview">Preview</button>
                    </div>
                    <div class="col-sm-4">
                        <button class="save_post" name="_action_saveDraft">Save post</button>
                    </div>
                    <div class="col-sm-4">
                        <!--button class="add_post" name="_action_addPost">Add post</button-->
                        <button class="add_post" name="_action_addPost">Add post</button>
                        <!--g:actionSubmit value="Add post" action="addPost" class="add_post"/-->
                    </div>
                </div>
            </g:uploadForm>
        </div>
    </div>

</body>
</html>
