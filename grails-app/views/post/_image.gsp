<%@ page import="browzy.Const" %>
<div class="post_block add_image ${it ? 'visible' : ''}">
    <div class="close"><asset:image src="close.png" alt="" name="delete_block"/></div>
    <asset:image src="red_arrow.png" alt="" class="red_arrow" />
    <div class="gray_rounded_block">
        <div class="dashed_rounded_block"
             style="${it ? 'background-image: url(' + createLink(controller:'post', action:'originalImage', id:post?.id ?: tempPostId, params:[imageName:it.name]) + ')' : ''}">
            <div>Add image</div>
            <asset:image src="img.png" class="center-block" alt=""/>
            <div class="or">Drop image</div>
            <div id="upload_overall3"  class="upload"></div>
            <div class="or">or</div>
            <div class="button_container">
                <input type="file" class="form-control" data-filename-placement="inside"
                        accept="${Const.IMAGE_TYPES}" onchange="return set_image(event)"
                        title="${it ? it.fileName : 'Upload image'}"
                        ${it ? "name=${it.name}" : ''} />
            </div>
        </div>
    </div>
</div>
<g:if test="${it}">
<script>
    employBlock(
        $("[name='${it.name}']").closest('.post_block'),
        'image',
        "${it.name.drop(5)}",
        'input');
</script>
</g:if>