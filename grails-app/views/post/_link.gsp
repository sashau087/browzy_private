<div class="post_block add_link ${it ? 'visible' : ''}">
    <div class="close"><asset:image src="close.png" alt="" name="delete_block"/></div>
    <asset:image src="red_arrow.png" alt="" class="red_arrow" />
    <div class="link_rounded_block">
        <div>Add link</div>
        <asset:image src="link2.png"  class="center-block" alt=""/>
        <div class="input">
            <input type="text" class="form-control" placeholder="Add link"
                ${it ? "name=${it.name} value=${it.content}" : ''} />
            <span class="form-control-feedback"></span>
        </div>
    </div>
</div>
<g:if test="${it}">
<script>
    employBlock(
        $("[name='${it.name}']").closest('.post_block'),
        'link',
        "${it.name.drop(4)}",
        'input');
</script>
</g:if>