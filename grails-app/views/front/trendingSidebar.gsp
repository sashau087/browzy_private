<html>
<head>
    <meta name="layout" content="content"/>
</head>
<body>
    <div class="pane_block">
        <div class="block_header">
            <g:link controller="front" action="trending"><g:titleHeader>Trending</g:titleHeader></g:link>
        </div>
        <g:render template="sidebarPost" collection="${trending}" var="post"/>
        <g:link controller="front" action="trending" class="more_button">More trending posts</g:link>
    </div>
</body>
</html>