<%@ page import="browzy.PostUtil" %>
<div class="preview">
    <g:set var="firstMedia" value="${PostUtil.firstMedia(post)}"/>
    <g:if test="${'image' == firstMedia?.type}">
        <g:postLink post="${post}">
            <img src="${createLink(controller:'post', action:'image', id:post.id)}" class="img-responsive center-block" alt=""/>
        </g:postLink>
    </g:if>
    <g:elseif test="${'video' == firstMedia?.type}">
        <g:postLink post="${post}">
            <asset:image src="empty.png" class="videolink" />
            <g:videoImage class="img-responsive center-block" size="medium">${firstMedia.content}</g:videoImage>
        </g:postLink>
    </g:elseif>
</div>
<div class="description"><g:postLink post="${post}">${post.title}</g:postLink></div>
