<html>
<head>
    <meta name="layout" content="content"/>
</head>
<body>
    <div class="pane_block">
        <div class="block_header">
            <g:link controller="front" action="latest"><g:titleHeader>Latest</g:titleHeader></g:link>
        </div>
        <g:render template="sidebarPost" collection="${latest}" var="post"/>
        <g:link controller="front" action="latest" class="more_button">More latest posts</g:link>
    </div>
</body>
</html>