<%@ page import="browzy.DbConst" %>
<!doctype html>
<html>
<head>
    <asset:javascript src="comment.js"/>
    <script>
        function showPosts(params, element) {
            $("#tabContent").load("${createLink(controller: 'post', action: 'list', params: [userId: user.id])}&" + params, function() {
                $("#authorPostsSelection").show();
            });
            if ($(element).closest("#authorPostsSelection").length) {
                selectButton(element);
            } else {
                selectTab(element);
            }
        }

        function showStaredPosts(element) {
            $("#tabContent").load("${createLink(controller: 'post', action: 'stared', params: [userId: user.id])}", function() {
                $("#authorPostsSelection").hide();
            });
            selectTab(element);
        }

        function showFollows(element) {
            $("#tabContent").load("${createLink(controller: 'user', action: 'follows', id: user.id)}", function() {
                $("#authorPostsSelection").hide();
                ${ownProfile ? 'allowUnfollow();' : ''}
            });
            selectTab(element);
        }

        function showFollowers(element) {
            $("#tabContent").load("${createLink(controller: 'user', action: 'followers', id: user.id)}", function() {
                $("#authorPostsSelection").hide();
            });
            selectTab(element);
        }

        function unfollow(userId) {
            $("#tabContent").load("${createLink(controller: 'user', action: 'unfollow', id: 'userId')}".replace("userId", userId));
        }

        $(document).ready(function() {
            $("#tab_${tab}").click();
        });
    </script>
</head>

<body>
<div id="inner_page">

<div class="container">
    <g:if test="${user?.id?.toString() == sec.loggedInUserInfo('field': 'id').toString()}">
        <g:link controller="user" action="edit" id="${user?.id}" class="assist_mode"/>

        <g:if env="development">
            <g:if test="${user?.id?.toString() == sec.loggedInUserInfo('field': 'id').toString()}">
                <g:link controller="admin" class="assist_mode1"/>
            </g:if>
        </g:if>
    </g:if>
    <div class="author_block">
        <div class="row">
            <div class="social_links pright">
                <a class="fb" href="${ user.facebookLink ?: '#' }"></a>
                <a class="tw" href="${ user.twitterLink ?: '#' }"></a>
                <a class="pt" href="${ user.pinterestLink ?: '#' }"></a>
                <a class="g" href="${ user.googleLink ?: '#' }"></a>
            </div>

            <g:if test="${!ownProfile}">
                <div class="follow_button">
                    <button>Follow author</button>
                </div>
            </g:if>

            <div class="col-md-3 col-sm-4">
                <div class="image">
                    <img src="${createLink(controller: 'user', action: 'avatar', id: user?.id)}"
                         class="avatar img-responsive" alt=""/>
                </div>
            </div>

            <div class="col-md-9 col-sm-8 about_block">
                <div class="author_name">${user.displayName}</div>

                <div class="about">About author</div>

                <div class="txt">${raw(user.about)}</div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7 col-xs-12">
            <div class="info_block">
                <div class="col-sm-2 col-xs-4">
                    <asset:image src="icon1.svg" class="img-responsive center-block" alt=""/>
                    <span>${user.looks}</span>
                </div>

                <div class="col-sm-2 col-xs-4">
                    <asset:image src="icon2.png" class="img-responsive center-block" alt=""/>
                    <span>${user.likes}</span>
                </div>

                <div class="col-sm-2 col-xs-4">
                    <asset:image src="icon3.png" class="img-responsive center-block" alt=""/>
                    <span>${user.dislikes}</span>
                </div>

                <div class="col-sm-2 col-xs-4">
                    <asset:image src="icon4.png" class="img-responsive center-block" alt=""/>
                    <span>${user.comments?.size()}</span>
                </div>

                <div class="col-sm-2 col-xs-4">
                    <asset:image src="icon5.png" class="img-responsive center-block" alt=""/>
                    <span>${user.shares}</span>
                </div>

                <div class="col-sm-2 col-xs-4">
                    <asset:image src="icon6.png" class="img-responsive center-block" alt=""/>
                    <span>${user.stars}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-7">
            <div class="posts_block">
                <div>
                    <span>${user.posts?.size()}</span>

                    <p>posts</p>
                </div>

                <div>
                    <span>${user.posts?.size()}</span>

                    <p>top posts</p>
                </div>
            </div>
        </div>

        <div class="col-md-2 col-xs-5">
            <div class="rating_block">
                <span>${user?.rating}</span>

                <p>author rating</p>
            </div>
        </div>
    </div>

    <div class="row post_menu_block">
        <div class="col-md-10 col-sm-12 col-xs-7 post_menu">
            <ul>
                <li>
                    <a id="tab_posts" href="#" onclick="showPosts('status=ACTIVE', this); return false;">Author's posts</a>
                </li>
                <li><a href="#">Author's posts in top</a></li>
                <li><a href="#">Favorite posts</a></li>
                <li>
                    <a id="tab_follows" href="#" onclick="showFollows(this); return false;">I follow</a>
                </li>
                <li>
                    <a id="tab_followers" href="#" onclick="showFollowers(this); return false;">My followers</a>
                </li>
                <li><a id="comments" style="outline: none;" href="#" onclick="openComments(this); return false;">Comments</a></li>
            </ul>

            <div class="mobile_postmenu_block">
                <div class="post_menu_trigger"><asset:image src="post_trigger.png" alt=""/></div>

                <div class="at">Authors posts</div>
            </div>
        </div>

        <div class="col-md-2  col-sm-12 col-xs-5 search">
            <div class="form_trigger"></div>

            <div class="hidden_form">
                <form>
                    <input type="text" class="form-control"/>

                    <div class="clear_form"></div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 left_pane postsBlock">
            <div id="authorPostsSelection" style="display:none;">
                <button class="btn-selected" onclick="showPosts('status=ACTIVE', this);">Published</button>
                <button class="btn-secondary" onclick="showPosts('status=APPROVE', this);">Under review</button>
                <button class="btn-secondary" onclick="showPosts('status=DRAFT', this);">Drafts</button>
            </div>
            <div id="tabContent">
                <g:include controller="post" action="list" params="[status:'ACTIVE']" />
            </div>


        </div>
        <div class="col-md-8 left_pane commentsBlock" style="display:none;">
            <g:if test="${user?.hasRole(DbConst.ROLE_ADMIN)}">
                <g:set var="postsVar" value="${posts}"/>
            </g:if>
            <g:if test="${!user?.hasRole(DbConst.ROLE_ADMIN)}">
                <g:set var="postsVar" value="${user.posts}"/>
            </g:if>
            <g:each in="${postsVar}" var="post">
                <div class="white_block">
                    <div class="subheader"><a href="/${post?.title}/${post?.id}">${post?.title}</a></div>
                %{--<div class="row comment_block">--}%
                        <g:each in="${post?.comments?}" var="p">
                            <g:set var="replyAccess" value="true"/>
                            <g:each in="${post?.comments?}" var="p2">
                                <g:each in="${p2.replies}" var="p3">
                                    <g:if test="${p3.id==p.id}">
                                        <g:set var="replyAccess" value="false"/>
                                    </g:if>
                                </g:each>
                            </g:each>
                            <g:if test="${replyAccess=='true'}">
                                <div class="row comment_block com${p?.id}">
                                    <div class="col-sm-1 col-xs-2" id="user" ><a href="/user/profile/${p?.user?.username}/${p?.user?.id}" class="comUser"><img src="/user/avatar/${p?.user?.id}?width=30" class="img-circle center-block" alt=""/></a></div>
                                    <div class="col-sm-11 col-xs-10">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="name"><a href="/user/profile/${p?.user?.username}/${p?.user?.id}">${p?.user?.username}</a> <span id="comment_date">${p?.date}</span></div>
                                            <div class="text"><a href="/${post?.title}/${post?.id}#com${p.id}">${p?.text}</a></div>
                                            <g:if test="${p?.avatarId!=null}">
                                                <img src="${createLink(controller: 'comment', action: 'avatar', id: p?.id)}" alt="" class="img-responsive img_comment" style="margin-bottom: 10px"/>
                                            </g:if>
                                        </div>
                                        <div class="col-sm-6 col-xs-12 reply_block" id="${p?.id}">
                                            %{--<sec:ifNotLoggedIn>--}%
                                            %{--<button data-toggle="modal" data-target="#login">Reply</button>--}%
                                            %{--</sec:ifNotLoggedIn>--}%
                                            %{--<sec:ifLoggedIn>--}%
                                            %{--<button class="replyButton${p?.id}" onclick="reply(${p?.id})">Reply</button>--}%
                                            %{--</sec:ifLoggedIn>--}%
                                            <g:if test="${user?.hasRole(DbConst.ROLE_ADMIN)}">
                                                <button class="deleteComment${p?.id}" onclick="deleteComment(${p?.id})">Delete</button>
                                            </g:if>
                                            <a class="likes likesComment${p?.id}
                                            <g:if test="${p?.getLikesCommentUser(user)==1}">likesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="likeComment(${p?.id})"</sec:ifLoggedIn><sec:ifNotLoggedIn> data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                                                ${p?.likesComment}
                                            </a>
                                            <a class="dislikes dislikesComment${p?.id}
                                            <g:if test="${p?.getDislikesCommentUser(user)==1}">dislikesCommentAdded</g:if>" <sec:ifLoggedIn>onclick="dislikeComment(${p.id})"</sec:ifLoggedIn><sec:ifNotLoggedIn> data-toggle="modal" data-target="#login"</sec:ifNotLoggedIn> style="cursor: pointer">
                                                ${p?.dislikesComment}
                                            </a>
                                        </div>
                                        <sec:ifLoggedIn>
                                            <div class="row add_post_form replyBlock${p?.id}" style="display: none;">
                                                <g:form enctype="multipart/form-data" name="fileinfo">
                                                    <div class="col-md-8 col-sm-9 textarea_container">
                                                        <textarea class="form-control commentBox${p?.id}" name="text" placeholder="Аdd comment"></textarea>
                                                    </div>
                                                    <div class="col-md-2 col-sm-3 file_container">
                                                        <input id="${p?.id}" type="file" class="form-control commentfileinput commentfile${p?.id} commentFileReply" data-filename-placement="inside" name="image${p?.id}" title="Upload image" />
                                                    </div>
                                                    <div id="post_errors" class="error"></div>
                                                    <div class="col-md-2 col-sm-4 col-md-offset-0 col-sm-offset-8">
                                                        <button class="add_comment_reply" onclick="add_comment_reply(${p?.id}, event)">add comment</button>
                                                    </div>
                                                    <div class="img_comment${p?.id}" style="width: 300px !important;"></div>
                                                </g:form>
                                            </div><br>
                                        </sec:ifLoggedIn>

                                    <g:each in="${p?.replies}" var="media">
                                    <g:render template="/post/replyProfile" model="${[comment: media, commentParent: p, post: post]}" />
                                    </g:each>
                                    </div>
                                </div>
                            </g:if>
                        </g:each>

                    </div>
                </g:each>
            </div>
            <div class="col-md-4 right_pane">
                %{--<g:if test="${!user?.hasRole(DbConst.ROLE_AUTHOR)}">
                    <div>
                        <div class="pane_block">
                            <div class="block_header">
                                <g:link class="more_button" controller="user" action="authorize"
                                        id="${user?.id}">Authorize</g:link>
                            </div>
                        </div>
                    </div>
                </g:if>--}%
                <g:include controller="front" action="trendingSidebar"/>
                <g:include controller="front" action="latestSidebar"/>
            </div>

        </div>
    </div>
</div>
</body>
</html>
