<div class="rating_item">
    <div class="row">
        <div class="col-sm-7 col-xs-12  col-sm-offset-2  user_item user_item_profile_page">
            <div class="col-xs-3 user_image">
                <g:userLink user="${user}">
                    <img src="${createLink(controller:'user', action:'avatar', id:user.id)}"
                         style="width:70px; height:70px;" alt=""/>
                </g:userLink>
            </div>
            <div class="col-xs-9">
                <div class="name">
                    <g:userLink user="${user}">${user.displayName}</g:userLink>
                </div>
                <span class="looks">${user.looks}</span>
                <span class="likes">${user.likes}</span>
                <span class="subscribers">${user.followersCount}</span>
            </div>
        </div>
        <div class="col-sm-2 col-xs-6 subscribe_button">
            <button data-userid="${user.id}">Following</button>
        </div>
        <div class="col-sm-3 col-xs-6 rank_block">
            <div class="bg">
                <div class="rank">${user.likes - user.dislikes}</div>
            </div>
        </div>
    </div>
</div>
