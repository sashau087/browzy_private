<%@ page import="browzy.Const" %>
<%@ page import="browzy.DbConst" %>
<%@ page import="browzy.NotificationService" %>
<% NotificationService notificationService = grailsApplication.mainContext.getBean('notificationService') %>
<!doctype html>
<html>
<head>
    <script>
        var IMAGE_TYPES = "${Const.IMAGE_TYPES}";
        var MAX_IMAGE_SIZE = "${DbConst.MAX_IMAGE_SIZE}";
        var MAX_GIF_SIZE = "${DbConst.MAX_GIF_SIZE}";
    </script>
</head>
<body>
<div id="inner_page">
    <div class="container">

        <g:uploadForm name="edit_user" controller="user" action="saveSettings">
            <g:set var="user" value="${flash.invalidUser ?: user}"/>
            <g:set var="errors" value="${user.errors}"/>
            <div class="author_block">
                <div class="row">
                    <div class="social_links">
                        <a class="fb inactive" href="${ user.facebookLink ?: '#' }"></a>
                        <a class="tw inactive" href="${ user.twitterLink ?: '#' }"></a>
                        <a class="pt inactive" href="${ user.pinterestLink ?: '#' }"></a>
                        <a class="g inactive" href="${ user.googleLink ?: '#' }"></a>
                    </div>

                    <div class="col-md-3 col-sm-4">
                        <div class="image">
                            <img src="${createLink(controller: 'user', action: 'avatar', id: user.id)}"
                                 class="avatar img-responsive" alt=""/>

                            <div style="position: absolute; bottom:10px; right:10px;">
                                <input type="file" name="avatar"
                                       class="form-control editprofileimage" data-filename-placement="inside" title=""
                                       accept="${Const.IMAGE_TYPES}" onchange="return setAvatar(event)"
                                       />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9 col-sm-8 about_block">
                        <input type="hidden" id="name" name="name" value="${user.name}"/>
                        <input type="hidden" id="about" name="about" value="${user.about}"/>

                        <div class="author_name nameedit${errors?.hasFieldErrors('name') ? ' has-error' : ''}" data-key="name" contenteditable="true">${user.displayName}</div>
                        <div class="error"><g:message error="${errors?.getFieldError('name')}" /></div>

                        <div class="about pl">About author</div>
                        <div class="txt textedit" data-key="about" contenteditable="true">
                            ${raw(user.about)}
                        </div>
                        <div class="error"><g:message error="${errors?.getFieldError('about')}" /></div>
                    </div>
                </div>
            </div>

            <div class="post_block">
                <div class="row input">
                    <div class="col-sm-2 label_block">
                        <label for="username">Login</label>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="username" name="username"
                               placeholder="${user?.username}" disabled="true"/>
                        <span class="form-control-feedback"></span>
                    </div>
                </div>

                <div class="row input">
                    <div class="col-sm-2 col-xs-12 label_block">
                        Gender
                    </div>

                    <div class="col-sm-2  col-xs-6 ">
                        <div class="radio gender_radio${user.gender != null ? ' disabled' : ''}">
                            <g:radio id="male" name="gender"
                                     checked="${'m' == user.gender}" value="m"
                                     disabled="${user.gender != null}"/>
                            <label for="male">Male</label>
                        </div>
                    </div>

                    <div class="col-sm-2  col-xs-6">
                        <div class="radio gender_radio${user.gender != null ? ' disabled' : ''}">
                            <g:radio id="female" name="gender"
                                     checked="${'f' == user.gender}" value="f"
                                     disabled="${user.gender != null}"/>
                            <label for="female">Female</label>
                        </div>
                    </div>
                </div>

                <div class="row select">
                    <div class="col-sm-2 label_block">
                        <label for="country">Country</label>
                    </div>

                    <div class="col-sm-10">
                        <g:countrySelect class="selectpicker form-control select_border" name="country"
                                         value="${user?.country}" noSelection="['': '-Choose your country-']"
                                         disabled="true"/>
                    </div>
                </div>

                <div class="row input">
                    <div class="col-sm-2 label_block">
                        <label for="email">Email</label>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="email" name="email" placeholder="${user?.email}"
                               disabled="true"/>
                        <span class="form-control-feedback"></span>
                    </div>
                </div>
            </div>

            <div class="post_block">
                <div class="row input">
                    <div class="col-sm-2 label_block">
                        <label for="old_password">Old password</label>
                    </div>

                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="old_password" name="old_password" form="pass"/>
                        <span class="form-control-pass"></span>
                    </div>
                </div>

                <div class="row input">
                    <div class="col-sm-2 label_block">
                        <label for="new_password">New password</label>
                    </div>

                    <div class="col-sm-10">
                        <input type="password" id="new_password" name="new_password" class="form-control" form="pass"/>
                        <span class="form-control-pass"></span>
                    </div>
                </div>

                <div class="row input">
                    <div class="col-sm-2 label_block">
                        <label for="repeat_password">Repeat</label>
                    </div>

                    <div class="col-sm-10">
                        <input type="password" id="repeat_password" name="repeat_password" class="form-control"
                               form="pass"/>
                        <span class="form-control-pass"></span>
                    </div>
                </div>

                <g:if test="${flash.changePasswordError}">
                    <div class="error">
                        ${flash.changePasswordError}
                    </div>
                    <script>
                        $('#old_password')[0].scrollIntoView(true);
                    </script>
                </g:if>

                <div class="row">
                    <div class="col-sm-2"></div>

                    <div class="col-sm-10">
                        <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-4">
                            <a class="reset_password_link" data-toggle="modal"
                               data-target="#remind_pass">Forgot your password?</a>
                        </div>

                        <div class="col-md-2 col-sm-3 col-xs-6 apply_button">
                            <button form="pass">Apply</button>
                        </div>

                        <div class="col-md-2  col-sm-3 col-xs-6 cancel_button">
                            <div>Cancel</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="post_block">
                <div class="row input">
                    <div class="col-sm-2 label_block">
                        <label for="facebook">Facebook</label>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="facebook" name="socialLinks"
                               value="${user?.facebookLink}"/>
                        <span class="form-control-feedback"></span>
                    </div>
                </div>

                <div class="row input">
                    <div class="col-sm-2 label_block">
                        <label for="twitter">Twitter</label>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="twitter" name="socialLinks"
                               value="${user?.twitterLink}"/>
                        <span class="form-control-feedback"></span>
                    </div>
                </div>

                <div class="row input">
                    <div class="col-sm-2 label_block">
                        <label for="pinterest">Pinterest</label>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="pinterest" name="socialLinks"
                               value="${user?.pinterestLink}"/>
                        <span class="form-control-feedback"></span>
                    </div>
                </div>

                <div class="row input">
                    <div class="col-sm-2 label_block">
                        <label for="google">Google +</label>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="google" name="socialLinks"
                               value="${user?.googleLink}"/>
                        <span class="form-control-feedback"></span>
                    </div>
                </div>
            </div>

            <div class="post_block">
                <div class="notification">notifications settings</div>

                <div class="checkbox">
                    <g:checkBox id="emailReply" name="emailReply"
                                value="${notificationService.checkNotificationAboutReplyToComment()}"/>
                    <label for="emailReply">Email me when somebody replies to my comment for the first time</label>
                </div>

                <div class="checkbox">
                    <g:checkBox id="emailSuccess" name="emailSuccess"
                                value="${notificationService.checkNotificationAboutSuccessStory()}"/>
                    <label for="emailSuccess">Email me about my story success</label>
                </div>

                <div class="checkbox">
                    <g:checkBox id="emailComment" name="emailComment"
                                value="${notificationService.checkNotificationAboutCommentPost()}"/>
                    <label for="emailComment">Email me when somebody comments on my post for the first time</label>
                </div>
            </div>

            <div class="save_changes">
                <button>Save changes</button>
            </div>
        </g:uploadForm>

        <g:form name="pass" controller="user" action="changePassword"></g:form>
    </div>
</div>
</body>
</html>
