<html>
<head>
    <meta name="layout" content="content" />
</head>
<body>
<div class="rating_pane">
    <div class="top_subheader">Followed authors</div>
    <g:if test="${!users?.empty}">
        <g:render template="userTemplate" collection="${users}" var="user"/>
    </g:if>
    <g:else>
        <div class="rating_item">
            <div class="row" style="border-bottom: 0;">
                No subscriptions.
            </div>
        </div>
    </g:else>
    <div class="pagination_block">
        <ul class="pagination">
        </ul>
    </div>
</div>
</body>
</html>