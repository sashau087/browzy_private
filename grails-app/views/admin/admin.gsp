<%@ page import="browzy.User" %>
<!doctype html>
<html>
<head>
    <asset:stylesheet src="admin.css"/>
    <asset:javascript src="angular.min.js"/>
</head>

<body>
<div ng-app="userTblApp" ng-controller="UserTblCtrl">
    <div ng-app="userTblApp" ng-controller="PostTblCtrl">
        <table id="tableUsers" class="hidden">
            <caption>Users</caption>
            <thead>
            <tr>
                <g:each in="${['username', 'email', 'country']}" var="p">
                    <th>
                        <a href="#" ng-click="sortUser = '${p}'; sortReverseUser = !sortReverseUser">
                            <g:if test="${p == 'email'}">
                                E-mail
                            </g:if>
                            <g:else>
                                ${p.charAt(0).toUpperCase()}${p.substring(1)}
                            </g:else>
                            <span ng-show="sortUser == '${p}' && !sortReverseUser" class="fa fa-caret-down"></span>
                            <span ng-show="sortUser == '${p}' && sortReverseUser" class="fa fa-caret-up"></span>
                        </a>
                    </th>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="user in users | orderBy:sortUser:sortReverseUser" ng-dblclick="findPosts();">
                <g:each in="${['username', 'email', 'country']}" var="p">
                    <td>
                        {{ user.${p} }}
                    </td>
                </g:each>
            </tr>
            </tbody>
        </table>
        <br>
        <table id="tablePosts" class="hidden">
            <caption>Posts</caption>
            <thead>
            <tr>
                <g:each in="${['title', 'author', 'categories', 'tags', 'posted', 'status', 'looks', 'likes', 'dislikes']}" var="p">
                    <th>
                        <a href="#" ng-click="sortPost = '${p}'; sortReversePost = !sortReversePost">
                            <g:if test="${p == 'posted'}">
                                Posted on
                            </g:if>
                            <g:else>
                                ${p.charAt(0).toUpperCase()}${p.substring(1)}
                            </g:else>
                            <span ng-show="sortPost == '${p}' && !sortReversePost" class="fa fa-caret-down"></span>
                            <span ng-show="sortPost == '${p}' && sortReversePost" class="fa fa-caret-up"></span>
                        </a>
                    </th>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="post in resultPosts = (posts | orderBy:sortPost:sortReversePost)">
                <td ng-repeat="(key, value) in post">
                    <span ng-if="key === 'title'">
                        <g:form controller="post" action="view">
                            <button class="btn1">{{ value }}</button>
                        </g:form>
                    </span>
                    <span ng-if="key !== 'title'">
                        {{ value }}
                    </span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<asset:javascript src="userTblCtrl.js"/>
</body>
</html>
