<g:setGlobalVars/>
<!doctype html>
<html>
<head>
</head>
<body>
<div id="page_block">
    <div class="container">
        <div class="row">
            <div class="col-md-8 left_pane">
                <div class="page_subheader">${header}</div>
                <g:render template="/post/postTemplate" collection="${posts}" var="post"/>
            </div>

            <g:include controller="front" action="sidebar"/>
        </div>
    </div>
</div>
</body>
</html>
