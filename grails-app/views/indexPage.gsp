<%@ page import="browzy.Const" %>
<html>
<head>
    <meta name="layout" content="content"/>
</head>
<body>
    <g:setGlobalVars/>

    <g:render template="/post/postTemplate" var="post" collection="${posts}" model="['headers': headers, 'lastHeader': lastHeader]"/>

    <g:if test="${posts.size == Const.POSTS_PAGE_LIMIT}">
        <div id="posts_loading">Loading...</div>
        <script>
            window.loadPostsUrl = "${raw(createLink(controller: 'front', action: 'indexPage',
                params: [start: start, topPostsIds: topPostsIds, lastHeader: lastHeader]))}";
            window.jPostsLoading = $('#posts_loading')
            addthis.layers.refresh()
        </script>
    </g:if>
</body>
</html>