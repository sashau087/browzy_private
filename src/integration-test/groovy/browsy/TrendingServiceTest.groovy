package browsy

import browzy.*
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import spock.lang.Specification

@Integration
@Rollback
class TrendingServiceTest extends Specification {

    def trendingService

    User user
    Post p1, p2, p3

    def setupData() {
        user = new User(username: 'user', password: 'pass', email: "ololo@domain.com")
        user.save(flush: true)
        p1 = new Post(structure: "title", status: PostStatus.ACTIVE, title: "ololol KEY 111", author: user, text: "sdfn")
        p1.save(flush: true)
        for (int i = 0; i < 10; i++) {
            User user1 = new User(username: 'user' + i, password: 'pass', email: "ololo$i@domain.com").save()
            new Counter(post: p1, user: user1, liked: true, looked: true).save()
            def c = new Comment(post: p1, user: user1, text: "text$i").save()
            def c2 = new Comment(post: p1, user: user1, text: "text$i$i").save()
            println("$c$c2")
        }
        p2 = new Post(structure: "title", status: PostStatus.ACTIVE, title: "ololol 1011 1KEY1 111", author: user, text: "sdfn")
        p2.save(flush: true)
        for (int i = 10; i < 22; i++) {
            User user1 = new User(username: 'user' + i, password: 'pass', email: "ololo$i@domain.com").save()
            new Counter(post: p2, user: user1, liked: true, looked: true).save()
            def c = new Comment(post: p2, user: user1, text: "text$i").save()
            println("$c")
        }
        p3 = new Post(structure: "title", status: PostStatus.ACTIVE, title: "ololol 1011 111 qwerty", author: user, text: "sdfn")
        p3.save(flush: true)
        for (int i = 22; i < 33; i++) {
            User user1 = new User(username: 'user' + i, password: 'pass', email: "ololo$i@domain.com").save()
            new Counter(post: p3, user: user1, liked: true, looked: false).save()
            def c = new Comment(post: p3, user: user1, text: "text$i").save()
            def c2 = new Comment(post: p3, user: user1, text: "text$i$i").save()
            def c3 = new Comment(post: p3, user: user1, text: "text$i$i$i").save()
            println("$c$c2$c3")
        }
    }

    def "ListTrendingPosts"() {
        given:
        setupData()

        when:
        def res = trendingService.listTrendingPosts()

        then:
        res[0] == p2
        res[1] == p3
        res[2] == p1
        res.size() == 3
    }

    def "ListPopularPosts"() {
        given:
        setupData()

        when:
        def res = trendingService.listPopularPosts()

        then:
        res[0] == p2
        res[1] == p1
        res.size() == 2
    }

    def "ListHotPosts"() {
        given:
        setupData()

        when:
        def res = trendingService.listHotPosts()

        then:
        res[0] == p3
        res[1] == p1
        res[2] == p2
        res.size() == 3
    }
}
