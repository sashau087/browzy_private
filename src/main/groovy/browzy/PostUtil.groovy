package browzy

import grails.converters.JSON

class PostUtil {

    static firstMedia(Post post) {
        def structure = JSON.parse(post.structure)
        def block = structure.find({ it.type == 'image' || it.type == 'video' })
        if (block) {
            if (block.type == 'image') {
                return [type: 'image']
            } else {
                return [type: 'video', content: post.texts.find({it.name == block.name}).content]
            }
        }
        return null
    }

    static String findVideo(Post post) {
        def structure = JSON.parse(post.structure)
        for (def block : structure) {
            if (block.type == 'video') {
                return post.texts.find({it.name == block.name}).content
            }
        }
        return null
    }

    static String urlTitle(Post post) {
        return post.title
                .toLowerCase()
//                .replaceAll("[^\\p{Alnum}\\s]", "")
//                .replaceAll("\\W", "")
//                .replaceAll("[^\\p{L}^\\p{N}^\\p{M}\\s%]+", "")
                .replaceAll("[^\\p{L}\\p{N}\\p{M}\\s]", "")
                .trim()
                .replaceAll(" +", " ")
                .replaceAll(" ", "-")
    }
}
