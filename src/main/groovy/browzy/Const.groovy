package browzy

interface Const {

    String IMAGE_TYPES = '.jpg,.jpeg,.png'
    String GIF_TYPE = '.gif'

    int POSTS_PAGE_LIMIT = 10

    PostStatus[] PUBLIC_POST_STATUSES = [PostStatus.ACTIVE, PostStatus.PAID]
}