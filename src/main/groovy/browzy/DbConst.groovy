package browzy

interface DbConst {

    String ROLE_ADMIN = "ROLE_ADMIN"
    String ROLE_REGISTERED = "ROLE_REGISTERED"
    String ROLE_AUTHOR = "ROLE_AUTHOR"

    String ADMIN = 'admin'
//    String REGISTERED = 'pavel'
//    String AUTHOR = 'author'

    String[] CATEGORIES = ['Facts', 'Cutes', 'Humor', 'Video', 'Gifs', 'Funny', 'DIY', 'Food', 'Architecture', 'Nature',
                           'Accident', 'Cats', 'Dogs', 'Animals', 'History', 'Photography', 'Travel', 'Design', 'Girls',
                           'Technology', 'People', 'Art', 'Lifehacks',
                           'Other']

    Object[] USER_FIELDS_SIMPLE_EDIT = ['username', 'name', 'about', 'email']

    // 1000 kb = 1000 * 1024
    int MAX_IMAGE_SIZE = 1024000

    int MAX_COMMENT_IMAGE_SIZE = 204800
    // 10 mb = 10 * 1000 * 1024
    int MAX_GIF_SIZE = 10240000

    int MAX_POST_TITLE_SIZE = 70
    int MAX_TEXT_SIZE = 3600
    int MAX_CATEGORIES = 3
    int MAX_LINK_SIZE = 512
    int MAX_USER_LOGIN_SIZE = 13
    int MAX_USER_NAME_SIZE = 20
    int MAX_USER_ABOUT_SIZE = 500

    int IMAGE_SCALED_WIDTH = 750
    int AVATAR_SCALED_WIDTH = 70
}