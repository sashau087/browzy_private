package browzy

class CategoryUtil {

    static list() {
        def list = Category.list()
        def other = list.find { it.name == 'Other' }
        list.remove(other)
        list.add(other)
        return list
    }
}
