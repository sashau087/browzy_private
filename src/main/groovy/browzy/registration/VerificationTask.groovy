package browzy.registration

import browzy.EmailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class VerificationTask {
    @Autowired
    EmailService emailService

    /**
     * every 10 minutes
     */
    @Scheduled(cron = "0 */10 * * * *", zone = "UTC")
    void notifyDoctorsAboutEndOfRecruitingOfStudies() {
        emailService.checkUserEmails()
    }
}
