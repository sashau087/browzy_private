<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <xsl:for-each select="rss/channel">
                    <h2><xsl:value-of select="title"/></h2>
                    <h3><xsl:value-of select="description"/></h3>

                    <xsl:for-each select="item">
                        <div style="margin-bottom: 10px;">
                            <div>
                                <a href="{link}"><xsl:value-of select="title"/></a>
                            </div>

                            <small>
                                <xsl:value-of select="pubDate"/>
                            </small>

                            <div>
                                <xsl:value-of select="description"/>
                            </div>
                        </div>
                    </xsl:for-each>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>