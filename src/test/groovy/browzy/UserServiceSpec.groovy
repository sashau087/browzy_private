package browzy

import browzy.registration.VerificationToken
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(UserService)
@Mock([User, Post, VerificationToken])
class UserServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test listAllUsersForAdminView"() {
        given:
        User user1 = new User(username: 'user1', password: 'password1', email: 'user1123@mail.com', country: 'Al')
        user1.save(flush: true)
        User user2 = new User(username: 'user2', password: 'password1', email: 'user2123@mail.com', country: 'Ua')
        user2.save(flush: true)
        User user3 = new User(username: 'user3', password: 'password1', email: 'user3123@mail.com', country: 'Gr')
        user3.save(flush: true)
        User user4 = new User(username: 'user4', password: 'password1', email: 'user4123@mail.com', country: 'Ar')
        user4.save(flush: true)

        Post p1 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p1', text: 'text', author: user1).save(flush: true)
        Post p2 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p2', text: 'text', author: user1).save(flush: true)

        Post p3 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p3', text: 'text', author: user2).save(flush: true)
        Post p4 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p4', text: 'text', author: user2).save(flush: true)
        Post p5 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p5', text: 'text', author: user2).save(flush: true)
        Post p6 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p6', text: 'text', author: user2).save(flush: true)

        Post p7 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p7', text: 'text', author: user3).save(flush: true)
        Post p8 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p8', text: 'text', author: user3).save(flush: true)
        Post p9 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p9', text: 'text', author: user3).save(flush: true)

        Post p10 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p10', text: 'text', author: user4).save(flush: true)
        Post p11 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p11', text: 'text', author: user4).save(flush: true)
        Post p12 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p12', text: 'text', author: user4).save(flush: true)
        Post p13 = new Post(status: PostStatus.ACTIVE, structure: 'text', title: 'p13', text: 'text', author: user4).save(flush: true)

        when:
        def res = service.listAllUsersForAdminView()

        then:
        res == [user4, user2, user3, user1]

    }
}
