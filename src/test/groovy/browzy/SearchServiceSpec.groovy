package browzy

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(SearchService)
@Mock([Post, User])
class SearchServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test split"() {
        given:
        String s = "one, two three"
        expect:
        s.split(/[,\s]+/) == ['one', 'two', 'three'] as String[]
    }

    void "test for search"(){
        given:
        Post p1 = new Post(structure: "title", status: PostStatus.ACTIVE, title: "ololol KEY 111", author: new User(), text: "sdfn")
                p1.save(flush:true)
        Post p2 = new Post(structure: "title", status: PostStatus.ACTIVE, title: "ololol 1011 1KEY1 111", author: new User(), text: "sdfn")
                p2.save(flush:true)
        Post p3 = new Post(structure: "title", status: PostStatus.ACTIVE, title: "ololol 1011 111 qwerty", author: new User(), text: "sdfn")
                p3.save(flush:true)

        when:
        def result = service.searchForPostByTitle("key, 1011 qwerty")

        then:
        result*.id == [1,2,3]
    }
}
