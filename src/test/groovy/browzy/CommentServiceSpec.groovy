package browzy

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(CommentService)
@Mock([Post, Comment, User])
class CommentServiceSpec extends Specification {

    def springSecurityService

    def setup() {
        springSecurityService = Mock(SpringSecurityService)
        service.springSecurityService = springSecurityService
    }

    def cleanup() {
    }

    void "test commentPost"() {
        given:
        boolean isNotified = false
        def mock = Mock(NotificationService)
        mock.notifyAboutCommentPost(_) >> { isNotified = true }
        Comment.metaClass.getNotificationService = { return mock }
        User author = new User(username: 'author', password: 'password')
        author.save(flush: true)
        User commentator = new User(username: 'commentator', password: 'password')
        author.save(flush: true)
        springSecurityService.getCurrentUser() >> commentator

        Post post = new Post(title: 'title', text: "some text", author: author, status: PostStatus.DRAFT, structure: "title, text")
        post.save(flush: true)

        when:
        def result = service.commentPost(post.id, "comment 1o1o1")
        then:
        result.user == commentator
        result.post == post
        result.text == "comment 1o1o1"
        isNotified
    }

    void "test commentAnotherComment"() {
        given:
        boolean isNotified = false
        def mock = Mock(NotificationService)
        mock.notifyAboutReplyToComment(_,_) >> { isNotified = true }
        Comment.metaClass.getNotificationService = { return mock }
        User author = new User(username: 'author', password: 'password')
        author.save(flush: true)
        User commentator = new User(username: 'commentator', password: 'password')
        author.save(flush: true)
        springSecurityService.getCurrentUser() >> commentator

        Post post = new Post(title: 'title', text: "some text", author: author, status: PostStatus.DRAFT, structure: "title, text")
        post.save(flush: true)

        Comment baseComment = new Comment(user: author, post: post, text: 'source text')
        baseComment.save(flush:true)

        when:
        def result = service.commentAnotherComment(post.id, "comment 1o1o1")

        then:
        result.user == author
        result.replies.first().user == commentator
        result.text == "source text"
        isNotified
    }
}
