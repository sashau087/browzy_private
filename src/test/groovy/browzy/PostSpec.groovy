package browzy

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Post)
@Mock([User])
class PostSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "test for approve post by admin"() {
        given:
        boolean isNotified = false
        def mock = Mock(NotificationService)
        mock.notifyAboutSuccessStory(_) >> { isNotified = true }
        Post.metaClass.getNotificationService = { return mock }
        Post post = new Post(status: PostStatus.ACTIVE, structure: "title", title: "post", text: "post", author: new User())
        post.save(flush:true)

        !isNotified
        when:
        post.status = PostStatus.APPROVE
        post.save(flush:true)

        then:
        post.status == PostStatus.APPROVE
        isNotified
    }
}
